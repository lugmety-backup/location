<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 5:24 PM
 */

namespace App\Http\Controllers;
use App\Repo\CountryInterface;
use App\Repo\CountryMetaInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CountryMetaController extends Controller
{
    protected $countryMeta;
    protected $country;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CountryMetaInterface $countryMeta,CountryInterface $country)
    {
        $this->countryMeta=$countryMeta;
        $this->country=$country;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $country= $this->countryMeta->countryMetaGetAll();
            try{
                if(!$country->first()){
                    throw new \Exception();
                }
            }
            catch (\Exception $ex){
                return response()->json([
                    'status'=>'200',
                    "message"=>'Empty Record'
                ],200);
            }
            return response()->json([
                'status'=>'200',
                'country'=>$country
            ],200);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'404',
                'message'=>'error while getting records'
            ],404);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{
            $this->validate($request, [
                'meta_key' => 'required|alpha',
                'country_id' => 'required|integer',
                'meta_value' => 'required|alpha',
            ]);
            $request=$request->all();
            try {
                $this->country->countryGetSpecific($request['country_id']) ;
            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'404',
                    'message'=>"Country  could not be found"
                ],404);
            }
            $country= $this->countryMeta->countryMetaGetSpecificByCode(strtolower($request['meta_key']),$request['country_id']);

            try {
                if ($country->count() != 0) {

                    throw new \Exception();
                }
            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'409',
                    'message'=>"Country Meta could not be created due to duplicate entry "
                ],409);
            }
            $country=$this->countryMeta->countryMetaCreate($request);
            return response()->json([
                'status'=>'200',
                'message'=>'Country created successfully',
                'CountryMeta'=>$country
            ],200);

        }

        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $country= $this->countryMeta->countryMetaGetSpecific($id);
            return response()->json([
                'status'=>'200',
                'countryMeta'=>$country
            ],200);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Country Meta could not be found"
            ],404);
        }
        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Country Meta could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Country Meta could not be found"
            ],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'meta_key' => 'required|alpha',
                'country_id' => 'required|integer',
                'meta_value' => 'required|alpha',
            ]);
            $request = $request->all();
            $country= $this->countryMeta->countryMetaGetSpecificByCode(strtolower($request['meta_key']),$request['country_id']);

            try {
                if ($country->count() != 0) {

                    throw new \Exception();
                }
            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'409',
                    'message'=>"Country Meta could not be Updated due to duplicate entry "
                ],409);
            }
            $country = $this->countryMeta->countryMetaUpdate($id, $request);
            return response()->json([
                'status' => '200',
                'message' => 'Country Meta Updated successfully',
                'countryMeta' => $country
            ]);

        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'409',
                'message'=>"Country Meta could not be created due to duplicate entry"
            ],409);
        }

        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Country could not be found"
            ],404);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $country= $this->countryMeta->countryMetaDelete($id);
            return response()->json([
                'status'=>'200',
                'countryMeta'=>"Country Meta deleted successfully"
            ]);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Country Meta could not be found"
            ],404);
        }
        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Country Meta could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Country Meta could not be found"
            ],404);
        }
    }

}