<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/16/2017
 * Time: 10:42 AM
 */

namespace App\Repo\Eloquent;


use App\BillingAddress;
use App\Repo\BillingAddressInterface;

class BillingAddressRepo implements BillingAddressInterface
{
    protected $billingAddress;

    /**
     * BillingAddressRepo constructor.
     * @param $billingAddress
     */
    public function __construct(BillingAddress $billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }

    public function billingAddressGetAllForDatatable(){
        return $this->billingAddress->all();
    }

    public function billingAddressGetAll($limit)
    {
        return $this->billingAddress->paginate($limit);
    }

    public function billingAddressGetSpecific($id)
    {
        return $this->billingAddress->findOrFail($id);
    }

    public function billingAddressGetSpecificByUserId($userId)
    {
        return $this->billingAddress->where("user_id",$userId)->get();

    }

    public function billingAddressCreate(array $attributes)
    {
        return $this->billingAddress->create($attributes);
    }

    public function billingAddressGetAllShippingAddressExceptPassedId($id,$userId){
        return $this->billingAddress->where([
            ["user_id",$userId],
            ["id","!=",$id]
        ])->get();
    }

    public function billingAddressUpdate($id, array $attributes)
    {
        $con=$this->billingAddress->findOrFail($id)->update($attributes);
        return $con;
    }

    public function billingAddressDelete($id)
    {
        return $this->billingAddressGetSpecific($id)->delete();
    }


}