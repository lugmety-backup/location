<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 3:19 PM
 */

namespace App\Http\Controllers;

use App\Repo\CountryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use App\Repo\CountryTranslationInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CountryTranslationController extends Controller
{
    protected $countryTranslation;
    protected $country;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CountryTranslationInterface $countryTranslation,CountryInterface $country)
    {
        $this->countryTranslation=$countryTranslation;
        $this->country=$country;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $country= $this->countryTranslation->countryTranslationGetAll();
            try{
                if(!$country->first()){
                    throw new \Exception();
                }
            }
            catch (\Exception $ex){
                return response()->json([
                    'status'=>'200',
                    "message"=>'Empty Record'
                ],200);
            }
            return response()->json([
                'status'=>'200',
                'country-translation'=>$country
            ],200);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'404',
                'message'=>'error while getting records'
            ],404);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status=Input::get('status','1');
        try{
            $this->validate($request, [
                'name' => 'required',
                'country_id' => 'required|integer',
                'lang' => 'required|alpha-dash',
            ]);
            $request=$request->all();
            try {
                $this->country->countryGetSpecific($request['country_id'],$status) ;
            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'404',
                    'message'=>"Country  could not be found"
                ],404);
            }
            $country= $this->countryTranslation->countryTranslationGetSpecificByName(strtolower($request['name']));

            try {
                if ($country->count() != 0) {

                    throw new \Exception();
                }
            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'409',
                    'message'=>"Country Translation could not be created due to duplicate entry"
                ],409);
            }
            $country=$this->countryTranslation->countryTranslationCreate($request);
            return response()->json([
                'status'=>'200',
                'message'=>'Country Translation created successfully',
                'Country Translation'=>$country
            ],200);

        }

        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $country= $this->countryTranslation->countryTranslationGetSpecific($id);
            return response()->json([
                'status'=>'200',
                'country-translation'=>$country
            ],200);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Country Translation could not be found"
            ],404);
        }
        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Country Translation could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Country Translation could not be found"
            ],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'name' => 'required|alpha',
                'country_id' => 'required|integer',
                'lang' => 'required|alpha-dash',
            ]);
            $request = $request->all();
            $country= $this->countryTranslation->countryTranslationGetSpecificByName(strtolower($request['name']));

            try {
                if ($country->count() != 0) {

                    throw new \Exception();
                }
            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'409',
                    'message'=>"Country Translation could not be created due to duplicate entry"
                ],409);
            }
            $country = $this->countryTranslation->countryTranslationUpdate($id, $request);
            return response()->json([
                'status' => '200',
                'message' => 'Country Translation Updated successfully',
                'countryTranslation' => $country
            ]);

        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'409',
                'message'=>"Country Translation could not be created due to duplicate entry"
            ],409);
        }

        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Country could not be found"
            ],404);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $country= $this->countryTranslation->countryTranslationDelete($id);
            return response()->json([
                'status'=>'200',
                'message'=>"Country Translation deleted successfully"
            ]);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Country Translation could not be found"
            ],404);
        }
        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Country Translation could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Country Translation could not be found"
            ],404);
        }
    }

}