<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/25/2017
 * Time: 10:51 PM
 */

namespace App\Http;


use App\CityTranslation;
use App\CountryTranslation;
use App\DistrictTranslation;
use App\Repo\CityInterface;
use App\Repo\CountryInterface;
use App\Repo\DistrictInterface;
use Geocoder\Query\GeocodeQuery;
use Geocoder\Query\ReverseQuery;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Config;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Redis;

/**
 * Class RoleCheckerFacade
 * @package App\Http
 */
class GeoLocationCheckerFacade
{
    private $district;
    private $city;
    protected $country;
    private $countryTranslation;
    private $cityTranslation;
    private $districtTranslation;

    /**
     * RemoteCallFacade constructor.
     * @param Client $guzzle
     */
    public function __construct(DistrictInterface $district, CityInterface $city,
                                CountryInterface $country, CountryTranslation $countryTranslation, CityTranslation $cityTranslation,DistrictTranslation $districtTranslation)
    {
        $this->district = $district;
        $this->city = $city;
        $this->country = $country;
        $this->countryTranslation = $countryTranslation;
        $this->cityTranslation = $cityTranslation;
        $this->districtTranslation = $districtTranslation;
    }

    private function getCountry($name = '')
    {
        $country_index = null;

        // fetch countries from database
        $all_countries = $this->country->countryGetAllForDatatables();
        $countries = [];
        foreach ($all_countries as $key => $country) {
            $countries[$key]['id'] = $country['id'];
            $translation = $country->countryTranslation->where("lang", "en")->first();
            $countries[$key]['name'] = $translation['name'];
        }

        if ($name) {
            $name = strtolower(preg_replace('/\s+/', '', $name));
            $name = str_replace('-', '', $name);

            foreach ($countries as $key => $country) {
                $country_name = strtolower(preg_replace('/\s+/', '', $country['name']));

                if ($country_name === $name) $country_index = $key;
            }

        }

        if ($country_index !== null && isset($countries[$country_index])) {
            return $countries[$country_index];
        }

        return false;
    }

    /**
     * Fetch cities associated with the country and compare with the reverse geocoder result
     *
     * @param int $country_id
     * @param string $name
     * @return bool
     */
    private function getCity($country_id, $name = '')
    {

        if ($country_id && $name) {
            $city_index = null;


            // fetch cities associated with country id from database
            $city_data = $this->city->cityGetAllByCountryForDataTable($country_id);
            $cities = [];
            foreach ($city_data as $key => $city) {
                $cities[$key]['id'] = $city['id'];
                $translation = $city->cityTranslation->where("lang_code", "en")->first();
                $cities[$key]['name'] = $translation['name'];
                $cities[$key]['country_id'] = $country_id;
            }

            $name = strtolower(preg_replace('/\s+/', '', $name));
            $name = str_replace('-', '', $name);
            $name = explode("''",$name);
            $name = $name[0];
            foreach ($cities as $key => $city) {
                $city_name = strtolower(preg_replace('/\s+/', '', $city['name']));
                $city_name = explode("''",$city_name);
                $city_name = $city_name[0];

                if ($city_name === $name) $city_index = $key;
            }

            if ($city_index !== null && isset($cities[$city_index])) {
                return $cities[$city_index];
            }

        }

        return false;
    }


    /**
     * Fetch district associated with the city and compare with the reverse geocoder result
     *
     * @param int $city_id
     * @param string $name
     * @return bool
     */
    private function getDistrict($city_id, $name = '')
    {
        if ($city_id && $name) {
            $district_index = null;

            // fetch districts associated with city from database
            $district_data = $this->district->districtGetAllForDataTable($city_id);
            $districts = [];
            foreach ($district_data as $key => $district) {
                $districts[$key]['id'] = $district['id'];
                $translation = $district->districtTranslation->where("lang_code", "en")->first();
                $districts[$key]['name'] = $translation['name'];
                $districts[$key]['city_id'] = $city_id;
            }
//            $name = strtolower(preg_replace('/\s+/', '', $name));
//            $name = str_replace('-', '', $name);
//            $name = str_replace('district', '', $name);
//            $name = explode("'",$name);

//            $name = str_replace('Al Mohammadiyyah', 'Al Mohammadiyah', $name);
            $rate = [];
//            dd($districts);
            foreach ($districts as $key => $district) {
//dd($district['name']);
//                $district_name = strtolower(preg_replace('/\s+/', '', $district['name']));
//                $district_name = str_replace('-', '', $district_name);
//                $district_name = explode("'",$district_name);
//                $district_name = $district_name[0];
                similar_text(strtolower(preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $name)),strtolower(preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $district['name'])),$percent);
//                $distance = levenshtein( strtolower(preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $name)),strtolower(preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $district['name'])));
//                $ratio = $distance/$percent;
//                $rate[] = [
//                    'original_name' => $name,
//                    'name' => $district['name'] ,
//                    'percentage' => $percent,
//                    'distance' => $distance,
//                    'ratio' => $ratio
//                ];
                if ($percent >= 90) $district_index = $key;
            }
//dd($districts[$district_index]);
//            dd($rate);

            $name = strtolower(preg_replace('/\s+/', '', $name));
            $name = str_replace('-', '', $name);
            $name = str_replace('district', '', $name);
            $name = explode("'",$name);
            $name = $name[0];
//            $name = str_replace('Al Mohammadiyyah', 'Al Mohammadiyah', $name);
//            $rate1 = [];
            if( $district_index === null ){
                $an_name = str_replace('al', 'an', $name );
                $al_name = str_replace('an', 'al', $name );
                $az_name = str_replace('az', 'al', $name );
                $az_al_name = str_replace('al', 'az', $name );
                $ar_name = str_replace('ar', 'al', $name );
                $ar_al_name = str_replace('al', 'ar', $name );
                foreach ($districts as $key => $district) {
                    $district_name = strtolower(preg_replace('/\s+/', '', $district['name']));
                    $district_name = str_replace('-', '', $district_name);
                    similar_text($district_name,strtolower($an_name),$percent1);
                    similar_text($district_name,strtolower($al_name),$percent2);
                    similar_text($district_name,strtolower($az_al_name),$percent3);
                    similar_text($district_name,strtolower($ar_al_name),$percent4);
                    similar_text($district_name,strtolower($az_name),$percent5);
                    similar_text($district_name,strtolower($ar_name),$percent6);
                    $avg_percentage = ($percent1 + $percent2 + $percent3 + $percent4 + $percent5 + $percent6)/6;
//                    $rate1[] = [
//                        'name' => $district_name,
//                        'avg' => $avg_percentage,
//                        'p' => [$percent1,$percent2,$percent3,$percent4,$percent5,$percent6]
//
//                    ];
                    if ($avg_percentage >= 80 ){
                        $district_index = $key;
                        break;
                    }

//                    elseif ($percent1 >= 80) {
//                        $district_index = $key;
//                        break;
//                    }
//                    elseif ($percent2 >= 80){
//                        $district_index = $key;
//                        break;
//                    }
//                    elseif ($district_name === $az_al_name) {
//                        $district_index = $key;
//                        break;
//                    }
//                    elseif ($district_name === $ar_name) {
//                        $district_index = $key;
//                        break;
//                    }
//                    elseif ($district_name === $ar_al_name) {
//                        $district_index = $key;
//                        break;
//                    }
                }
            }
            if ($district_index !== null && isset($districts[$district_index])) {
                return $districts[$district_index];
            }

        }

        return false;
    }

    private function isEnglishLang($string)
    {
        if (!preg_match('/[^\W_ ] /', $string)) {
            return false;
        }

        return true;
    }

    /**
     * Fetch, validate the location using coordinates
     * @return array
     */
    public function fetch($lng, $lat, $lang)
    {
        try {
            /**
             * Getting Shipping Address Limit from settings in general service
             */
            $data = \Settings::getSettings('google-coordinate-to-detail-api-key');
            if ($data["status"] != 200) {
                return response()->json(
                    $data["message"]
                    , $data["status"]);
            }
            $api_key = \Redis::get('google-coordinate-to-detail-api-key');
//        $api_key = 'AIzaSyDQOj1N10Nt7igBzgllQF_46l8EVmklzIo';

            $response = array(
                'status' => 422,
                'message' => ["coordinates"=>['Error fetching location, please try again.']]
            );

            $adapter = new \Http\Adapter\Guzzle6\Client();
            $provider = new \Geocoder\Provider\GoogleMaps\GoogleMaps($adapter, null, $api_key);
            $geocoder = new \Geocoder\StatefulGeocoder($provider, 'en');


            $result = $geocoder->reverseQuery(ReverseQuery::fromCoordinates($lat, $lng));

            if ($result->count() > 0) {
                $country = array();
                $city = array();
                $district = array();
                $street = '';

                // break loop flag; only used if response is not in english language
                $break_loop = true;

                foreach ($result as $location) {
                    // find country in our system
//                    return $location->getCountry()->getName();
                    if ($location->getCountry()->getName()) {
                        $break_loop = $this->isEnglishLang($location->getCountry()->getName()) ? true : false;
                        $country = $this->getCountry($location->getCountry()->getName());

                        if ($country) {
                            // specific locality for saudi arabia
                            if ($country['id'] === 1) {
                                $break_loop = $this->isEnglishLang($location->getLocality()) ? true : false;
                                if ($location->getLocality()) {
                                    $city = $this->getCity($country['id'], $location->getLocality());

                                    // fetch district associated
                                    if (count($city)) {
                                        if ($location->getSubLocality()) {
                                            $break_loop = $this->isEnglishLang($location->getSubLocality()) ? true : false;

                                            foreach ($result as $key => $dist) {
//                                                if($key == 0) continue;
                                                $district = $this->getDistrict($city['id'], $dist->getSubLocality());
                                                if( $district !== false ) break;
                                            }
                                        }

                                        if ($location->getStreetName()) {
                                            $street = $location->getStreetName();
                                        }
                                    }
                                }
                            } else {
                                // fetch city
                                if ($location->getAdminLevels()) {
                                    foreach ($location->getAdminLevels() as $admin_level_location) {

                                        $city = $this->getCity($country['id'], $admin_level_location->getName());
                                        if( $city !== false ) break;
                                    }
                                    // fetch district associated
                                    if (count($city)) {
                                        if ($location->getLocality()) {
                                            $district = $this->getDistrict($city['id'], $location->getLocality());
                                        }
                                        if ($location->getSubLocality()) {
                                            $street_name = ( $location->getStreetName() )? $location->getStreetName() .', ' : '';
                                            $street =  $street_name . $location->getSubLocality();
                                        }


                                    }
                                }
                            }
                        }
                    }
                    if ($break_loop) break;

                }
                if (count($country) > 1 && count($city) > 1 && count($district) > 1) {
                    if($lang != "en"){
                        $translation = $this->countryTranslation->where([["country_id",$country['id']],["lang",$lang]])->first();
                        if($translation != null){
                            $country['name'] = $translation['name'];
                        }
                        $translation = $this->cityTranslation->where([["city_id",$city['id']],["lang_code",$lang]])->first();
                        if($translation != null){
                            $city['name'] = $translation['name'];
                        }
                        $translation = $this->districtTranslation->where([["district_id",$district['id']],["lang_code",$lang]])->first();
                        if($translation != null){
                            $district['name'] = $translation['name'];
                        }
                    }

                    $response = array(
                        'status' => 200,
                        'payload' => array(
                            'country' => [
                                'country_id' => $country['id'],
                                'name' => $country['name']
                            ],
                            'city' => [
                                'city_id' => $city['id'],
                                'name' => $city['name']
                            ],
                            'district' => [
                                'district_id' => $district['id'],
                                'name' => $district['name']
                            ],
                            'street' => $street
                        )
                    );
                } else {
                    $response = array(
                        'status' => 422,
                        'message' => ["coordinates"=>['Unfortunately we could not find your location, please fill out the fields manually.']]
                    );
                }


            }
        } catch (\Exception $e) {

            $response = array(
                'status' => 422,
                'message' => $e->getMessage()
            );

        }

        return $response;
    }

    public function fetchCoordinate($locationName)
    {
        try {
            $count = 0;
            addSaudiSuffix:
            if($count >= 0){
                $locationName = $locationName . '+'. 'saudi+arabia';
            }

//        $locationName = trim(preg_replace("/[^ \w]+/", '', $locationName));
            $data = \Settings::getSettings('google-coordinate-to-detail-api-key');
            if ($data["status"] != 200) {
                return response()->json(
                    $data["message"]
                    , $data["status"]);
            }
            $api_key = \Redis::get('google-coordinate-to-detail-api-key');

//        $api_key = "AIzaSyDoz-YDNS31TXHVeRHS_KL3h7IwZXJof2k";
            $response = array(
                'status' => 422,
                'message' => ["location"=>['Error fetching location, please try again.']]
            );

            $adapter = new \Http\Adapter\Guzzle6\Client();
            $provider = new \Geocoder\Provider\GoogleMaps\GoogleMaps($adapter, null, $api_key);
            $geocoder = new \Geocoder\StatefulGeocoder($provider, 'en');


            $result = $geocoder->geocodeQuery(GeocodeQuery::create($locationName));
            if ($result->count() > 0) {
                $lat = 0;
                $lng = 0;
                $address = '';

                foreach ($result as $location) {

                    if (count($location->getCoordinates())) {
                        $lat = $location->getCoordinates()->getLatitude();
                        $lng = $location->getCoordinates()->getLongitude();
                        $address = $location->getFormattedAddress();
                        break;
                    }
                }

                if ($lat > 0 && $lng > 0 && $address) {
                    $response = array(
                        'status' => 200,
                        'payload' => array(
                            'lat' => $lat,
                            'lng' => $lng,
                            'address' => $address,
                        )
                    );
                }
                elseif ($count === 0){
                    $count++;
                    goto addSaudiSuffix;
                }
                else {
//                dd($count);
                    $response = array(
                        'status' => 422,
                        'message' => ["location"=>['Unfortunately we could not find your location, please fill out the fields manually.']]
                    );
                }


            }
//            print_r( $location );
        } catch (Exception $e) {

            $response = array(
                'status' => 422,
                'message' => $e->getMessage()
            );

        }

        return $response;
    }


}