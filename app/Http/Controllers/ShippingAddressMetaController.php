<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 5:24 PM
 */

namespace App\Http\Controllers;
use App\Repo\ShippingAddressInterface;
use App\Repo\ShippingAddressMetaInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ShippingAddressMetaController extends Controller
{
    protected $shippingAddressMeta;
    protected $shippingAddres;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ShippingAddressMetaInterface $shippingAddressMeta,ShippingAddressInterface $shippingAddress)
    {
        $this->shippingAddressMeta=$shippingAddressMeta;
        $this->shippingAddress=$shippingAddress;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $shippingAddressMeta= $this->shippingAddressMeta->shippingAddressMetaGetAll();
            try{
                if(!$shippingAddressMeta->first()){
                    throw new \Exception();
                }
            }
            catch (\Exception $ex){
                return response()->json([
                    'status'=>'200',
                    "message"=>'Empty Record'
                ],200);
            }
            return response()->json([
                'status'=>'200',
                'shipping_address_meta'=>$shippingAddressMeta
            ],200);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'404',
                'message'=>'error while getting records'
            ],404);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{
            $this->validate($request, [
                'meta_key' => 'required',
                'shipping_address_id' => 'required|integer',
                'meta_value' => 'required',
            ]);
            $request=$request->all();
            try {
                $this->shippingAddress->shippingAddressGetSpecific($request['shipping_address_id']) ;
            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'404',
                    'message'=>"Shipping Address  could not be found"
                ],404);
            }
            $shippingAddressMeta=$this->shippingAddressMeta->shippingAddressMetaCreate($request);
            return response()->json([
                'status'=>'200',
                'message'=>'Shipping Address Meta created successfully',
                'shipping_address_meta'=>$shippingAddressMeta
            ],200);
        }

        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $shippingAddress= $this->shippingAddressMeta->shippingAddressMetaGetSpecific($id);
            return response()->json([
                'status'=>'200',
                'shipping_address_meta'=>$shippingAddress
            ],200);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Shipping Address Meta could not be found"
            ],404);
        }
        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Shipping Address Meta could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Shipping Address Meta could not be found"
            ],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'meta_key' => 'required|alpha',
                'shipping_address_id' => 'required|integer',
                'meta_value' => 'required|alpha',
            ]);
            $request = $request->all();
            $shippingAddress = $this->shippingAddressMeta->shippingAddressMetaUpdate($id, $request);
            return response()->json([
                'status' => '200',
                'message' => 'Shipping Address Meta Updated successfully',
                'shipping_address_meta' => $shippingAddress
            ]);

        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'409',
                'message'=>"Shipping Address Meta could not be updated due to duplicate entry"
            ],409);
        }

        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Shipping Address could not be found"
            ],404);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $shippingAddress= $this->shippingAddressMeta->shippingAddressMetaDelete($id);
            return response()->json([
                'status'=>'200',
                'shipping_address_meta'=>"Shipping Address Meta deleted successfully"
            ]);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Shipping Address Meta could not be found"
            ],404);
        }
        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Shipping Address Meta could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Shipping Address Meta could not be found"
            ],404);
        }
    }

}