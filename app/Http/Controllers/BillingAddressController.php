<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/16/2017
 * Time: 11:03 AM
 */

namespace App\Http\Controllers;

use App\Repo\BillingAddressInterface;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use RoleChecker;
use Illuminate\Support\Facades\Input;
use Validator;
use Illuminate\Support\Facades\Config;
class BillingAddressController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $billingAddress;
    public function __construct(
        BillingAddressInterface $billingAddress


    )
    {
        $this->billingAddress=$billingAddress;

    }

    /**
     * Display a listing of the resource from table named .
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            if(RoleChecker::hasRole("customer")) {
                $userId = Rolechecker::getUser();
                $billingAddress =$this->billingAddress->billingAddressGetSpecificByUserId($userId);
            }else{

                $limit["limit"] = Input::get("limit",10);
                /*
                         * validation of limit param if limit param is not null
                         * */
                if (!is_null($limit['limit'])) {
                    $rules = [
                        "limit" => "sometimes|numeric|min:1",
                    ];
                    $validator = Validator::make($limit, $rules);
                    if ($validator->fails()) {
                        $request['limit'] = 10;
                    }
                }
                $billingAddress=$this->billingAddress->billingAddressGetAll($limit["limit"]);
                $billingAddress->appends(["limit" => $limit["limit"]])->withPath("/billing/address");
            }

            try{
                if($billingAddress->first()==null){
                    throw new \Exception();
                }
            }
            catch (\Exception $ex){
                return response()->json([
                    'status'=>'404',
                    'message'=>'Empty Record'
                ],404);

            }
            return response()->json([
                'status'=>'200',
                'data'=>$billingAddress
            ],200);

        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>'Empty Record'
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>'error while getting records'
            ],404);
        }
    }

    /**
     * Display a listing of the billing address for admin .
     *
     * @return \Illuminate\Http\Response
     */
    public function adminIndex()
    {
        try{
            if(RoleChecker::hasRole("admin") ||RoleChecker::hasRole("super-admin") ) {
                $billingAddress=$this->billingAddress->billingAddressGetAllForDatatable();
                try{
                    if($billingAddress->first()==null){
                        throw new \Exception();
                    }
                }
                catch (\Exception $ex){
                    return response()->json([
                        'status'=>'404',
                        'message'=>'Empty Record'
                    ],404);

                }
              return \Datatables::of($billingAddress)->make(true);
            }
            else{
                return response()->json([
                    "status" => "403",
                    "message" => "Only Admin or Super Admin has access "
                ],403);
            }
   }
        catch (ModelNotFoundException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>'Empty Record'
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>'error while getting records'
            ],404);
        }
    }

    /**
     * Store a newly created resource in table name .
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        try{

            if(RoleChecker::hasRole("customer")){
                $this->validate($request, [
                    'address_name'=>'required',
                    'first_name'=>'required',
                    'last_name'=>'required',
                    'email'=>'required|email',
                    'postal_code'=>'required|numeric',
                    'address_line_one'=>'required',
                    'city'=>'required',
                    'district'=>'required',
                    'country'=>'required',
                    'primary' => 'integer|min:0|max:1',
                ]);
                $request["user_id"] = Rolechecker::getUser();
            }
            else{
                $this->validate($request,[
                    'user_id'=>'required|integer',
                    'address_name'=>'required',
                    'first_name'=>'required',
                    'last_name'=>'required',
                    'email'=>'required|email',
                    'postal_code'=>'required|numeric',
                    'address_line_one'=>'required',
                    'city'=>'required',
                    'district'=>'required',
                    'country'=>'required',
                    'primary' => 'integer|min:0|max:1',
                ]);
            }
            $billingAddressLimit = Config::get('config.billing_address_limit');
            /**
             * Compares if the shipping address limit is less or equals to the records obtained for the passed user
             * */
            if($billingAddressLimit <=  count($this->billingAddress->billingAddressGetSpecificByUserId( $request["user_id"]))){
                return response()->json([
                    "status" => "403",
                    "message" => "More than three $billingAddressLimit addresses per user is not allowed"
                ],403);
            }
            $request['primary']=$request->input('primary',1);
            $billingAddress=$this->billingAddress->billingAddressCreate($request->all());
            return response()->json([
                'status'=>'200',
                'message'=>'Billing Address Created Successfully',
            ]);
        }
        catch (ModelNotFoundException $e){
            return response()->json([
                'status'=>'500',
                'message'=>"Error Creating Billing Address"
            ],500);
        }

        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

    /**
     * Display the billing address of all user.user with role customer cannot have access
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function adminShow($id)
    {
        try {
            $billingAddress=$this->billingAddress->billingAddressGetSpecific($id) ;

            if(!RoleChecker::hasRole('customer')){
                return response()->json([
                    'status'=>'200',
                    'data'=>$billingAddress
                ]);
            }
            else{
                return response()->json([
                    "status" => "403",
                    "message" => "You have no access"
                ],403);
            }

        }catch ( ModelNotFoundException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Billing Address could not be found'
            ],404);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $billingAddress=$this->billingAddress->billingAddressGetSpecific($id) ;
            $userId = Rolechecker::getUser();
                if($userId !== $billingAddress["user_id"]){
                    return response()->json([
                        "status" => "403",
                        "message" => "You are only allowed to view your information only"
                    ],403);
                }
            return response()->json([
                'status'=>'200',
                'data'=>$billingAddress
            ]);
        }catch ( ModelNotFoundException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Billing Address could not be found'
            ],404);
        }
    }


    /**
     * Update the specified resource in storage in table named.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->billingAddress->billingAddressGetSpecific($id);
        }catch (\Exception $ex){
            return response()->json([
                "status" => "404",
                "message" => "Billing Address Not found"
            ],404);
        }

        try {
            if(RoleChecker::hasRole("customer")){
                $this->validate($request, [
                    'address_name'=>'required',
                    'first_name'=>'required',
                    'last_name'=>'required',
                    'email'=>'required|email',
                    'postal_code'=>'required|numeric',
                    'address_line_one'=>'required',
                    'city'=>'required',
                    'district'=>'required',
                    'country'=>'required',
                    'primary' => 'integer|min:0|max:1',
                ]);
                $request["user_id"] = Rolechecker::getUser();
            }
            else{
                $this->validate($request,[
                    'user_id'=>'required|integer',
                    'address_name'=>'required',
                    'first_name'=>'required',
                    'last_name'=>'required',
                    'email'=>'required|email',
                    'postal_code'=>'required|numeric',
                    'address_line_one'=>'required',
                    'city'=>'required',
                    'district'=>'required',
                    'country'=>'required',
                    'primary' => 'integer|min:0|max:1',
                ]);
            }
            $billingAddressLimit = Config::get('config.billing_address_limit');
            /**
             * Compares if the shipping address limit is less or equals to the records obtained for the passed user
             * */
            if($billingAddressLimit <=  count($this->billingAddress-> billingAddressGetAllShippingAddressExceptPassedId($id, $request["user_id"]))){
                return response()->json([
                    "status" => "403",
                    "message" => "More than three $billingAddressLimit addresses per user is not allowed"
                ],403);
            }
            $request = $request->all();
            $billingAddress = $this->billingAddress->billingAddressUpdate($id, $request);
            return response()->json([
                'status' => '200',
                'message' => 'Billing Address Updated successfully'
            ]);

        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Billing Address Not found"
            ],404);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

    /**
     * Remove billing address if user_id match with the data $id and user_id .
     *
     * @param  int  $id
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {

        try {
            if(RoleChecker::hasRole("customer")){
                $request["user_id"] = Rolechecker::getUser();
            }else{
                try {
                    $this->validate($request, [
                        'user_id' => 'required|integer']);
                } catch (\Exception $e){
                    return response()->json([
                        'status'=>'422',
                        'message'=>$e->response->original
                    ],422);
                }
            }
            $billingAddress=$this->billingAddress->billingAddressGetSpecific($id) ;
            if($billingAddress->user_id!==$request['user_id']){
                throw new \Exception();
            }
            $this->billingAddress->billingAddressDelete($id);
            return response()->json([
                'status'=>'200',
                'message'=>"Requested Billing address deleted successfully"
            ]);
        }catch ( ModelNotFoundException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Billing Address could not be found'
            ],404);
        }

        catch (\Exception $ex){
            return response()->json([
                'status'=>'403',
                'message'=>'User does not match with the requested  id'
            ],403);
        }
    }
}