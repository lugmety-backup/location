<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:10 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class DistrictTranslation extends Model
{
    protected $guarded=['id'];
    protected $table='district_translation';
    public $timestamps = false;
    public function district(){
        return $this->belongsTo('App\District','district_id');
    }
    public function setLangCodeAttribute($value){
        $this->attributes['lang_code'] = strtolower($value);
    }

}