<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:16 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class ShippingAddressAdditionalField extends Model
{
    protected $guarded=['id'];
    protected $table='shipping_address_additional_field';
    protected $fillable = ["shipping_address_id","customer_name","customer_email","customer_phone"];
    protected $hidden=array('shipping_address_id');
    public $timestamps = false;
    public function shippingAddress(){
        return $this->belongsTo('App\ShippingAddress','shipping_address_meta');
    }


}