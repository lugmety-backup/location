<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:16 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class ShippingAddressMeta extends Model
{
    protected $guarded=['id'];
    protected $table='shipping_address_meta';
    protected $fillable = ["shipping_address_id","meta_key","meta_value"];
    protected $hidden=array('shipping_address_id','updated_at',"created_at");
    public $timestamps = false;
    public function shippingAddress(){
        return $this->belongsTo('App\ShippingAddress','shipping_address_meta');
    }


}