<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('district_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('district_id');
            $table->foreign('district_id')->references('id')->on('district')->onDelete('cascade');
            $table->string('lang_code',20);
            $table->string('name',30);
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('district_translation');
    }
}
