<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/16/2017
 * Time: 10:39 AM
 */

namespace App\Repo;


interface BillingAddressInterface
{
    public function billingAddressGetAllForDatatable();

    public function billingAddressGetAll($limit);

    public function billingAddressGetSpecific($id);

    public function billingAddressGetSpecificByUserId($userId);

    public function billingAddressCreate(array $attributes);

    public function billingAddressUpdate($id, array $attributes);

    public function billingAddressGetAllShippingAddressExceptPassedId($id,$userId);

    public function billingAddressDelete($id);
}