<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:02 AM
 */

namespace App\Repo\Eloquent;


use App\Country;
use App\Language;
use App\Repo\CountryLanguageInterface;

class CountryLanguageRepo implements CountryLanguageInterface
{
    private $country;
    private $language;
    /**
     * CountryRepo constructor.
     * @param $country
     */
    public function __construct(Country $country,Language $language)
    {
        $this->country = $country;
        $this->language = $language;
    }

    public function getAllLanguageOfCountry($id)//get all the country information from database with status 1
    {
        return $this->country->find($id)->language->where('status',1);
    }

    public function getAllCountryByLanguage($id)//get all the country information from database with status 1
    {
        return $this->language->find($id)->country;
    }

    public function getSpecificLanguageOfCountryByLanguage($id,$code)
    {
        return $this->country->find($id)->language->where('code', $code);
    }


    public function attachCountryToLanguage($id,$request)
    {
        $country = $this->country->find($id);
        return $country->language()->attach($request);
    }
    public function dettachCountryToLanguage($id,$request)
    {
        $country = $this->country->find($id);
        return $country->language()->detach($request);
    }

    public function dettachCountryWithAllLanguage($id)
    {
        $langs=$this->country->find($id)->language;
        foreach ($langs as $lang){
            $this->country->find($id)->language()->detach($lang->id);
        }
    }

    //    public function countryGetSpecificByCode($code){
//        return $this->country->where('code',$code)->get();
//    }
//
//    public function countryGetSpecific($id)
//    {
//        return $this->country->findOrFail($id);
//    }
//
//    public function countryCreate(array $attributes)
//    {
//        return $this->country->create($attributes);
//    }
//
//    public function countryUpdate($id, array $attributes)
//    {
//        $con=$this->country->findOrFail($id)->update($attributes);
//        $con=$this->country->findOrFail($id)->fill($attributes);
//        return $con;
//    }
//
//    public function countryDelete($id)
//    {
//        $data=$this->country->findOrFail($id)->delete();
//        return true ;
//
//    }

}