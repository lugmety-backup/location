<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 5:02 PM
 */

namespace App\Http\Controllers;


use App\Repo\CountryInterface;
use App\Repo\LanguageInterface;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Repo\CountryLanguageInterface;


class CountryLanguageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $countrylanguage;
    private $country;
    private $language;

    public function __construct(CountryLanguageInterface $countryLanguage, CountryInterface $country, LanguageInterface $language)
    {
        $this->countrylanguage=$countryLanguage;
        $this->country=$country;
        $this->language=$language;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

        try{

            $countrylanguage= $this->countrylanguage->getAllLanguageOfCountry($id);
            try{
                if(!$countrylanguage->first()){
                    throw new \Exception();
                }
            }
            catch (\Exception $ex){
                return response()->json([
                    'status'=>'200',
                    "message"=>'Empty Record'
                ],200);
            }
            return response()->json([
                'status'=>'200',
                'language'=>$countrylanguage
            ],200);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'404',
                'message'=>'Country Could not found'
            ],404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,Request $requests)
    {
        try{
            $this->validate($requests, [
                'language' => 'required'
            ]);
            $country=$this->country->countryGetSpecific($id);
            try {
                if (count($country) == 0) {
                    throw new \Exception();
                }
            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'404',
                    'message'=>"Country could not be found"
                ],409);
            }
            foreach ($requests['language'] as $request) {
                try {
                    $language= $this->language->languageGetSpecificByCode($request);
                    if (count($language) == 0) {
                        throw new \Exception();
                    }
                }
                catch (\Exception $ex){
                    return response()->json([
                        'status'=>'404',
                        'message'=>"Language could not be found"
                    ],409);
                }
                $request=(string)$language[0]->id;
                $countrylanguage = $this->countrylanguage->attachCountryToLanguage($id, $request);
            }
            return response()->json([
                'status'=>'200',
                'message'=>'Country attached successfully to languages'
            ],200);

        }

        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $requests)
    {
        try{
            $this->validate($requests, [
                'language' => 'required'
            ]);
            $country=$this->country->countryGetSpecific($id);

            try {
                if (count($country) == 0) {
                    throw new \Exception();
                }
            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'404',
                    'message'=>"Country could not be found"
                ],409);
            }
            foreach ($requests['language'] as $request) {
                try {
                    $language = $this->language->languageGetSpecific($request);
                    if (count($language) == 0) {
                        throw new \Exception();
                    }
                }
                catch (\Exception $ex){
                    return response()->json([
                        'status'=>'404',
                        'message'=>"Language could not be found"
                    ],409);
                }
                $countrylanguage = $this->countrylanguage->dettachCountryToLanguage($id, $request);
            }
            return response()->json([
                'status'=>'200',
                'message'=>'Country detached successfully',
            ],200);

        }

        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

}