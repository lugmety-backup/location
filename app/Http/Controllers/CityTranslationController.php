<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/13/2017
 * Time: 12:27 PM
 */

namespace App\Http\Controllers;


use App\Repo\CityInterface;
use App\Repo\CityTranslationInterface;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CityTranslationController extends Controller
{
    protected $cityTranslation;
    protected $city;

    /**
     * CityTranslationController constructor.
     * @param $cityTranslation
     * @param $city
     */
    public function __construct(CityTranslationInterface $cityTranslation, CityInterface $city)
    {
        $this->cityTranslation = $cityTranslation;
        $this->city = $city;
    }
    public function index()
    {
        try{
            $city= $this->cityTranslation->cityTranslationGetAll();
            try{
                if(!$city->first()){
                    throw new \Exception();
                }
            }
            catch (\Exception $ex){
                return response()->json([
                    'status'=>'200',
                    "message"=>'Empty Record'
                ],200);
            }
            return response()->json([
                'status'=>'200',
                'city-translation'=>$city
            ],200);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'404',
                'message'=>'error while getting records'
            ],404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'name' => 'required',
                'city_id' => 'required|integer',
                'lang_code' => 'required|alpha-dash',
            ]);
            $request=$request->all();
            try {
                $this->city->cityGetSpecific($request['city_id']) ;
            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'404',
                    'message'=>"City  could not be found"
                ],404);
            }
            $city= $this->cityTranslation->cityTranslationGetSpecificByCode(strtolower($request['name']));

            try {
                if ($city->count() != 0) {

                    throw new \Exception();
                }
            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'409',
                    'message'=>"City Translation could not be created due to duplicate entry"
                ],409);
            }
            $city=$this->cityTranslation->cityTranslationCreate($request);
            return response()->json([
                'status'=>'200',
                'message'=>'City Translation created successfully',
                'city-translation'=>$city
            ],200);

        }

        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $city= $this->cityTranslation->cityTranslationGetSpecific($id);
            return response()->json([
                'status'=>'200',
                'city-translation'=>$city
            ],200);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested City Translation could not be found"
            ],404);
        }
        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested City Translation could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested City Translation could not be found"
            ],404);
        }
    }
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                'city_id' => 'required|integer',
                'lang_code' => 'required|alpha-dash',
            ]);
            $request = $request->all();
            $city= $this->cityTranslation->cityTranslationGetSpecificByCode(strtolower($request['name']));

            try {
                if ($city->count() != 0) {

                    throw new \Exception();
                }
            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'409',
                    'message'=>"City Translation could not be created due to duplicate entry"
                ],409);
            }
            $city = $this->cityTranslation->cityTranslationUpdate($id, $request);
            return response()->json([
                'status' => '200',
                'message' => 'City Translation Updated successfully',
                'country-translation' => $city
            ]);

        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'409',
                'message'=>"City Translation could not be created due to duplicate entry"
            ],409);
        }

        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested City could not be found"
            ],404);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

    public function destroy($id)
    {
        try{
            $city= $this->cityTranslation->cityTranslationDelete($id);
            return response()->json([
                'status'=>'200',
                'message'=>"City Translation deleted successfully"
            ]);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested city Translation could not be found"
            ],404);
        }
        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested city Translation could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested city Translation could not be found"
            ],404);
        }
    }

}