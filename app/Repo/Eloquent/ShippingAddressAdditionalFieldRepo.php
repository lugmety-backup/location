<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 5:18 PM
 */

namespace App\Repo\Eloquent;


use App\Repo\ShippingAddressAdditionalFieldInterface;
use App\ShippingAddressAdditionalField;

class ShippingAddressAdditionalFieldRepo implements ShippingAddressAdditionalFieldInterface
{
    private $shippingAddressAdditionalField;

    /**
     * CountryRepo constructor.
     * @param $countryAdditionalField
     */
    public function __construct(ShippingAddressAdditionalField $shippingAddressAdditionalField)
    {
        $this->shippingAddressAdditionalField = $shippingAddressAdditionalField;
    }

    public function shippingAddressAdditionalFieldGetAll()//get all the shippingAddressAdditionalField information from database
    {
        // TODO: Implement countryAdditionalFieldGetAll() method.

        return $this->shippingAddressAdditionalField->all();
    }


    public function shippingAddressAdditionalFieldGetAllByShippingAddressId($id)
    {
        return $this->shippingAddressAdditionalField->where('shipping_address_id',$id)->get();
    }

    public function shippingAddressAdditionalFieldGetSpecific($id)
    {
        return $this->shippingAddressAdditionalField->findOrFail($id);
    }

    public function shippingAddressAdditionalFieldUpdateOrCreate(array $attributes)
    {
        $shippingAdditionalField=$this->shippingAddressAdditionalField->where([
            ['shipping_address_id',$attributes['shipping_address_id']]
            , ['meta_key',$attributes['meta_key']]
        ])->get();
        if($shippingAdditionalField->count()!=0){
            $con=$this->shippingAddressAdditionalField->where([
                ['shipping_address_id',$attributes['shipping_address_id']]
                , ['meta_key',$attributes['meta_key']]
            ])->update($attributes);
        }
        else{
            $con=$this->shippingAddressAdditionalField->create($attributes);
        }
        return $con;
    }

    public function shippingAddressAdditionalFieldGetSpecificByKey($key, $id)
    {
        return $this->shippingAddressAdditionalField->where([['shipping_address_id',$id],['meta_key',$key]])->get();
    }

    public function shippingAddressAdditionalFieldCreate(array $attributes)
    {
        return $this->shippingAddressAdditionalField->create($attributes) ;
    }

    public function shippingAddressAdditionalFieldUpdate($id, array $attributes)
    {
        $con=$this->shippingAddressAdditionalField->findOrFail($id)->update($attributes);
        $con=$this->shippingAddressAdditionalField->findOrFail($id)->fill($attributes);
        return $con;
    }

    public function shippingAddressAdditionalFieldDelete($id)
    {
        $data=$this->shippingAddressAdditionalField->findOrFail($id)->delete();
        return true ;

    }

    public function shippingAddressAdditionalFieldDeleteByShippingId($id)
    {
        $data=$this->shippingAddressAdditionalField->where("shipping_address_id",$id)->delete();
        return true ;

    }
}