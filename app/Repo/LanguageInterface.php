<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 4:57 PM
 */

namespace App\Repo;


interface LanguageInterface
{
    public function languageGetAll();

    public function languageGetAllByStatusAndLimit($limit);

    public function languageGetSpecific($id);

    public function languageGetSpecificByCode($code);

    public function languageGetSpecificByCodeAndEnabled($code);

    public function languageGetSpecificAndStatusEnabled($id);

    public function languageCreate(array $attributes);

    public function languageUpdate($id, array $attributes);

    public function languageDelete($id);
}