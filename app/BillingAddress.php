<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:19 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class BillingAddress extends Model
{
    protected $guarded=['id'];
    protected $fillable = [
        "",
    ];
    protected $table='billing_address';


}