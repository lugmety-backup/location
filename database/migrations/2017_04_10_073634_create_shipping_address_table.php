<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_address', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('address_name');
            $table->string('street');
            $table->double('longitude');
            $table->double('latitude');
            $table->unsignedInteger('district_id');
            $table->foreign('district_id')->references('id')->on('district')->onDelete('cascade');
            $table->text('extra_direction');
            $table->enum('type',['house','building']);
            $table->smallInteger('primary');
            $table->softDeletes();
            $table->timestamps();
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_address');
    }
}
