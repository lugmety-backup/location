<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 5:02 PM
 */

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\District;
use App\Repo\CityInterface;
use App\Repo\CountryInterface;
use App\Repo\ShippingAddressAdditionalFieldInterface;
use App\Repo\ShippingAddressInterface;
use App\Repo\ShippingAddressMetaInterface;
use App\Repo\DistrictInterface;
use App\ShippingAddress;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Validator;
use App\ShippingAddressMeta;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use RoleChecker;
use Illuminate\Support\Facades\Config;

class ShippingAddressController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $shipping_address;
    private $shipping_address_meta;
    private $district;
    private $country;
    protected $checker;
    protected $city;
    private $shippingAddressAdditionalField;

    public function __construct(ShippingAddressInterface $shippingAddress,
                                RoleChecker $checker,
                                CountryInterface $country,
                                CityInterface $city,
                                ShippingAddressMetaInterface $shippingAddressMeta,
                                DistrictInterface $district, ShippingAddressAdditionalFieldInterface $shippingAddressAdditionalField)
    {
        $this->checker = $checker;
        $this->shipping_address = $shippingAddress;
        $this->shipping_address_meta = $shippingAddressMeta;
        $this->district = $district;
        $this->country = $country;
        $this->city = $city;
        $this->shippingAddressAdditionalField = $shippingAddressAdditionalField;
    }

    /**
     * Display a listing of the shipping address.user with role customer cannot have access
     *
     * @return \Illuminate\Http\Response
     */
    public function adminIndex(Request $request)
    {
        try {
            try {
                $this->validate($request, [
                    "user_id" => "required|integer|min:1",
                    "country_id" => "required|integer|min:1"
                ]);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "422",
                    "message" => $ex->response->original
                ], 422);
            }
            if (!RoleChecker::hasRole('customer')) {
                $shippingAddress = $this->shipping_address->shippingAddressGetAllForDatatable($request->user_id);
                $shippingAddress = collect($shippingAddress)->where("country_id", $request->country_id);
                $countryName = $this->country->countryGetSpecific($request->country_id)->countryTranslation()->where("lang", "en")->first()->name;


                $cityList = array_unique($shippingAddress->pluck("city_id")->toArray());
                $districtList = array_unique($shippingAddress->pluck("district_id")->toArray());
                $districtData = District::find($districtList);
                $districtNameList = [];
                foreach ($districtData as $district) {
                    $districtNameList[] = ["id" => $district["id"], "name" => $district->districtTranslation()->select("name")->where("lang_code", "en")->first()->name];
//                $districtNameList[]["name"] = $district->districtTranslation()->select("name")->where("lang_code","en")->first()->name;

                }

                $cityList = City::find($cityList);
                $cityNameList = [];
                foreach ($cityList as $city) {
                    $cityNameList[] = ["id" => $city["id"], "name" => $city->cityTranslation()->select("name")->where("lang_code", "en")->first()->name];
                }
                try {
                    if (count($shippingAddress) == 0) {
                        throw new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        'status' => '404',
                        "message" => 'Empty Record'
                    ], 404);
                }
                foreach ($shippingAddress as $shippingAddres) {
                    $shippingAddres["country_name"] = $countryName;
                    foreach ($cityNameList as $cityList) {
                        if ($cityList["id"] == $shippingAddres->city_id) {
                            $shippingAddres["city_name"] = $cityList["name"];
                            break;
                        }
                    }
                    foreach ($districtNameList as $districtList) {
                        if ($districtList["id"] == $shippingAddres->district_id) {
                            $shippingAddres["district_name"] = $districtList["name"];
                            break;
                        }
                    }
                    $shippingAddres['meta'] = $shippingAddres->shippingAddressMeta()->get();
                }
                return \Datatables::of($shippingAddress)->make(true);
            } else {
                return response()->json([
                    "status" => "403",
                    "message" => "Only Admin or Super Admin has access "
                ], 403);
            }
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => '404',
                'message' => 'Requested delivery address could not be found.'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'Requested delivery address could not be found.'
            ], 404);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lang = Input::get('lang', 'en');
        try {
            try {
                $this->validate($request, [
                    "country_id" => "sometimes|integer|min:1"
                ]);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "422",
                    "message" => $ex->response->original
                ], 422);
            }

//            if (RoleChecker::hasRole("customer")) {
            $limit["limit"] = Input::get("limit", 10);
            /*
                     * validation of limit param if limit param is not null
                     * */
            if (!is_null($limit['limit'])) {
                $rules = [
                    "limit" => "sometimes|numeric|min:1",
                ];
                $validator = Validator::make($limit, $rules);
                if ($validator->fails()) {
                    $request['limit'] = 10;
                }
            }
            $userId = RoleChecker::getUser();
            $shippingAddress = $this->shipping_address->shippingAddressGetAll($userId);
//            } else {
//                return response()->json([
//                    "status" => "403",
//                    "message" => "Only customer can view his/her list of shipping address"
//                ], 403);
//            }

            //dd(count($district));


            $shippingAddress = collect($shippingAddress);
            $countryList = array_unique($shippingAddress->pluck("country_id")->toArray());
            $cityList = array_unique($shippingAddress->pluck("city_id")->toArray());
            $districtList = array_unique($shippingAddress->pluck("district_id")->toArray());
            $districtData = District::find($districtList);
            $districtNameList = [];
            foreach ($districtData as $district) {
                $districtNameList[] = ["id" => $district["id"], "name" => $district->districtTranslation()->select("name")->where("lang_code", "en")->first()->name];
//                $districtNameList[]["name"] = $district->districtTranslation()->select("name")->where("lang_code","en")->first()->name;

            }

            $cityList = City::find($cityList);
            $cityNameList = [];
            foreach ($cityList as $city) {
                $cityNameList[] = ["id" => $city["id"], "name" => $city->cityTranslation()->select("name")->where("lang_code", "en")->first()->name];
            }

            $countryLists = Country::find($countryList);
            $countryNameList = [];
            foreach ($countryLists as $country) {
                $countryNameList[] = ["id" => $country["id"], "name" => $country->countryTranslation()->select("name")->where("lang", "en")->first()->name];
            }
            foreach ($shippingAddress as $key => $shippingAddres) {
                foreach ($cityNameList as $cityList) {
                    if ($cityList["id"] == $shippingAddres->city_id) {
                        $shippingAddres["city_name"] = $cityList["name"];
                        break;
                    }
                }
                foreach ($districtNameList as $districtList) {
                    if ($districtList["id"] == $shippingAddres->district_id) {
                        $shippingAddres["district_name"] = $districtList["name"];
                        break;
                    }
                }
                foreach ($countryNameList as $countryLists) {
                    if ($countryLists["id"] == $shippingAddres->country_id) {
                        $shippingAddres["country_name"] = $countryLists["name"];
                        break;
                    }
                }
                if (isset($request["country_id"])) {
                    $countryId = $shippingAddres->district->city->country_id;
                    if ($request["country_id"] != $countryId) unset($shippingAddress[$key]);
                }
                $shipping = $this->shipping_address_meta->shippingAddressMetaGetAllByShippingAddressId($shippingAddres['id']);
                $shippingAddres['meta'] = $shipping;
            }
            try {
                if (count($shippingAddress) == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return \Settings::getErrorMessageOrDefaultMessage('location-empty-record',
                    "Empty Record", 404, $lang);
            }

            return response()->json([
                'status' => '200',
                'data' => $shippingAddress->values()
            ], 200);
        } catch (ModelNotFoundException $e) {
            return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                "Requested delivery address could not be found.", 404, $lang);
        } catch (\Exception $e) {
            return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                "Requested delivery address could not be found.", 404, $lang);
        }
    }

    public function indexPagination(Request $request)
    {
        try {
            $this->validate($request, [

                "search" => "sometimes|string",
                "country_id" => "required|integer|min:1",
                "city_id" => "integer|min:1",
                "district_id" => "integer|min:1",
                "sort_column" => "sometimes",
                "sort_by" => "sometimes",


            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            $this->validate($request, [
                "limit" => "integer|min:1",
            ]);
        } catch (\Exception $ex) {
            $request['limit'] = 10;
        }
        if (RoleChecker::hasRole("customer") || RoleChecker::hasRole("restaurant-customer")) {
            $request['user_id'] = RoleChecker::getUser();
        }
//        try {

        $filter = [
            'sort_by' => $request->get("sort_by", "desc"),
            'limit' => $request->get("limit", 10),
            'user_id' => ($request->has("user_id")) ? $request->user_id : null,
            "search" => ($request->has("search")) ? $request->search : null,
            "sort_column" => ($request->has("sort_column")) ? $request->sort_column : null,
            "city_id" => ($request->has("city_id")) ? $request->city_id : null,
            "district_id" => ($request->has("district_id")) ? $request->district_id : null,
            "lang" => ($request->has("lang")) ? $request->lang : "en",
            "country_id" => $request['country_id']
        ];
        $shippingAddress = $this->shipping_address->shippingAddressPagination($filter);
        return $shippingAddress->withPath("/pagination/shipping/address")->appends($filter);
        try{} catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => "error getting data"
            ], 500);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lang = Input::get('lang', 'en');
        $isRestaurantCustomer = false;
        DB::beginTransaction();
        /**
         * All Validation of given request
         *
         */
        try {
            $messages = [
                'house_number.required' => 'Villa number is required.',
            ];
            if ($request->has('type')) {
                if ($request['type'] == "building") {
                    $messages = [
                        'house_number.required' => 'Apartment number is required.'
                    ];
                }
            }

            if (RoleChecker::hasRole("customer")) {
                $this->validate($request, [
                    'address_name' => 'required',
                    'street' => 'required',
                    'district_id' => 'required|integer',
                    'country_id' => 'required|integer',
                    'city_id' => 'required|integer',
//                    'extra_direction' => 'required',
                    'type' => 'required|in:house,building',
                    'house_number' => "required",
                    "floor_number" => "required_if:type,building",
//                    "zip_code" => 'sometimes',
                    'longitude' => 'required|numeric',
                    'latitude' => 'required|numeric',
                ], $messages);
                $userId = Rolechecker::getUser();
            } elseif (RoleChecker::hasRole("restaurant-customer")) {
                $isRestaurantCustomer = true;
                $this->validate($request, [
                    'address_name' => 'required',
                    'street' => 'required',
                    'district_id' => 'required|integer',
                    'country_id' => 'required|integer',
                    'city_id' => 'required|integer',
//                    'extra_direction' => 'required',
                    'type' => 'required|in:house,building',
                    'house_number' => "required",
                    "floor_number" => "required_if:type,building",
//                    "zip_code" => 'sometimes',
                    'longitude' => 'required|numeric',
                    'latitude' => 'required|numeric',
                    'customer_name' => 'required|string',
                    'customer_email' => 'email',
                    'customer_phone' => 'digits_between:7,20'
                ], $messages);
                $userId = Rolechecker::getUser();
            } else {
                $this->validate($request, [
                    "user_id" => 'required|integer',
                    'address_name' => 'required',
                    'street' => 'required',
                    'district_id' => 'required|integer',
                    'country_id' => 'required|integer',
                    'city_id' => 'required|integer',
//                    'extra_direction' => 'required',
                    'type' => 'required|in:house,building',
                    'house_number' => "required",
                    "floor_number" => "required_if:type,building",
//                    "zip_code" => 'required|digits:5|integer',
                    'longitude' => 'required|numeric',
                    'latitude' => 'required|numeric',
                ], $messages);
                $userId = $request["user_id"];
            }
            try {
                $district = $this->district->districtGetSpecific($request["district_id"]);
                if ($district["status"] == 0) {
                    throw new ModelNotFoundException();
                }
                $citydata = $district->city;
                $cityId = $citydata->id;
                $countryData = $citydata->country;
                $countryId = $countryData->id;
                if (($request["city_id"] != $cityId) || ($request["country_id"] != $countryId)) {
                    return \Settings::getErrorMessageOrDefaultMessage('location-given-district-doesnt-belong-to-city-country',
                        "The given district id does not belongs to either city or country.", 404, $lang);
                }
            } catch (ModelNotFoundException  $ex) {
                DB::rollback();
                return \Settings::getErrorMessageOrDefaultMessage('location-district-could-not-be-found',
                    "District could not be found.", 404, $lang);
            }
            try {
                $shipping = $this->shipping_address->shippingAddressGetAllByCountryId($userId, $request->country_id);

                if (count($shipping) == 0) {
                    $request['primary'] = 1;
                } else {
                    $request['primary'] = 0;
                }
            } catch (ModelNotFoundException  $ex) {
                DB::rollback();
                return response()->json([
                    'status' => '404',
                    'message' => "Shipping address of users query failed"
                ], 404);
            }


            if (!RoleChecker::hasRole("restaurant-customer")) {
                if (RoleChecker::hasRole("admin") || RoleChecker::hasRole("super-admin")) {
                    $fullUrl = $oauthUrl = Config::get('config.oauth_base_url') . "/user/" . $request['user_id'];
                    $getUserData = \RemoteCall::getSpecific($oauthUrl);
                    if ($getUserData["status"] != "200") {
                        if ($getUserData['status'] == 503) {
                            return response()->json(
                                $getUserData, $getUserData['status']);
                        }
                        return response()->json(
                            $getUserData["message"], $getUserData['status']);
                    }
                    if ($getUserData["message"]["data"]["role_name"] == "restaurant-customer") {
                        $isRestaurantCustomer = true;
                        $this->validate($request, [
                            'customer_name' => 'required|string',
                            'customer_email' => 'email',
                            'customer_phone' => 'digits_between:7,20'
                        ], $messages);
                        goto skipCustomerValidation;
                    }
                }
                /**
                 * Getting Shipping Address Limit from settings in general service
                 */
                $data = \Settings::getSettings('shipping-address-limit');
                if ($data["status"] != 200) {
                    return response()->json(
                        $data["message"]
                        , $data["status"]);
                }
                $shippingLimit = \Redis::get('shipping-address-limit');

//            $shippingLimit = Config::get('config.billing_address_limit');

                /**
                 * Compares if the shipping address limit is less or equals to the records obtained for the passed user
                 * */
                if ($shippingLimit <= count($shipping)) {
                    return response()->json([
                        "status" => "403",
                        "message" => "More than $shippingLimit addresses per user is not allowed"
                    ], 403);
                }
            }
            skipCustomerValidation:

        } catch (\Exception $e) {
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }
        $requestHasMeta = 0;
        if ($request->has('meta')) {
            $requestHasMeta = 1;

            if (!is_array($request['meta'])) {
                DB::rollBack();

                return response()->json([
                    'status' => '422',
                    "message" => ["meta" => "Meta key is not an  array"]
                ], 422);
            }

            $rules = [
                'meta_key' => 'required',
                'meta_value' => 'required',
            ];//validation of translation field
            foreach ($request['meta'] as $key => $req) {

                $validator = Validator::make($req, $rules);

                if ($validator->fails()) {
                    $error = $validator->errors();
                    DB::rollBack();
//                    $this->logStoreHelper->storeLogError(array('Shipping Meta', [
//                        'status' => '422',
//                        "message" => [$key => $error]
//                    ]));
                    return response()->json([
                        'status' => '422',
                        "message" => [$key => $error]
                    ]);
                }
            }
        }

        $locationDetail = \GeoLocationChecker::fetch($request['longitude'], $request['latitude'], $lang);
        Log::info('goelocation-check', [$locationDetail]);
        if (!isset($locationDetail['status'])) {
            return response()->json([
                'status' => '422',
                "message" => ["coordinates" => ["Error! Could not fetch coordinates."]]
            ], 422);
        }
        if ($locationDetail['status'] != "200") {
            return response()->json([
                'status' => '422',
                "message" => ["coordinates" => ["Error! Could not verify coordinates with provided address."]]
            ], 422);
        } else {
            if ($request['country_id'] != $locationDetail['payload']['country']['country_id']
                || $request['city_id'] != $locationDetail['payload']['city']['city_id']
                || $request['district_id'] != $locationDetail['payload']['district']['district_id']
            ) {
                return response()->json([
                    'status' => '422',
                    "message" => ["coordinates" => ["Error! Could not verify coordinates with provided address."]]
                ], 422);
            }
        }

        //$request=$request->all();


        /**
         * All Validation of given request ends
         *
         */

        try {
            $request["user_id"] = $userId;
            if ($request->type == "house") {
                $shipping_address = $this->shipping_address->shippingAddressCreate($request->except(["floor_number", "building_name", "office_name"]));
            } else {
                $shipping_address = $this->shipping_address->shippingAddressCreate($request->all());
            }

            if ($isRestaurantCustomer) {

                try {

                    $customerReq = $request->all();
                    $customerReq['user_id'] = $userId;
                    $customerReq['shipping_address_id'] = $shipping_address->id;
                    $this->shippingAddressAdditionalField->shippingAddressAdditionalFieldDeleteByShippingId($customerReq['shipping_address_id']);
                    $shippingAddressMeta = $this->shippingAddressAdditionalField->shippingAddressAdditionalFieldCreate($customerReq);
                } catch (\Exception $e) {
                    DB::rollBack();
                    Log::error('Country Meta', [
                        'status' => '422',
                        'message' => $e->getMessage()
                    ]);
                    return response()->json([
                        'status' => '422',
                        'message' => $e->getMessage()
                    ], 422);
                }

            }

            /************** shipping meta post starts ********************/
            if ($requestHasMeta == 1) {
                foreach ($request['meta'] as $req) {
                    try {

                        $req['shipping_address_id'] = $shipping_address->id;
                        $shippingAddressMeta = $this->shipping_address_meta->shippingAddressMetaGetSpecificByKey($req['meta_key'], $req['shipping_address_id']);
                        try {
                            if ($shippingAddressMeta->count() != 0) {

                                throw new \Exception();
                            }
                        } catch (\Exception $ex) {
                            DB::rollBack();
                            Log::error('Shipping Address Meta', [
                                'status' => '409',
                                'message' => "Delivery address meta could not be created due to duplicate entry."
                            ]);
                            return response()->json([
                                'status' => '409',
                                'message' => "Delivery address meta could not be created due to duplicate entry."
                            ], 409);
                        }
                        $shippingAddressMeta = $this->shipping_address_meta->shippingAddressMetaCreate($req);
                        Log::info('Country Meta ', [
                            "status" => "200",
                            "data" => $shippingAddressMeta]);

                    } catch (\Exception $e) {
                        DB::rollBack();
                        Log::error('Country Meta', [
                            'status' => '422',
                            'message' => $e->getMessage()
                        ]);
                        return response()->json([
                            'status' => '422',
                            'message' => $e->getMessage()
                        ], 422);
                    }

                }
            }
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'New delivery address successfully added.',
                "id" => $shipping_address->id
            ], 200);

            /******************** shippping meta post stops ************************/

        } catch (QueryException $exception) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => 'Delivery address not found.'
            ], 404);
        } catch (ModelNotFoundException  $ex) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => "Delivery address not found."
            ], 404);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }

    }


    /**
     * Display the specified shipping address.user with role customer cannot have access
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function adminShow($id)
    {
        try {
            if (!RoleChecker::hasRole('customer')) {
                $shippingAddress = $this->shipping_address->shippingAddressGetSpecific($id);
                try {
                    if (count($shippingAddress) == 0) {
                        throw new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        'status' => '404',
                        "message" => 'Empty Record'
                    ], 404);
                }
                //return response()->json($shippingAddress);
                $shippingAddress['meta'] = $shippingAddress->shippingAddressMeta()->get();

                return response()->json([
                    'status' => '200',
                    'data' => $shippingAddress
                ], 200);
            } else {
                return response()->json([
                    "status" => "403",
                    "message" => "You have no access."
                ], 403);
            }
        } catch (ModelNotFoundException $e) {

            return response()->json([
                'status' => '404',
                'message' => 'Requested delivery address could not be found.'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'Requested delivery address could not be found.'
            ], 404);
        }
    }

    /**
     * gets shipping details for machine integraartion purpose
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function showForInternalCall($id, Request $request)
    {

        $lang = $request['lang'] = Input::get('lang', 'en');
        try {
            $shippingAddress = $this->shipping_address->shippingAddressGetSpecificIncludingDeleted($id);
            try {
                if (count($shippingAddress) == 0) {
                    throw new \Exception();
                }
                $districtData = $shippingAddress->district;
                $district_name = $districtData->districtTranslation()->select("name")->where("lang_code", $request['lang'])->first();
                if ($district_name == null) {
                    $district_name = $districtData->districtTranslation()->select("name")->where("lang_code", "en")->first();
                }
                $shippingAddress['district_name'] = $district_name->name;
                $cityData = $districtData->city;
                $countryData = $cityData->country;
                $city_name = $cityData->cityTranslation()->select("name")->where("lang_code", $request['lang'])->first();
                if ($city_name == null) {
                    $city_name = $cityData->cityTranslation()->select("name")->where("lang_code", "en")->first();
                }
                $shippingAddress['city_name'] = $city_name->name;
                $country_name = $countryData->countryTranslation()->select("name")->where("lang", $request['lang'])->first();
                if ($country_name == null) {
                    $country_name = $countryData->countryTranslation()->select("name")->where("lang", "en")->first();
                }
                $shippingAddress['country_name'] = $country_name->name;

            } catch (\Exception $ex) {
                return \Settings::getErrorMessageOrDefaultMessage('location-empty-record',
                    "Empty Record.", 404, $lang);
            }
            return response()->json([
                'status' => '200',
                'data' => $shippingAddress
            ], 200);
        } catch (QueryException $e) {
            return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                "Requested delivery address could not be found.", 404, $lang);
        } catch (\Exception $e) {
            return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                "Requested delivery address could not be found.", 404, $lang);
        }
    }

    /**
     * gets shipping details for machine integraartion purpose
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function showShippingForMachineIntegration($id)
    {

        try {
            $shippingAddress = $this->shipping_address->shippingAddressGetSpecificIncludingDeleted($id);
            try {
                if (count($shippingAddress) == 0) {
                    throw new \Exception();
                }
                $districtData = $shippingAddress->district;
                $shippingAddress["district_name"] = $districtData->districtTranslation()->select("name")->where("lang_code", "en")->first()->name;
                $cityData = $districtData->city;
                $countryData = $cityData->country;
                $shippingAddress["city_name"] = $cityData->cityTranslation()->select("name")->where("lang_code", "en")->first()->name;
                $shippingAddress["country_name"] = $countryData->countryTranslation()->select("name")->where("lang", "en")->first()->name;
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }
            return response()->json([
                'status' => '200',
                'data' => $shippingAddress
            ], 200);
        } catch (QueryException $e) {

            return response()->json([
                'status' => '404',
                'message' => 'Requested delivery address could not be found.'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'Requested delivery address could not be found.'
            ], 404);
        }
    }

    /**
     * Display the specified shipping address.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        try {
            $lang = $request->get("lang", "en");
            $shippingAddress = $this->shipping_address->shippingAddressGetSpecific($id);
            $userId = Rolechecker::getUser();
//            if(RoleChecker::hasRole('customer') ){
            if ($userId !== $shippingAddress["user_id"]) {
                return \Settings::getErrorMessageOrDefaultMessage('location-you-are-only-allowed-to-view-your-information',
                    "You are only allowed to view your information.", 403, $lang);
            }

            //get country info
            try {
                $countryInfo = $this->country->countryGetSpecific($shippingAddress->country_id);
                $shippingAddress["country_name"] = $countryInfo->countryTranslation()->select("name")->where("lang", $lang)->first()->name;
                if (count($shippingAddress["country_name"]) == 0) {
                    $shippingAddress["country_name"] = $countryInfo->countryTranslation()->where("lang", "en")->select("name")->first()->name;
                }
            } catch (\Exception $ex) {
                return \Settings::getErrorMessageOrDefaultMessage('location-country-could-not-be-found',
                    "Country could not be found.", 404, $lang);
//                return response()->json([
//                    "status" => "404",
//                    "message" => "Country could not be found."
//                ], 404);
            }

//            }
            //dd(count($district));
            try {
                if (count($shippingAddress) == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return \Settings::getErrorMessageOrDefaultMessage('location-empty-record',
                    "Empty Record.", 404, $lang);
            }
            //return response()->json($shippingAddress);

            $shipping = $this->shipping_address_meta->shippingAddressMetaGetAllByShippingAddressId($shippingAddress['id']);
            $shippingAddress['meta'] = $shipping;

            return response()->json([
                'status' => '200',
                'data' => $shippingAddress
            ], 200);
        } catch (QueryException $e) {
            return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                "Requested delivery address could not be found.", 404, $lang);
        } catch (\Exception $e) {
            return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                "Requested delivery address could not be found.", 404, $lang);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lang = Input::get('lang', 'en');

        try {
            $shippingAddress = $this->shipping_address->shippingAddressGetSpecific($id);
        } catch (\Exception $e) {
            return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                "Requested delivery address could not be found.", 404, $lang);
        }

        DB::beginTransaction();
        /**
         * All Validation of given request
         *
         */

        try {
            $messages = [
                'house_number.required' => 'Villa number is required.',
            ];
            if ($request->has('type')) {
                if ($request['type'] == "building") {
                    $messages = [
                        'house_number.required' => 'Apartment number is required.'
                    ];
                }
            }
            if (RoleChecker::hasRole("customer")) {
                $this->validate($request, [
                    'address_name' => 'required',
                    'street' => 'required',
                    'district_id' => 'required|integer',
                    'country_id' => 'required|integer',
                    'city_id' => 'required|integer',
//                    'extra_direction' => 'required',
                    'type' => 'required|in:house,building',
                    'house_number' => "required",
                    "floor_number" => "required_if:type,building",
//                    "zip_code" => 'sometimes|digits:5|integer',
                    'longitude' => 'required|numeric',
                    'latitude' => 'required|numeric',
                ], $messages);
                $userId = Rolechecker::getUser();
                if ($shippingAddress->user_id !== $userId) {
                    return response()->json(
                        [
                            "status" => "403",
                            "message" => "You are only allowed to update your delivery address."
                        ]
                        , 403);
                }
            }elseif (RoleChecker::hasRole("restaurant-customer")) {
                $isRestaurantCustomer = true;
                $this->validate($request, [
                    'address_name' => 'required',
                    'street' => 'required',
                    'district_id' => 'required|integer',
                    'country_id' => 'required|integer',
                    'city_id' => 'required|integer',
//                    'extra_direction' => 'required',
                    'type' => 'required|in:house,building',
                    'house_number' => "required",
                    "floor_number" => "required_if:type,building",
//                    "zip_code" => 'sometimes',
                    'longitude' => 'required|numeric',
                    'latitude' => 'required|numeric',
                    'customer_name' => 'required|string',
                    'customer_email' => 'email',
                    'customer_phone' => 'digits_between:7,20'
                ], $messages);
                $userId = Rolechecker::getUser();
            } else {
                $this->validate($request, [
                    "user_id" => 'required|integer',
                    'address_name' => 'required',
                    'street' => 'required',
                    'district_id' => 'required|integer',
                    'country_id' => 'required|integer',
                    'city_id' => 'required|integer',
//                    'extra_direction' => 'required',
                    'type' => 'required|in:house,building',
                    'house_number' => "required",
                    "floor_number" => "required_if:type,building",
//                    "zip_code" => 'sometimes|digits:5|integer',
                    'longitude' => 'required|numeric',
                    'latitude' => 'required|numeric',
                ], $messages);
                $userId = $request["user_id"];
            }

        } catch (\Exception $e) {
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }
        $request['zip_code'] = Input::get("zip_code", 0);
        $request['primary'] = $shippingAddress->primary;

        if (!RoleChecker::hasRole("restaurant-customer")) {
            if (RoleChecker::hasRole("admin") || RoleChecker::hasRole("super-admin")) {
                $fullUrl = $oauthUrl = Config::get('config.oauth_base_url') . "/user/" . $request['user_id'];
                $getUserData = \RemoteCall::getSpecific($oauthUrl);
                if ($getUserData["status"] != "200") {
                    if ($getUserData['status'] == 503) {
                        return response()->json(
                            $getUserData, $getUserData['status']);
                    }
                    return response()->json(
                        $getUserData["message"], $getUserData['status']);
                }
                if ($getUserData["message"]["data"]["role_name"] == "restaurant-customer") {
                    $isRestaurantCustomer = true;
                    $this->validate($request, [
                        'customer_name' => 'required|string',
                        'customer_email' => 'email',
                        'customer_phone' => 'digits_between:7,20'
                    ], $messages);
                    goto skipCustomerValidation;
                }
            }
            /**
             * Getting Shipping Address Limit from settings in general service
             */
            $data = \Settings::getSettings('shipping-address-limit');
            if ($data["status"] != 200) {
                return response()->json(
                    $data["message"]
                    , $data["status"]);
            }
            $shippingLimit = \Redis::get('shipping-address-limit');
            //$shippingLimit = Config::get('config.billing_address_limit');
            /**
             * Compares if the shipping address limit is less or equals to the records obtained for the passed user except for passed shipping address $id
             * */
            if ($shippingLimit <= count($this->shipping_address->shippingAddressGetAllShippingAddressExceptPassedId($id, $userId, $request->country_id))) {
                return response()->json([
                    "status" => "403",
                    "message" => "More than three $shippingLimit addresses per user is not allowed"
                ], 403);
            }
        }
        skipCustomerValidation:



        $requestHasMeta = 0;
        if ($request->has('meta')) {
            $requestHasMeta = 1;
            $rules = [
                'meta_key' => 'required',
                'meta_value' => 'required',
            ];//validation of translation field
            foreach ($request['meta'] as $key => $req) {

                $validator = Validator::make($req, $rules);

                if ($validator->fails()) {
                    $error = $validator->errors();
                    DB::rollBack();
//                    $this->logStoreHelper->storeLogError(array('Shipping Meta', [
//                        'status' => '422',
//                        "message" => [$key => $error]
//                    ]));
                    return response()->json([
                        'status' => '422',
                        "message" => [$key => $error]
                    ]);
                }
            }

        }
        try {
            $district = $this->district->districtGetSpecific($request["district_id"]);
            if ($district["status"] == 0) {
                throw new ModelNotFoundException();
            }
            $citydata = $district->city;
            $cityId = $citydata->id;
            $countryData = $citydata->country;
            $countryId = $countryData->id;
            if (($request["city_id"] != $cityId) and ($request["country_id"] != $countryId)) {
                return \Settings::getErrorMessageOrDefaultMessage('location-given-district-doesnt-belong-to-city-country',
                    "The given district id does not belongs to either city or country.", 403, $lang);
            }
        } catch (ModelNotFoundException  $ex) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => "District not found with given district_id"
            ], 404);
        }

        $locationDetail = \GeoLocationChecker::fetch($request['longitude'], $request['latitude'], $lang);
        if (!isset($locationDetail['status'])) {
            return response()->json([
                'status' => '422',
                "message" => ["coordinates" => ["Error! Could not fetch coordinates."]]
            ], 404);
        }
        if ($locationDetail['status'] != "200") {
            return response()->json([
                'status' => '422',
                "message" => ["coordinates" => ["Error! Could not verify coordinates with provided address."]]
            ], 422);
        } else {
            if ($request['country_id'] != $locationDetail['payload']['country']['country_id']
                || $request['city_id'] != $locationDetail['payload']['city']['city_id']
                || $request['district_id'] != $locationDetail['payload']['district']['district_id']
            ) {
                return response()->json([
                    'status' => '422',
                    "message" => ["coordinates" => ["Error! Could not verify coordinates with provided address."]]
                ], 422);
            }
        }

        /**
         * All Validation of given request ends
         *
         */
        try {

            $request["user_id"] = $userId;
            if ($request->type == "house") {
                $shipping_address = $this->shipping_address->shippingAddressUpdate($id, $request->except(["floor_number", "building_name", "office_name"]));
            } else {
                $shipping_address = $this->shipping_address->shippingAddressUpdate($id, $request->all());
            }

            /************** shipping meta post starts ********************/
            if ($requestHasMeta == 1) {
                $rules = [
                    'meta_key' => 'required',
                    'meta_value' => 'required',
                ];
                foreach ($request['meta'] as $key => $req) {
                    try {
                        $req['shipping_address_id'] = $shipping_address->id;
                        $validator = Validator::make($req, $rules);

                        if ($validator->fails()) {
                            $error = $validator->errors();
                            DB::rollBack();
                            $this->logStoreHelper->storeLogError(array('Country Meta', [
                                'status' => '422',
                                "message" => [$key => $error]
                            ]));
                            return response()->json([
                                'status' => '422',
                                "message" => [$key => $error]
                            ]);
                        }
                        $shippingAddressMeta = $this->shipping_address_meta->shippingAddressMetaUpdateOrCreate($req);
                        Log::info('Country Meta ', [
                            "status" => "200",
                            "data" => $shippingAddressMeta]);

                    } catch (\Exception $e) {
                        DB::rollBack();
                        Log::error('Country Meta', [
                            'status' => '422',
                            'message' => $e->getMessage()
                        ]);
                        return response()->json([
                            'status' => '422',
                            'message' => $e->getMessage()
                        ], 422);
                    }

                }
            }
            DB::commit();
            return \Settings::getErrorMessageOrDefaultMessage('location-delivery-address-updated-successfully',
                "Delivery address updated successfully.", 200, $lang);
//            return response()->json([
//                'status' => '200',
//                'message' => 'Delivery address updated successfully.'
//            ], 200);

            /******************** shippping meta post stops ************************/

        } catch (QueryException $exception) {
            DB::rollback();
            return \Settings::getErrorMessageOrDefaultMessage('location-delivery-address-not-found',
                "Delivery address not found.", 404, $lang);
//            return response()->json([
//                'status' => '404',
//                'message' => 'Delivery address not found.'
//            ], 404);
        } catch (ModelNotFoundException  $ex) {
            DB::rollback();
            return \Settings::getErrorMessageOrDefaultMessage('location-delivery-address-not-found',
                "Delivery address not found.", 404, $lang);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $lang = Input::get('lang', 'en');
        try {
            $shippingAddress = $this->shipping_address->shippingAddressGetSpecific($id);
            if (RoleChecker::hasRole("customer")) {
                $userId = Rolechecker::getUser();
                if ($shippingAddress->user_id !== $userId) {
                    return \Settings::getErrorMessageOrDefaultMessage('location-you-are-only-allowed-to-delete-your-delivery-address',
                        "You are only allowed to delete your delivery address.", 403, $lang);
                }
            }


        } catch (\Exception $ex) {
            return \Settings::getErrorMessageOrDefaultMessage('location-delivery-address-not-found',
                "Delivery address not found.", 404, $lang);
//            return response()->json([
//                'status' => '404',
//                'message' => "Delivery address could not be found."
//            ], 404);
        }
        DB::beginTransaction();

        try {
            $shipping = $this->shipping_address->shippingAddressGetAllSortedByDate($shippingAddress->user_id);
//            if (count($shipping) == 1) {
//                return response()->json([
//                    'status' => '403',
//                    'message' => "Shipping Address could not be deleted! Only one Shipping Address is available"
//                ], 403);
//            } else
            if (count($shipping) > 1) {
                if ($shippingAddress->primary == 1) {

                    if ($shipping[0]['id'] == $id) {
                        $this->shipping_address->setShippingAddressPrimary($shipping[1]['id'], 1);
                    } else {
                        $this->shipping_address->setShippingAddressPrimary($shipping[0]['id'], 1);
                    }
                }
            }
        } catch (ModelNotFoundException  $ex) {
            DB::rollback();
            return \Settings::getErrorMessageOrDefaultMessage('location-delivery-address-of-users-query-failed',
                "Delivery address of users query failed.", 404, $lang);
        }

        try {
//            $shippingAddressMetas = $this->shipping_address_meta->shippingAddressMetaGetAllByShippingAddressId($id);
//
//            foreach ($shippingAddressMetas as $shippingAddressMeta) {
//                //return response()->json($shippingAddressMeta);
//                $shippingAddressMeta = $this->shipping_address_meta->shippingAddressMetaDelete($shippingAddressMeta['id']);
//            }
            $shippingAddress = $this->shipping_address->shippingAddressDelete($id);
            DB::commit();
            return \Settings::getErrorMessageOrDefaultMessage('location-delivery-address-deleted-successfully',
                "Delivery address deleted successfully.", 200, $lang);
//            return response()->json([
//                'status' => '200',
//                'message' => "Delivery address deleted successfully."
//            ]);
        } catch (QueryException $ex) {
            DB::rollback();
            return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                "Requested delivery address could not be found.", 404, $lang);
//            return response()->json([
//                'status' => '404',
//                'message' => "Requested delivery address could not be found."
//            ], 404);
        } catch (ModelNotFoundException  $ex) {
            DB::rollback();
            return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                "Requested delivery address could not be found.", 404, $lang);
        } catch (\Exception $ex) {
            DB::rollback();
            return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                "Requested delivery address could not be found.", 404, $lang);
        }
    }

    /*
   * get list of users based on passed array of user id
   */
    public function getListOfShippingAddressBased(Request $request)
    {
        try {
            $lang = Input::get("lang", "en");
            $shippingAddresss = $this->shipping_address->getListOfShippingDetail($request->address);

            try {
                if (count($shippingAddresss) == 0) {
                    throw new \Exception();
                }
                foreach ($shippingAddresss as $shippingAddress) {
                    $districtData = $shippingAddress->district;
                    $districtName = $districtData->districtTranslation()->select("name")->where("lang_code", $lang)->first();
                    if (!$districtName) {
                        $districtName = $districtData->districtTranslation()->select("name")->where("lang_code", "en")->first();
                    }
                    $districtName = $districtName->name;
                    $cityData = $districtData->city;
                    $countryData = $cityData->country;
                    $cityName = $cityData->cityTranslation()->select("name")->where("lang_code", $lang)->first();
                    if (!$cityName) {
                        $cityName = $cityData->cityTranslation()->select("name")->where("lang_code", "en")->first();
                    }
                    $cityName = $cityName->name;
                    $countryName = $countryData->countryTranslation()->select("name")->where("lang", $lang)->first();
                    if (!$countryName) {
                        $countryName = $countryData->countryTranslation()->select("name")->where("lang", "en")->first();
                    }
                    $countryName = $countryName->name;
                    $shippingAddress["address"] = $shippingAddress["street"]
                        . ', ' . $districtName
                        . ', ' . $cityName . ', ' . $countryName;
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }
            return response()->json([
                'status' => '200',
                'data' => $shippingAddresss
            ], 200);
        } catch (QueryException $e) {
            return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                "Requested delivery address could not be found.", 404, $lang);
//            return response()->json([
//                'status' => '404',
//                'message' => 'Requested delivery address could not be found.'
//            ], 404);
        } catch (\Exception $e) {
            return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                "Requested delivery address could not be found.", 404, $lang);
        }

    }

    public function makePrimary($id, Request $request)
    {
        $lang = Input::get('lang', 'en');
        DB::beginTransaction();
        /**
         * All Validation of given request
         *
         */
        try {
            $shippingAddress = $this->shipping_address->shippingAddressGetSpecific($id);
        } catch (\Exception $ex) {
            DB::rollBack();
            return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                "Requested delivery address could not be found.", 404, $lang);
//            return response()->json([
//                'status' => '404',
//                'message' => "Requested delivery address could not be found."
//            ], 404);
        }
        $userId = Rolechecker::getUser();
        if ($shippingAddress->user_id !== $userId) {
            DB::rollBack();
            return \Settings::getErrorMessageOrDefaultMessage('location-you-are-only-allowed-to-change-your-delivery-address',
                "You are only allowed to change your delivery address.", 403, $lang);
        }
        if ($shippingAddress->primary == 1) {
            DB::rollBack();
            return \Settings::getErrorMessageOrDefaultMessage('location-this-address-is-already-set-to-primary',
                "This address is already set to primary.", 404, $lang);
        }
        try {
            $primaryAddresses = $this->shipping_address->getShippingAddressWithPrimary($shippingAddress->user_id);
        } catch (\Exception $ex) {
            DB::rollBack();
            return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                "Requested delivery address could not be found.", 404, $lang);
        }
        foreach ($primaryAddresses as $primaryAddress) {
            try {
                $this->shipping_address->setShippingAddressPrimary($primaryAddress->id, 0);
            } catch (\Exception $ex) {
                DB::rollBack();
                return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                    "Requested delivery address could not be found.", 404, $lang);
            }
        }
        try {
            $this->shipping_address->setShippingAddressPrimary($id, 1);
        } catch (\Exception $ex) {
            DB::rollBack();
            return \Settings::getErrorMessageOrDefaultMessage('location-requested-delivery-address-could-not-be-found',
                "Requested delivery address could not be found.", 404, $lang);
        }
        DB::commit();
        return \Settings::getErrorMessageOrDefaultMessage('location-delivery-address-has-been-successfully-set-as-primary',
            "Delivery address has been successfully set as primary.", 200, $lang);
//        return response()->json([
//            'status' => '200',
//            'message' => "Delivery address has been successfully set as primary."
//        ], 200);


    }


}