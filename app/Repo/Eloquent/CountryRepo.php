<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:02 AM
 */

namespace App\Repo\Eloquent;


use App\Country;
use App\Language;
use App\Repo\CountryInterface;

class CountryRepo implements CountryInterface
{
    private $country;

    /**
     * CountryRepo constructor.
     * @param $country
     */
    public function __construct(Country $country)
    {
        $this->country = $country;
    }

    public function countryGetAllForDatatables(){
        return $this->country->all();
    }

    public function countryGetAllForDatatablesOnlyDeleted(){
        return $this->country->onlyTrashed()->get();
    }

    public function getCountryList(array $countryList){
        return $this->country->select("id","currency_symbol")->find($countryList);
    }

    public function countryGetAll($status,$limit)//get all the country information from database
    {
        // TODO: Implement countryGetAll() method.
        if(is_null($status)){
            $countries= $this->country->paginate($limit);
            $countries->appends(['limit' => $limit])->withPath('/country');
        }else{
            $countries= $this->country->where('status',$status)->paginate($limit);
            $countries->appends(['limit' => $limit])->withPath('/country');

        }
        return $countries;
    }

    public function countryGetSpecificByCode($code){
            return $this->country->where('code',$code)->get();
    }

    public function countryGetSpecificByCodeByStatusOne($code){
        return $this->country->where([['code',$code],['status',1]])->get();
    }


    public function countryGetSpecific($id)
    {
        return $this->country->findOrFail($id);
    }

    public function getCountrySpecificByStatus($id){
        return $this->country->where([
            ["status",1],
            ["id",$id]
        ])->firstOrFail();
    }

    public function countryCreate(array $attributes)
    {

        return $this->country->create($attributes);
    }

    public function countryUpdate($id, array $attributes)
    {
        $con=$this->country->findOrFail($id)->update($attributes);
        $con=$this->country->findOrFail($id)->fill($attributes);
        return $con;
    }

    public function countryDelete($id)
    {
        $data=$this->country->findOrFail($id)->delete();
        return true ;

    }

    public function countryGetSpecificSoftOrHardDeleted($id){
        return $this->country->withTrashed()->findOrFail($id);
    }

}