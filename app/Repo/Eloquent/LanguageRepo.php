<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 4:58 PM
 */

namespace App\Repo\Eloquent;


use App\Language;
use App\Repo\LanguageInterface;


class LanguageRepo implements LanguageInterface
{
    protected $language;

    /**
     * LanguageRepo constructor.
     * @param $language
     */
    public function __construct(Language $language)
    {
        $this->language = $language;
    }

    public function languageGetAll()//get all the language information from database
    {
        // TODO: Implement languageGetAll() method.

        return $this->language->all();
    }

    public function languageGetAllByStatusAndLimit($limit){
        return $this->language->where(
            "status",1)->paginate($limit);
    }


    public function languageGetSpecificByCode($code){
        return $this->language->where('code',$code)->get();
    }

    public function languageGetSpecificByCodeAndEnabled($code){
        return $this->language->where([['code',$code],["status",1]])->get();
    }

    public function languageGetSpecific($id)
    {
        return $this->language->findOrFail($id);
    }

    public function languageGetSpecificAndStatusEnabled($id){
        return $this->language->where('status',1)->findOrFail($id);
    }

    public function languageCreate(array $attributes)
    {
        return $this->language->create($attributes);
    }

    public function languageUpdate($id, array $attributes)
    {
        $con=$this->language->findOrFail($id)->update($attributes);
        $con=$this->language->findOrFail($id)->fill($attributes);
        return $con;
    }

    public function languageDelete($id)
    {
        return $this->language->findOrFail($id);

    }

}