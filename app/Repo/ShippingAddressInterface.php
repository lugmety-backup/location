<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface ShippingAddressInterface
{
    public function  shippingAddressGetAllForDatatable($user_id);

    public function shippingAddressGetAll($user_id);

    public function shippingAddressPagination($filter);

    public function shippingAddressGetAllSortedByDate($user_id);

    public function checkDistrictExist($districtId);

    public function shippingAddressGetSpecific($id);

    public function shippingAddressGetAllByCountryId($user_id,$countryId);

    public function getShippingAddressFromUserIdAndId($id,$userId);

    public function shippingAddressGetAllExceptCustomer($limit);

    public function shippingAddressGetSpecificIncludingDeleted($id);

    public function getListOfShippingDetail(array $list);

    public function shippingAddressGetAllShippingAddressExceptPassedId($id,$userId,$countryId);

    public function shippingAddressCreate(array $attributes);

    public function shippingAddressUpdate($id, array $attributes);

    public function shippingAddressDelete($id);

    public function setShippingAddressPrimary($id, $value);

    public function getShippingAddressWithPrimary($userId);
}