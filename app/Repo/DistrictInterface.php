<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface DistrictInterface
{
    public function districtGetAll($status);

    public function districtGetAllPagination($status,$city_id,$limit);

    public function districtGetSpecific($id);

    public function districtGetAllByCityId($city_id,$status);

    public function districtGetAllForDataTable($city_id);

    public function districtGetAllForDatatablesOnlyDeleted($city_id);

    public function districtGetSpecificByStatus($id,$status);

    public function districtCreate(array $attributes);

    public function districtUpdate($id, array $attributes);

    public function districtDelete($id);

    public function getDistrictFromCityId($status,$city_id,$limit);

    public function districtGetSpecificSoftOrHardDeleted($id);

    public function districtIdToNames($districts);
}