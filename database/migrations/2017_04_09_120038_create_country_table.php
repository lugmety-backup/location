<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code','15');
            $table->string('currency_code','20');
            $table->string('currency_symbol','15');
            $table->string('calling_code','20');
            $table->string('timezone',20);
            $table->float('tax_percentage',5,3);
            $table->float('min_cost');//added for cost calculation
            $table->float('per_cost');//added for cost caluclation
            $table->float("min_distance");
            $table->smallInteger('status');
            $table->softDeletes();
            $table->string('week_day_start_at','50');
            $table->timestamps();
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country');
    }
}
