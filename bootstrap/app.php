<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades();

$app->withEloquent();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);
$app->configure('config');
/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

// $app->middleware([
//    App\Http\Middleware\ExampleMiddleware::class
// ]);

$app->routeMiddleware([
    //'auth' => App\Http\Middleware\Authenticate::class,
    'checkTokenPermission' => App\Http\Middleware\CheckTokenPermission::class,
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/
$app->configure('amqp');
$app->register(Bschmitt\Amqp\LumenServiceProvider::class);

class_alias('Yajra\Datatables\Datatables', 'Datatables');
$app->configure('datatables');
$app->register('Yajra\Datatables\DatatablesServiceProvider');
$app->register(Illuminate\Redis\RedisServiceProvider::class);
class_alias('Illuminate\Support\Facades\Redis', 'Redis');
if(!class_exists("CalculateDistance")) {
    class_alias('App\Http\Facades\CalculateDistanceFacade', 'CalculateDistance');
}
if(!class_exists( 'RoleChecker')){
    class_alias('App\Http\Facades\RoleCheckerFacade', 'RoleChecker');
}
if(!class_exists( 'RemoteCall')){
    class_alias('App\Http\Facades\RemoteCallFacade', 'RemoteCall');
}



if(!class_exists( 'Settings')){
    class_alias('App\Http\Facades\SettingsFacade', 'Settings');
}
if(!class_exists( 'GeoLocationChecker')){
    class_alias('App\Http\Facades\GeoLocationCheckerFacade', 'GeoLocationChecker');
}

class_alias('App\Http\Facades\ImageUploadFacade', 'ImageUploader');
$app->register(App\Providers\AppServiceProvider::class);
// $app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/
$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
    require __DIR__.'/../routes/web.php';
});

/**
* if in env the ENABLE_STACK_DRIVER value is present and is true then allow to store in google cloud stack driver
*/
if(env('ENABLE_STACK_DRIVER')) {
    /**
     * fetch the json that contains google cloud credentials
     */
    $google_json= file_get_contents(storage_path().'/stackdriver.json');
    $data = json_decode($google_json,true);
    /**
     * overwriting the monolog
     */
    $app->configureMonologUsing(function ($monolog) use ($data){
        /**
         * Calling LogginClient to pass the project id and the location of the json file
         */
//        $logging = new \Google\Cloud\Logging\LoggingClient([
//            'projectId' => $data['project_id'],
//            'keyFilePath' => storage_path() . '/stackdriver.json'
//        ]);
        /**
         * calling the MonologStackDriverHandler which is the package to call the google cloud logger
         */
//        $stackdriverHandler = new \MonologStackdriverHandler\MonologStackdriverHandler($data['project_id'], 'order-service', [
//            'keyFilePath' => storage_path() . '/stackdriver.json'
//        ],$logging);
        $stackdriverHandler = new GoogleStackDriverHandler($data['project_id'], 'location-service');
        $monolog->pushHandler($stackdriverHandler);
        return $monolog;
    });
}

return $app;
