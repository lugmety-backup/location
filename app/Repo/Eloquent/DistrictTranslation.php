<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 2:55 PM
 */

namespace App\Repo\Eloquent;




use App\Repo\DistrictTranslationInterface;

class DistrictTranslation implements DistrictTranslationInterface
{
    protected $districtTranslation;

    /**
     * CountryTranslation constructor.
     * @param $countryTranslation
     */
    public function __construct(\App\DistrictTranslation $districtTranslation)
    {
        $this->districtTranslation = $districtTranslation;
    }

    public function districtTranslationGetAll()
    {
        // TODO: Implement countryTranslationGetAll() method.
        return $this->districtTranslation->all();

    }

    public function districtTranslationGetByDistrictId($id)
    {
        return $this->districtTranslation->where('district_id',$id)->get();
    }

    public function districtTranslationGetSpecific($id)
    {
        // TODO: Implement countryTranslationGetSpecific() method.
        return $this->districtTranslation->findOrFail($id);
    }

    public function districtTranslationGetSpecificByCode($code,$district_id)
    {
        $translation= $this->districtTranslation->where([['lang_code',$code],['district_id',$district_id]])->get();
        if($translation->count()==0){
            $translation= $this->districtTranslation->where([['lang_code','en'],['district_id',$district_id]])->get();
        }
        return $translation;
    }

    public function districtTranslationCreate(array $attributes)
    {
        // TODO: Implement countryTranslationCreate() method.
        //dd($attributes);
        return $this->districtTranslation->create($attributes);
    }

    public function districtTranslationUpdate($id, array $attributes)
    {
        // TODO: Implement countryTranslationUpdate() method.
        $con=$this->districtTranslation->findOrFail($id)->update($attributes);
        $con=$this->districtTranslation->findOrFail($id)->fill($attributes);
        return $con;
    }

    public function districtTranslationUpdateOrCreate(array $attributes)
    {

        $translation=$this->districtTranslation->where([
            ['lang_code',$attributes['lang_code']]
            , ['district_id',$attributes['district_id']]
        ])->get();
        if($translation->count()!=0){
            $con=$this->districtTranslation->where([
                ['lang_code',$attributes['lang_code']]
                , ['district_id',$attributes['district_id']]
            ])->update($attributes);
        }
        else{
            $con=$this->districtTranslation->create($attributes);
        }
        return $con;
    }

    public function districtTranslationDelete($id)
    {
        // TODO: Implement countryTranslationDelete() method.
        $data=$this->districtTranslation->findOrFail($id)->delete();
        return true ;
    }



}