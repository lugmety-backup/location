<?php

namespace App\Providers;


use App\Repo\BillingAddressInterface;
use App\Repo\CityInterface;
use App\Repo\CityTranslationInterface;
use App\Repo\CountryLanguageInterface;
use App\Repo\CountryMetaInterface;
use App\Repo\CountryTranslationInterface;
use App\Repo\Eloquent\BillingAddressRepo;
use App\Repo\Eloquent\CityRepo;
use App\Repo\Eloquent\CityTranslationRepo;
use App\Repo\DistrictInterface;
use App\Repo\DistrictTranslationInterface;
use App\Repo\Eloquent\CountryLanguageRepo;
use App\Repo\Eloquent\CountryMetaRepo;
use App\Repo\Eloquent\CountryTranslation;
use App\Repo\Eloquent\DistrictRepo;
use App\Repo\Eloquent\DistrictTranslation;
use App\Repo\Eloquent\LanguageRepo;
use App\Repo\Eloquent\ShippingAddressAdditionalFieldRepo;
use App\Repo\Eloquent\ShippingAddressMetaRepo;
use App\Repo\Eloquent\ShippingAddressRepo;
use App\Repo\LanguageInterface;
use App\Repo\ShippingAddressAdditionalFieldInterface;
use App\Repo\ShippingAddressInterface;
use App\Repo\ShippingAddressMetaInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repo\CountryInterface', 'App\Repo\Eloquent\CountryRepo');
        $this->app->bind(CountryTranslationInterface::class,CountryTranslation::class);
        $this->app->bind(LanguageInterface::class,LanguageRepo::class);
        $this->app->bind(CountryMetaInterface::class,CountryMetaRepo::class);
        $this->app->bind(CountryLanguageInterface::class,CountryLanguageRepo::class);
        $this->app->bind(CityInterface::class,CityRepo::class);
        $this->app->bind(CityTranslationInterface::class,CityTranslationRepo::class);
        $this->app->bind(DistrictInterface::class,DistrictRepo::class);
        $this->app->bind(DistrictTranslationInterface::class,DistrictTranslation::class);
        $this->app->bind(BillingAddressInterface::class,BillingAddressRepo::class);
        $this->app->bind(ShippingAddressInterface::class,ShippingAddressRepo::class);
        $this->app->bind(ShippingAddressMetaInterface::class,ShippingAddressMetaRepo::class);
        $this->app->bind(ShippingAddressAdditionalFieldInterface::class,ShippingAddressAdditionalFieldRepo::class);
        //$this->app->bind();
        $this->app->bind('roleChecker', 'App\Http\RoleCheckerFacade');
        $this->app->bind("remoteCall",'App\Http\RemoteCallFacade');
        $this->app->bind("imageUploader",'App\Http\ImageUploadFacade');
        $this->app->bind('settings', 'App\Http\SettingsFacade');
        $this->app->bind('calculateDistance','App\Http\CalculateDistanceFacade');
        $this->app->bind('geolocationchecker','App\Http\GeoLocationCheckerFacade');
    }


    public function boot()
    {
        /**
         * takes the google cloud credential
         */
        $array = [
            "type" => env('STACK_TYPE'),
            "project_id" => env('STACK_PROJECT_ID'),
            "private_key_id" => env('STACK_PRIVATE_KEY_ID'),
            "private_key" => str_replace('\n', "\n", env('STACK_PRIVATE_KEY')),
            "client_email" => env('STACK_CLIENT_EMAIL'),
            "client_id" => env('STACK_CLIENT_ID'),
            "auth_uri" => env('STACK_AUTH_URI'),
            "token_uri" => env('STACK_TOKEN_URL'),
            "auth_provider_x509_cert_url" => env('STACK_AUTH_PROVIDER_x509_CERT_URL'),
            "client_x509_cert_url" => env('STACK_CLIENT_X509_CERT_URL')
        ];
        /**
         * the path to create and store the json
         */
        $path = storage_path() . '/stackdriver.json';
        /**
         * open the file
         */
        $fp = fopen($path, 'w');
        /**
         * store array in json with pretty print
         */
        fwrite($fp, json_encode($array, JSON_PRETTY_PRINT));
        fclose($fp);
    }
}
