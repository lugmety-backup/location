<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 5:02 PM
 */

namespace App\Http\Controllers;

use App\Repo\CountryLanguageInterface;
use App\Repo\LanguageInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Input;
use Validator;
use RoleChecker;
class LanguageController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $language;
    private $countryLanguage;
    public function __construct(LanguageInterface $language, CountryLanguageInterface $countryLanguage)
    {
        $this->language=$language;
        $this->countryLanguage = $countryLanguage;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function publicIndex()
    {

        try{
            $request['limit'] = Input::get("limit", 10);
            /*
     * validation of limit param if limit param is not null
     * */
            if (!is_null($request['limit'])) {
                $rules = [
                    "limit" => "sometimes|numeric|min:1",
                ];
                $validator = Validator::make($request, $rules);
                if ($validator->fails()) {
                    $request['limit'] = 10;
                }
            }

            $language= $this->language->languageGetAllByStatusAndLimit($request['limit']);
            try{
                if(!$language->first()){
                    throw new \Exception();
                }
            }
            catch (\Exception $ex){
                return response()->json([
                    'status'=>'404',
                    "message"=>'Empty Record'
                ],404);
            }
            return response()->json([
                'status'=>'200',
                'data'=>$language->appends(['limit' =>  $request['limit']])->withPath("public/language")
            ],200);
        }
        catch (QueryException $e){

            return response()->json([
                'status'=>'404',
                "message" => "Language could not be found"
            ],404);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'404',
                "message" => "Language could not be found"
            ],404);
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function adminIndex()
    {
        try {
            /*
        * if logged in user doesnot have role customer ,he/she can view country with either status 1 0r 0 or both at once when status is not passed
        * */
            if (!RoleChecker::hasRole('customer')) {
                $language = $this->language->languageGetAll();
                try {
                    if (!$language->first()) {
                        throw new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        'status' => '404',
                        "message" => 'Empty Record'
                    ], 404);
                }
                return \Datatables::of($language)->make(true);
            } else {
                return response()->json([
                    "status" => "403",
                    "message" => "You have no access"
                ],403);
            }
        }
        catch (QueryException $e){

            return response()->json([
                'status'=>'404',
                "message" => "Language could not be found"
            ],404);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'404',
                "message" => "Language could not be found"
            ],404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->has("code")) $request->code = strtolower($request->code);

            try {
                $this->validate($request, [
                    'code' => 'required|unique:language',
                    'name' => 'required|alpha',
                    'status' => 'integer|min:0|max:1'
                ]);
            }
            catch (\Exception $e){
                return response()->json([
                    'status'=>'422',
                    'message'=>$e->response->original
                ],422);
            }
            $request=$request->all();
            $request['status']=Input::get('status',1);//1 means active
            $language= $this->language->languageGetSpecificByCode(strtolower($request['code']));

            try {
                if ($language->count() != 0) {

                    throw new \Exception();
                }
            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'409',
                    'message'=>"Language could not be created due to duplicate entry"
                ],409);
            }
            $language=$this->language->languageCreate($request);
            return response()->json([
                'status'=>'200',
                'message'=>'Language created successfully'
            ],200);

        }

        catch (\Exception $e){
            return response()->json([
                'status'=>'500',
                'message'=>"Error creating language"
            ],500);
        }

    }

    /**
     * Display the language to public where status is enabled.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function adminShow($id)
    {
        try{
            /*
        * if logged in user doesnot have role customer ,he/she can view country with either status 1 0r 0 or both at once when status is not passed
        * */
            if (!RoleChecker::hasRole('customer')) {
                $language= $this->language->languageGetSpecific($id);
                return response()->json([
                    'status'=>'200',
                    'data'=>$language
                ],200);
            }
            else{
                return response()->json([
                    "status" => "403",
                    "message" => "You have no access"
                ],403);
            }
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Language could not be found"
            ],404);
        }
catch (ModelNotFoundException  $ex){
    return response()->json([
        'status'=>'404',
        'message'=>"Requested Language could not be found"
    ],404);
}
        catch (\Exception $ex){
    return response()->json([
        'status'=>'404',
        'message'=>"Requested Language could not be found"
    ],404);
}
    }


    /**
     * Display the language to public where status is enabled.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function publicShow($id)
{
    try{
        $language= $this->language->languageGetSpecificAndStatusEnabled($id);
        return response()->json([
            'status'=>'200',
            'data'=>$language
        ],200);
    }
    catch (QueryException $ex){
        return response()->json([
            'status'=>'404',
            'message'=>"Requested Language could not be found"
        ],404);
    }
    catch (ModelNotFoundException  $ex){
        return response()->json([
            'status'=>'404',
            'message'=>"Requested Language could not be found"
        ],404);
    }
    catch (\Exception $ex){
        return response()->json([
            'status'=>'404',
            'message'=>"Requested Language could not be found"
        ],404);
    }
}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
{
    try {
        if($request->has("code")) $request->code = strtolower($request->code);

        try
        {
            $this->validate($request, [
                'code' => 'required|unique:language,code,'.$id,
                'name' => 'required|alpha',
                'status' => 'integer|min:0|max:1',
            ]);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
        $request = $request->all();

        $language = $this->language->languageUpdate($id, $request);
        return response()->json([
            'status' => '200',
            'message' => 'Language Updated successfully',
            'data' => $language
        ]);

    }
    catch (QueryException $ex){
        return response()->json([
            'status'=>'409',
            'message'=>"Language could not be created due to duplicate entry"
        ],409);
    }

    catch (ModelNotFoundException  $ex){
        return response()->json([
            'status'=>'404',
            'message'=>"Requested Language could not be found"
        ],404);
    }
    catch (\Exception $e){
        return response()->json([
            'status'=>'422',
            'message'=>$e->response->original
        ],422);
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
{
    try{

         $language= $this->language->languageDelete($id);
         $checkAttachedLanguage = $this->countryLanguage->getAllCountryByLanguage($language->id);
         if(count($checkAttachedLanguage) !== 0){
             return response()->json([
                 "status" => "403",
                 "message" => "The language which is attached to country(s) is not allowed to delete."
             ],403);
         }

        if($language->code =="en"){
            return response()->json([
                "status" => "403",
                "message" => "English language is not allowed to delete"
            ],403);
        }
        $language->delete();
        return response()->json([
            'status'=>'200',
            'message'=>"Language deleted successfully"
        ]);
    }
    catch (QueryException $ex){
        return response()->json([
            'status'=>'404',
            'message'=>"Requested Language could not be found"
        ],404);
    }
    catch (ModelNotFoundException  $ex){
        return response()->json([
            'status'=>'404',
            'message'=>"Requested Language could not be found"
        ],404);
    }
    catch (\Exception $ex){
        return response()->json([
            'status'=>'404',
            'message'=>"Requested Language could not be found"
        ],404);
    }
}

}