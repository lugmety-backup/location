<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/13/2017
 * Time: 12:07 PM
 */

namespace App\Repo\Eloquent;


use App\CityTranslation;
use App\Repo\CityTranslationInterface;


class CityTranslationRepo implements CityTranslationInterface
{
    protected $cityTranslation;

    /**
     * CityTranslationRepo constructor.
     * @param $cityTranslation
     */
    public function __construct(CityTranslation $cityTranslation)
    {
        $this->cityTranslation = $cityTranslation;
    }

    public function cityTranslationGetAll()
    {
        return  $this->cityTranslation->all();
    }

    public function cityTranslationGetByCityId($id)
    {
        return $this->cityTranslation->where('city_id',$id)->get();
    }

    public function cityTranslationGetSpecific($id)
    {
        return $this->cityTranslation->findOrFail($id);
    }

    public function cityTranslationGetSpecificByCode($code,$city_id)
    {
        $translation= $this->cityTranslation->where([['lang_code',$code],['city_id',$city_id]])->get();
        if($translation->count()==0){
            $translation= $this->cityTranslation->where([['lang_code','en'],['city_id',$city_id]])->get();
        }
        return $translation;
    }

    public function cityTranslationUpdateOrCreate(array $attributes)
    {
        $translation=$this->cityTranslation->where([
            ['lang_code',$attributes['lang_code']]
            , ['city_id',$attributes['city_id']]
        ])->get();
        if($translation->count()!=0){
            $con=$this->cityTranslation->where([
                ['lang_code',$attributes['lang_code']]
                , ['city_id',$attributes['city_id']]
            ])->update($attributes);
        }
        else{
            $con=$this->cityTranslation->create($attributes);
        }
        return $con;
    }

    public function cityTranslationCreate(array $attributes)
    {
        return $this->cityTranslation->create($attributes);
    }

    public function cityTranslationUpdate($id, array $attributes)
    {
        $city=$this->cityTranslation->findOrFail($id)->update($attributes);
        return $this->cityTranslation->fill($attributes);
    }

    public function cityTranslationDelete($id)
    {
        return $this->cityTranslation->findOrFail($id)->delete();
    }


}