<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:02 AM
 */

namespace App\Repo\Eloquent;

use App\ShippingAddress;
use App\Repo\ShippingAddressInterface;
use Illuminate\Support\Facades\Schema;

class ShippingAddressRepo implements ShippingAddressInterface
{


    private $shippingAddress;

    /**
     * CountryRepo constructor.
     * @param $country
     */
    public function __construct(ShippingAddress $shippingAddress)
    {
        $this->shippingAddress = $shippingAddress;
    }

    public function shippingAddressGetAllForDatatable($user_id)
    {
        return $this->shippingAddress->where("user_id", $user_id)->get();
    }

    public function shippingAddressPagination($filter)
    {
        $columnsList = Schema::getColumnListing("shipping_address");

        array_push($columnsList, "customer_name", "customer_email", "customer_phone");
        $order = $columnsList[0];
        if (!is_null($filter["sort_column"])) {
            foreach ($columnsList as $columnName) {
                if ($columnName == $filter["sort_column"]) {
                    $order = $columnName;
                    break;
                }
            }
        }

        $shippingAddress = $this->shippingAddress
            ->leftjoin("shipping_address_additional_field", "shipping_address.id", "=", "shipping_address_additional_field.shipping_address_id")
            ->join("city_translation","shipping_address.city_id","=", "city_translation.city_id")
            ->join("district_translation","shipping_address.district_id","=","district_translation.district_id")
            ->select("shipping_address.*", "shipping_address_additional_field.customer_name", "shipping_address_additional_field.customer_email", "shipping_address_additional_field.customer_phone",
                "city_translation.name as city_name", "district_translation.name as district_name")
            ->where("city_translation.lang_code","=",$filter['lang'])
            ->where("district_translation.lang_code","=",$filter['lang'])
            ->where("country_id", $filter['country_id']);
        if (!is_null($filter["user_id"])) {
            $shippingAddress = $shippingAddress->where("user_id", $filter['user_id']);
        }
        if (!is_null($filter["district_id"])) {
            $shippingAddress = $shippingAddress->where("shipping_address.district_id", $filter['district_id']);
        }
        if (!is_null($filter["city_id"])) {
            $shippingAddress = $shippingAddress->where("shipping_address.city_id", $filter['city_id']);
        }

        if (!is_null($filter["search"])) {
            $value = "%" . strtolower($filter["search"]) . "%";
//            $slugedValue = "%" . $this->slugify->slugify($value) . "%";
//            dd($columnsList);
            $shippingAddress = $shippingAddress->where(function ($query) use ($columnsList, $value) {
                foreach ($columnsList as $key => $column) {
                    if ($column == "id") $column = "shipping_address.id";
                    if ($column == "created_at") $column = "shipping_address.created_at";
                    if ($column == "updated_at") $column = "shipping_address.updated_at";
                    if ($column == "city_id") $column = "shipping_address.city_id";
                    if ($column == "district_id") $column = "shipping_address.district_id";
                    if ($key === 0) {
                        $query->where($column, "like", $value);
                        continue;
                    }
                    $query->orWhere($column, "like", $value);
                }
                $query->orWhere("city_translation.name", "like", $value);
                $query->orWhere("district_translation.name", "like", $value);
            });

        }
        if ($order == "id") $order = "shipping_address.id";
        if ($order == "created_at") $order = "shipping_address.created_at";
        if ($order == "updated_at") $order = "shipping_address.updated_at";
        return $shippingAddress->orderBy($order, $filter["sort_by"])->paginate($filter['limit']);
    }

    public function checkDistrictExist($districtId)
    {
        return $this->shippingAddress->where("district_id", $districtId)->first();
    }

    public function getShippingAddressFromUserIdAndId($id, $userId)
    {//added for cost calculation
        return $this->shippingAddress->withTrashed()->where("user_id", $userId)->findOrFail($id);
    }

    public function shippingAddressGetAll($user_id)
    {
        // TODO: Implement countryGetAll() method.

        return $this->shippingAddress->where('user_id', $user_id)->get();
    }

    public function shippingAddressGetAllByCountryId($user_id, $countryId)
    {

        return $this->shippingAddress->where([['user_id', $user_id], ["country_id", $countryId]])->get();
    }

    public function shippingAddressGetAllSortedByDate($user_id)
    {
        return $this->shippingAddress->where('user_id', $user_id)->orderBy("created_at", "desc")->get();
    }

    public function shippingAddressGetAllExceptCustomer($limit)
    {
        return $this->shippingAddress->paginate($limit)->appends(["limit" => $limit])->withPath('shipping/address');
    }

    public function shippingAddressGetAllShippingAddressExceptPassedId($id, $userId, $countryId)
    {
        return $this->shippingAddress->where([
            ['user_id', $userId],
            ["id", "!=", $id],
            ["country_id", $countryId]
        ])->get();
    }

    public function shippingAddressGetSpecific($id)
    {
        return $this->shippingAddress->findOrFail($id);
    }

    public function shippingAddressGetSpecificIncludingDeleted($id)
    {
        return $this->shippingAddress->withTrashed()->findOrFail($id);
    }

    public function shippingAddressCreate(array $attributes)
    {
        return $this->shippingAddress->create($attributes);
    }

    public function shippingAddressUpdate($id, array $attributes)
    {
        $con = $this->shippingAddress->findOrFail($id)->update($attributes);
        $con = $this->shippingAddress->findOrFail($id)->fill($attributes);
        return $con;
    }

    public function shippingAddressDelete($id)
    {
        $data = $this->shippingAddress->findOrFail($id)->delete();
        return true;
    }

    public function setShippingAddressPrimary($id, $value)
    {
        $con = $this->shippingAddress->findOrFail($id);
        if ($con) {
            $con->primary = $value;
            $con->save();
        }
        return $con;
    }

    public function getShippingAddressWithPrimary($userId)
    {
        return $this->shippingAddress->where([['user_id', $userId], ['primary', 1]])->get();
    }

    public function getListOfShippingDetail(array $list)
    {
        return $this->shippingAddress->withTrashed()->select("id", "street", "district_id")->find($list);
    }


}