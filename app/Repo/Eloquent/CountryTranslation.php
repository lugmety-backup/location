<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 2:55 PM
 */

namespace App\Repo\Eloquent;




use App\Repo\CountryTranslationInterface;

class CountryTranslation implements CountryTranslationInterface
{
    protected $countryTranslation;

    /**
     * CountryTranslation constructor.
     * @param $countryTranslation
     */
    public function __construct(\App\CountryTranslation $countryTranslation)
    {
        $this->countryTranslation = $countryTranslation;
    }

    public function countryTranslationGetAll()
    {
        // TODO: Implement countryTranslationGetAll() method.
        return $this->countryTranslation->all();

    }

    public function countryTranslationGetSpecific($id)
    {
        // TODO: Implement countryTranslationGetSpecific() method.
        return $this->countryTranslation->findOrFail($id);
    }

    public function countryTranslationGetSpecificByName($name)
    {
        // TODO: Implement countryTranslationGetSpecificByName() method.
        return $this->countryTranslation->where('name',$name)->get();
    }
    public function countryTranslationGetSpecificByNameCountryId($name,$id){
        return $this->countryTranslation->where([['name',$name],['country_id',$id]])->get();
    }

    public function countryTranslationGetByCountryId($id)
    {
        return $this->countryTranslation->where('country_id',$id)->get();
    }

    public function countryTranslationCreate(array $attributes)
    {
        // TODO: Implement countryTranslationCreate() method.
        return $this->countryTranslation->create($attributes);
    }

    public function countryTranslationUpdate($id, array $attributes)
    {
        // TODO: Implement countryTranslationUpdate() method.
        $con=$this->countryTranslation->findOrFail($id)->update($attributes);
        $con=$this->countryTranslation->findOrFail($id)->fill($attributes);
        return $con;
    }

    public function countryTranslationUpdateOrCreate(array $attributes)
    {
        $translation=$this->countryTranslation->where([
            ['lang',$attributes['lang']]
            , ['country_id',$attributes['country_id']]
        ])->get();
        if($translation->count()!=0){
            $con=$this->countryTranslation->where([
                ['lang',$attributes['lang']]
                , ['country_id',$attributes['country_id']]
            ])->update($attributes);
        }
        else{
            $con=$this->countryTranslation->create($attributes);
        }
        return $con;
    }

    public function countryTranslationDelete($id)
    {
        // TODO: Implement countryTranslationDelete() method.
        $data=$this->countryTranslation->findOrFail($id)->delete();
        return true ;
    }


}