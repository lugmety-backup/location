<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 5:17 PM
 */

namespace App\Repo;


interface CountryMetaInterface
{
    public function countryMetaGetAll();

    public function countryMetaGetSpecific($id);

    public function countryMetaGetAllByCountryId($id);

    public function countryMetaGetSpecificByCode($code,$id);

    public function countryMetaCreate(array $attributes);

    public function countryMetaUpdate($id, array $attributes);

    public function countryMetaDelete($id);

    public function countryMetaUpdateOrCreate(array $attributes);
}