<?php

/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/9/2017
 * Time: 10:56 AM
 */
use App\City;
use Laravel\Lumen\Testing\DatabaseTransactions;
class CityTest extends TestCase
{
    use DatabaseTransactions;
    public function __construct(){
        $access_token=$this->getAccessToken();
        $this->access_token1=$access_token[0];//having user access
        $this->access_token2=$access_token[1];//not having access

    }


    public function testGetCorrectSpecificCity(){

        $city=factory(City::class)->create();
        $translation=factory(\App\CityTranslation::class)->create(['city_id'=>$city->id]);

        $res=$this->get("city/$city->id");
        $this->seeJson([
            'data'=>[
                'id'=>$city->id,
                'country_id'=>$city->country_id,
                'timezone'=>$city->timezone,
                'tax_percentage'=>$city->tax_percentage,
                'status'=>$city->status,
                'deleted_at'=>null,
                'created_at'=>(string)$city->created_at,
                'updated_at'=>(string)$city->updated_at,
                'lang_code'=>$translation->lang_code,
                'name'=>$translation->name,
                ],'status'=>'200'
        ])->assertResponseStatus(200);
    }
    public function testGetCorrectSpecificCityNotExistedInDB(){
        $city=factory(City::class)->create();
        \App\City::whereId($city->id)->delete();
        $this->get('/city/city->id')->assertResponseStatus(404);
    }

    public function testGetCorrectSpecificCityWithGivenLanguage(){ //Get Specific record with given language

        $city=factory(City::class)->create();

        $translation=factory(\App\CityTranslation::class)->create(['city_id'=>$city->id,'lang_code'=>'ar']);

        $res=$this->get("/city/$city->id?lang=ar",[
            'Authorization'=>'Bearer '.$this->access_token1
        ]);


        $this->seeJson([
            'data'=>[
                'id'=>$city->id,
                'country_id'=>$city->country_id,
                'timezone'=>$city->timezone,
                'tax_percentage'=>$city->tax_percentage,
                'status'=>$city->status,
                'deleted_at'=>null,
                'created_at'=>(string)$city->created_at,
                'updated_at'=>(string)$city->updated_at,
                'lang_code'=>$translation->lang_code,
                'name'=>$translation->name,
            ],'status'=>'200'
        ])->assertResponseStatus(200);
    }
    public function testGetCorrectSpecificCityWithEnglishInsteadOfGivenLanguage(){//Get Specific record with english as given language as given language name donot exist

        $city=factory(City::class)->create();

        $translation=factory(\App\CityTranslation::class)->create(['city_id'=>$city->id,'lang_code'=>'en']);

        $res=$this->get("/city/$city->id?lang=xx");

        $this->seeJson([
            'data'=>[
                'id'=>$city->id,
                'country_id'=>$city->country_id,
                'timezone'=>$city->timezone,
                'tax_percentage'=>$city->tax_percentage,
                'status'=>$city->status,
                'deleted_at'=>null,
                'created_at'=>(string)$city->created_at,
                'updated_at'=>(string)$city->updated_at,
                'lang_code'=>'en',
                'name'=>$translation->name,
            ],'status'=>'200'
        ])->assertResponseStatus(200);
    }

    public function testCreateCity(){//testing for creating city for valid input
        $country=factory(\App\Country::class)->create();
        $t=\Illuminate\Support\Facades\DB::table('language')->where('code','en')->first();
        $test=\Illuminate\Support\Facades\DB::table('country_language')->insert(
            [
                'country_id' => $country->id,
                'language_id' => $t->id
            ]
        );
        $request['country_id']=$country->id;
        $request['timezone']="GMT-5:45";
        $request['tax_percentage']=13.33;
        $request['status']=1;
        $request['translation']=['en'=>['name'=>'Bagmati']];
        $this->post('city',$request)->seeJson([
            'status'=>'200'
        ])->assertResponseStatus(200);
    }
    public function testCreateCityWithInvalidRequest(){//testing for creating city for invalid input
        //$country=factory(\App\Country::class)->create();
        $this->post('/city',[" "],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(422);
    }
    public function testCreateCityWithInvalidRequestInTranslation(){//testing for creating city for invalid input in translation
        $country=factory(\App\Country::class)->create();
        $request['country_id']=$country->id;
        $request['timezone']="GMT-5:45";
        $request['tax_percentage']=13.33;
        $request['status']=1;
        $request['translation']=['en'=>['']];
        $this->post('/city',$request,[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(422);
    }
    public function testCreateCityWithLanguageNotExisted(){//testing for creating city wit invalid language
        $country=factory(\App\Country::class)->create();
        $request['country_id']=$country->id;
        $request['timezone']="GMT-5:45";
        $request['tax_percentage']=13.33;
        $request['status']=1;
        $request['translation']=['unknown'=>['name'=>'Bagmati']];
        $this->post('/city',$request,[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);
    }
    public function testCreateCityWithNoCountry(){//testing for creating city with Country Not Found
        $country=factory(\App\Country::class)->create();
        \App\Country::whereId($country->id)->delete();
        $request['country_id']=$country->id;
        $request['timezone']="GMT-5:45";
        $request['tax_percentage']=13.33;
        $request['status']=1;
        $request['translation']=['en'=>['name'=>'Bagmati']];
        $this->post('/city',$request,[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);
    }
    public function testUpdate(){
        $country=factory(\App\Country::class)->create();
        $t=\Illuminate\Support\Facades\DB::table('language')->where('code','en')->first();
        $test=\Illuminate\Support\Facades\DB::table('country_language')->insert(
            [
                'country_id' => $country->id,
                'language_id' => $t->id
            ]
        );
        $city=factory(City::class)->create();
        $translation=factory(\App\CityTranslation::class)->create(['city_id'=>$city->id]);
        $request['country_id']=$country->id;
        $request['timezone']="GMT-5:45";
        $request['tax_percentage']=13.33;
        $request['status']=1;
        $request['translation']=['en'=>['name'=>'Bagmati']];
        //dd($request);
        $this->put("city/$city->id",$request,[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(200);
    }
    public function testUpdateWithInvalidCityId(){
        $city=factory(City::class)->create();
        City::whereId($city->id)->delete();
        $translation=factory(\App\CityTranslation::class)->create(['city_id'=>$city->id]);
        $request['country_id']=$city->id;
        $request['timezone']="GMT-5:45";
        $request['tax_percentage']=13.33;
        $request['status']=1;
        $request['translation']=['en'=>['name'=>'Bagmati']];
        //dd($request);
        $this->put("city/$city->id",$request,[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);
    }
    public function testUpdateWithInvalidCountryId(){
        $country=factory(\App\Country::class)->create();
        $city=factory(City::class)->create();
        \App\Country::whereId($country->id)->delete();
        $translation=factory(\App\CityTranslation::class)->create(['city_id'=>$city->id]);
        $request['country_id']=$country->id;
        $request['timezone']="GMT-5:45";
        $request['tax_percentage']=13.33;
        $request['status']=1;
        $request['translation']=['en'=>['name'=>'Bagmati']];
        //dd($request);
        $this->put("city/$city->id",$request,[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);
    }
    public function testDelete(){
        $city=factory(City::class)->create();
        $translation=factory(\App\CityTranslation::class)->create(['city_id'=>$city->id]);
        $this->delete("city/$city->id")->assertResponseStatus(200);
    }
    public function testDeleteWithInvalidCityId(){
        $city=factory(City::class)->create();
        City::whereId($city->id)->delete();
        $this->delete("city/$city->id")->assertResponseStatus(404);
    }

}
