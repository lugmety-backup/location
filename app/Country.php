<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/9/2017
 * Time: 5:48 PM
 */

namespace App;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
class Country extends Model
{
    use SoftDeletes;
    protected $guarded=['id'];
    protected $table='country';
    protected $dates = ['deleted_at'];
    protected $hidden=array('deleted_at','updated_at',"created_at","pivot");

    public function countryTranslation(){
        return $this->hasMany('App\CountryTranslation','country_id');
    }

    public function countryMeta(){
        return $this->hasMany('App\CountryMeta','country_id');
    }

    public function language(){
        return $this->belongsToMany('App\Language','country_language','country_id','language_id');
    }

    public function lang(){
        return $this->belongsToMany('App\Language');
    }

    public function city(){
        return $this->hasMany('App\City','country_id');
    }

    public function cityTranslation(){
        return $this->hasManyThrough('App\CityTranslation', 'App\City');
    }

    public function setCodeAttribute($value){
        $this->attributes['code'] = strtolower($value);
    }

    public function getCurrencyCodeAttribute($value){
        return strtoupper($value);
    }

    public function getCodeAttribute($value){
        return strtolower($value);
    }

    public function getFlagAttribute($value){
        if(empty($value)){
            return "";
        }
        return Config::get('config.image_service_base_url_cdn').$value;
    }

}