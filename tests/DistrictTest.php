<?php

/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/9/2017
 * Time: 10:56 AM
 */
use App\City;
use Laravel\Lumen\Testing\DatabaseTransactions;
class DistrictTest extends TestCase
{
    use DatabaseTransactions;
    public function __construct(){
        $access_token=$this->getAccessToken();
        $this->access_token1=$access_token[0];//having user access
        $this->access_token2=$access_token[1];//not having access

    }


    public function testGetCorrectSpecificDistrict(){

        $district=factory(\App\District::class)->create();

        $translation=factory(\App\DistrictTranslation::class)->create(['district_id'=>$district->id]);

        $res=$this->get("/district/$district->id");

        $this->seeJson([
            'data'=>[
                'id'=>$district->id,
                'city_id'=>$district->city_id,
                'status'=>$district->status,
                'created_at'=>(string)$district->created_at,
                'updated_at'=>(string)$district->updated_at,
                'lang_code'=>$translation->lang_code,
                'name'=>$translation->name,
                ],'status'=>'200'
        ])->assertResponseStatus(200);
    }


    public function testGetCorrectSpecificDistrictNotExistedInDB(){
        $district=factory(\App\District::class)->create();
        \App\District::whereId($district->id)->delete();
        $this->get('/district/district->id')->assertResponseStatus(404);
    }

    public function testGetCorrectSpecificDistrictWithGivenLanguage(){ //Get Specific record with given language

        $district=factory(\App\District::class)->create();

        $translation=factory(\App\DistrictTranslation::class)->create(['district_id'=>$district->id,'lang_code'=>'ar']);

        $res=$this->get("/district/$district->id?lang=ar",[
            'Authorization'=>'Bearer '.$this->access_token1
        ]);

        $this->seeJson([
            'data'=>[
                'id'=>$district->id,
                'city_id'=>$district->city_id,
                'status'=>$district->status,
                'created_at'=>(string)$district->created_at,
                'updated_at'=>(string)$district->updated_at,
                'lang_code'=>$translation->lang_code,
                'name'=>$translation->name,
            ],'status'=>'200'
        ])->assertResponseStatus(200);
    }
    public function testGetCorrectSpecificDistrictWithEnglishInsteadOfGivenLanguage(){//Get Specific record with english as given language as given language name donot exist

        $district=factory(\App\District::class)->create();

        $translation=factory(\App\DistrictTranslation::class)->create(['district_id'=>$district->id,'lang_code'=>'en']);

        $res=$this->get("/district/$district->id?lang=ar",[
            'Authorization'=>'Bearer '.$this->access_token1
        ]);

        $this->seeJson([
            'data'=>[
                'id'=>$district->id,
                'city_id'=>$district->city_id,
                'status'=>$district->status,
                'created_at'=>(string)$district->created_at,
                'updated_at'=>(string)$district->updated_at,
                'lang_code'=>'en',
                'name'=>$translation->name,
            ],'status'=>'200'
        ])->assertResponseStatus(200);
    }

    public function testCreateDistrict(){//testing for creating district for valid input
        $city=factory(\App\City::class)->create();
        $request['city_id']=$city->id;
        $request['status']=1;
        $request['translation']=['en'=>['name'=>'Kathmandu']];
        $this->post('/district',$request,[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(200);
    }
    public function testCreateDistrictWithInvalidRequest(){//testing for creating district for invalid input
        $this->post('/district',[" "],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(422);
    }
    public function testCreateDistrictWithInvalidRequestInTranslation(){//testing for creating district for invalid input in translation
        $city=factory(\App\City::class)->create();
        $request['city_id']=$city->id;
        $request['status']=1;
        $request['translation']=['en'=>['']];
        $this->post('/district',$request,[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(422);
    }
    public function testCreateDistrictWithLanguageNotExisted(){//testing for creating district with invalid language
        $city=factory(\App\City::class)->create();
        $request['city_id']=$city->id;
        $request['status']=1;
        $request['translation']=['unknown'=>['name'=>'Bagmati']];
        $this->post('/district',$request,[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);
    }
    public function testCreateDistrictWithNoCity(){//testing for creating district with City Not Found
        $city=factory(\App\City::class)->create();
        \App\City::whereId($city->id)->delete();
        $request['city_id']=$city->id;
        $request['status']=1;
        $request['translation']=['en'=>['name'=>'Bagmati']];
        $this->post('/district',$request,[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);
    }
    public function testUpdate(){
        $city=factory(\App\City::class)->create();
        $district=factory(\App\District::class)->create();
        $translation=factory(\App\DistrictTranslation::class)->create(['district_id'=>$district->id]);
        $request['city_id']=$city->id;
        $request['status']=1;
        $request['translation']=['en'=>['name'=>'Bagmati']];
        //dd($request);
        $this->put("district/$district->id",$request,[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(200);
    }
    public function testUpdateWithInvalidDistrictId(){
        $district=factory(\App\District::class)->create();
        \App\District::whereId($district->id)->delete();
        $translation=factory(\App\DistrictTranslation::class)->create(['district_id'=>$district->id]);
        $request['city_id']=$district->id;
        $request['status']=1;
        $request['translation']=['en'=>['name'=>'Kathmandu']];
        //dd($request);
        $this->put("district/$district->id",$request,[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);
    }
    public function testUpdateWithInvalidCityId(){
        $city=factory(\App\City::class)->create();
        $district=factory(\App\District::class)->create();
        \App\City::whereId($city->id)->delete();
        $translation=factory(\App\DistrictTranslation::class)->create(['district_id'=>$district->id]);
        $request['city_id']=$city->id;
        $request['status']=1;
        $request['translation']=['en'=>['name'=>'Kathmandu']];
        //dd($request);
        $this->put("district/$district->id",$request,[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);
    }
    public function testDelete(){
        $district=factory(\App\District::class)->create();
        $translation=factory(\App\DistrictTranslation::class)->create(['district_id'=>$district->id]);
        $this->delete("district/$district->id")->assertResponseStatus(200);
    }
    public function testDeleteWithInvalidDistrictId(){
        $district=factory(\App\District::class)->create();
        \App\District::whereId($district->id)->delete();
        $this->delete("district/$district->id")->assertResponseStatus(404);
    }

}
