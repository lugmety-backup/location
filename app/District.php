<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:05 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class District extends Model
{
    use SoftDeletes;
    protected $guarded=['id'];
    protected $table='district';
    protected $hidden=array('deleted_at');
    protected $dates = ['deleted_at'];

    public function city(){
        return $this->belongsTo("App\City","city_id");
    }
    public function districtTranslation(){
        return $this->hasMany('App\DistrictTranslation','district_id');
    }

    public function shippingAddress(){
        return $this->hasMany('App\ShippingAddress','district_id');
    }
}