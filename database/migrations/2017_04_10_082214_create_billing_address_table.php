<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_address', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('address_name');
            $table->string('first_name',50);
            $table->string('middle_name',50)->nullable();
            $table->string('email');
            $table->string('postal_code',50);
            $table->string('last_name',50);
            $table->string('address_line_one');
            $table->string('address_line_two')->nullable();
            $table->string('district',100);
            $table->string('city',100);
            $table->string('country',50);
            $table->smallInteger('primary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_address');
    }
}
