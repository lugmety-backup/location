<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 5:18 PM
 */

namespace App\Repo\Eloquent;


use App\CountryMeta;
use App\Repo\CountryMetaInterface;

class CountryMetaRepo implements CountryMetaInterface
{
    private $countryMeta;



    /**
     * CountryRepo constructor.
     * @param $countryMeta
     */
    public function __construct(CountryMeta $countryMeta)
    {
        $this->countryMeta = $countryMeta;
    }

    public function countryMetaGetAll()//get all the countryMeta information from database
    {
        // TODO: Implement countryMetaGetAll() method.

        return $this->countryMeta->all();
    }

    public function countryMetaGetAllByCountryId($id)
    {
        return $this->countryMeta->where('country_id',$id)->get();
    }

    public function countryMetaGetSpecificByCode($code,$id){
        return $this->countryMeta->where([['country_id',$id],['meta_key',$code]])->get();
    }

    public function countryMetaGetSpecific($id)
    {
        return $this->countryMeta->findOrFail($id);
    }

    public function countryMetaCreate(array $attributes)
    {
        return $this->countryMeta->create($attributes) ;
    }

    public function countryMetaUpdate($id, array $attributes)
    {
        $con=$this->countryMeta->findOrFail($id)->update($attributes);
        $con=$this->countryMeta->findOrFail($id)->fill($attributes);
        return $con;
    }

    public function countryMetaUpdateOrCreate(array $attributes)
    {
        $countryMeta=$this->countryMeta->where([
            ['country_id',$attributes['country_id']]
            , ['meta_key',$attributes['meta_key']]
        ])->get();
        if($countryMeta->count()!=0){
            $con=$this->countryMeta->where([
                ['country_id',$attributes['country_id']]
                , ['meta_key',$attributes['meta_key']]
            ])->update($attributes);
        }
        else{
            $con=$this->countryMeta->create($attributes);
        }
        return $con;
    }

    public function countryMetaDelete($id)
    {
        $data=$this->countryMeta->findOrFail($id)->delete();
        return true ;

    }
}