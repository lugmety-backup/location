<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 12:51 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $guarded=['id'];
    protected $fillable= ['code','name','status'];
    protected $table='language';
    protected $hidden=array('pivot','updated_at',"created_at");
    public function country(){
        return $this->belongsToMany('App\Country');
    }
    public function setCodeAttribute($value){
        $this->attributes['code'] = strtolower($value);
    }
}