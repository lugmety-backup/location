<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterShippingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipping_address', function (Blueprint $table) {
            $table->unsignedInteger('country_id');
            //$table->foreign('country_id')->references('id')->on('country')->onDelete('cascade');
            $table->unsignedInteger('city_id');
            //$table->foreign('city_id')->references('id')->on('city')->onDelete('cascade');
            $table->string("zip_code",10)->nullable();
            $table->string("house_number",40)->nullable();
            $table->string("floor_number",40)->nullable();
            $table->string("building_name",40)->nullable();
            $table->string("office_name",40)->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipping_address', function (Blueprint $table) {
            //
        });
    }
}
