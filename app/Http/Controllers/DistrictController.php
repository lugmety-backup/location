<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 5:02 PM
 */

namespace App\Http\Controllers;

use App\Repo\CityInterface;
use App\Repo\CountryInterface;
use App\Repo\CountryLanguageInterface;
use App\Repo\DistrictInterface;
use App\Repo\DistrictTranslationInterface;
use App\Repo\LanguageInterface;
use App\Repo\ShippingAddressInterface;
use Geocoder\Query\ReverseQuery;
use RoleChecker;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;
use Yajra\Datatables\Datatables;
use RemoteCall;
use Illuminate\Support\Facades\Config;

class DistrictController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $district;
    private $city;
    private $districtTranslation;
    private $language;
    protected $country;
    protected $countryLanguage;
    protected $shippingAddress;

    public function __construct(DistrictInterface $district, CityInterface $city,
                                DistrictTranslationInterface $districtTranslation,
                                LanguageInterface $language,
                                ShippingAddressInterface $shippingAddress,
                                CountryInterface $country,
                                CountryLanguageInterface $countryLanguage)
    {
        $this->district = $district;
        $this->city = $city;
        $this->districtTranslation = $districtTranslation;
        $this->language = $language;
        $this->country = $country;
        $this->countryLanguage = $countryLanguage;
        $this->shippingAddress = $shippingAddress;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $request['limit'] = Input::get("limit", 10);
        $request['lang'] = "en";
        /*
         *
    * validation of limit param if limit param is not null
    * */
        if (!is_null($request['limit'])) {
            $rules = [
                "limit" => "sometimes|numeric|min:1",
                "city_id" => "sometimes|integer|min:1"
            ];

            try {
                $this->validate($request, $rules);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => '422',
                    'message' => $e->response->original
                ], 422);
            }
        }

        /**
         * Checking the given laguage exists in language table if not exist default en is assigned
         *
         */
        $request['lang'] = "en";

        try {
            /**
             * Get all district records from district table .. the records are
             * returned which is equals to status if status is given in parameter
             * else all the records are returned
             *
             */
            $districts = $this->district->districtGetAllPagination(null, $request['city_id'], $request["limit"]);

            try {
                if (count($districts) == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }

            foreach ($districts as $district) {
                $districtTranslation = $this->districtTranslation->districtTranslationGetSpecificByCode($request['lang'], $district['id']);
                //return response()->json($districtTranslation);
                $district['lang_code'] = $districtTranslation[0]['lang_code'];
                $district['name'] = $districtTranslation[0]['name'];
            }
            return response()->json([
                'status' => '200',
                'data' => $districts->appends(["city_id" => $request['city_id'], "lang" => $request["lang"]])
            ], 200);
        } catch (ModelNotFoundException $e) {

            return response()->json([
                'status' => '404',
                "message" => "Empty Record"
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                "message" => "Empty Record"
            ], 404);
        }
    }

    public function adminIndex(Request $request)
    {
        if (RoleChecker::hasRole('customer')) {
            return response()->json([
                'status' => '403',
                "message" => "You have no access"
            ], 403);
        }
        try {
            /**
             * Get all district records from district table .. the records are
             * returned which is equals to status if status is given in parameter
             * else all the records are returned
             *
             */
            $request['lang'] = Input::get('lang', 'en');
            if ($request['type'] == "deleted") {
                $districts = $this->district->districtGetAllForDatatablesOnlyDeleted($request['city_id']);
            } else {
                $districts = $this->district->districtGetAllForDataTable($request['city_id']);
            }


            foreach ($districts as $district) {
                $city = $this->city->cityGetSpecific($district['city_id']);
                if (!$city) continue;
                $countryLanguage = $this->countryLanguage->getSpecificLanguageOfCountryByLanguage($city['country_id'], $request['lang']);
                if ($countryLanguage->count() == 0) {
                    $request['lang'] = "en";
                }
                $districtTranslation = $this->districtTranslation->districtTranslationGetSpecificByCode($request['lang'], $district['id']);
                //return response()->json($districtTranslation);
                if (count($districtTranslation) == 0) continue;
                $district['lang_code'] = $districtTranslation[0]['lang_code'];
                $district['name'] = $districtTranslation[0]['name'];
            }
            return Datatables::of($districts)->make(true);

        } catch (\Exception $e) {
            return response()->json([
                "status" => 404,
                "message" => $e->getMessage()
            ], 404);
        }

    }

    public function publicIndex($city_id)
    {
        /*
         * Validation of Country id
         * */
        try {
            $city = $this->city->cityGetSpecificByStatus($city_id, 1);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Requested City could not be found"
            ], 404);
        }


        /**
         * Display a listing of the district and district translation from district and district_translation table.
         *
         */
        $request['limit'] = Input::get('limit', 999);
        $request['lang'] = Input::get('lang', 'en');

        /* *
         validation of limit param if limit param is not null
     * */
        if (!is_null($request['limit'])) {
            $rules = [
                "limit" => "sometimes|numeric|min:1",
            ];
            $validator = Validator::make($request, $rules);
            if ($validator->fails()) {
                $request['limit'] = 999;
            }
        }

        /**
         * Checking the given laguage exists in language table if not exist default en is assigned
         *
         */
        $language = $this->language->languageGetSpecificByCode($request['lang']);
        if ($language->count() == 0) {
            $request['lang'] = "en";
        }

        try {
            /**
             * Get all district records from district table .. the records are
             * returned which is equals to status if status is given in parameter
             *
             *
             */
            $districts = $this->district->getDistrictFromCityId(1, $city_id, $request['limit']);

            try {
                if (count($districts) == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }

            foreach ($districts as $district) {
                $city = $this->city->cityGetSpecific($district['city_id']);
                $countryLanguage = $this->countryLanguage->getSpecificLanguageOfCountryByLanguage($city['country_id'], $request['lang']);
                if ($countryLanguage->count() == 0) {
                    $request['lang'] = "en";
                }
                $districtTranslation = $this->districtTranslation->districtTranslationGetSpecificByCode($request['lang'], $district['id']);
                //return response()->json($districtTranslation);
                $district['lang_code'] = $districtTranslation[0]['lang_code'];
                $district['name'] = $districtTranslation[0]['name'];
            }
            return response()->json([
                'status' => '200',
                'data' => $districts->appends(["lang" => $request["lang"]])
            ], 200);
        } catch (QueryException $e) {

            return response()->json([
                'status' => '404',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
            ], 404);
        }

    }

    public function cityOfCountry($country_id)
    {
        try {
            $cities = $this->country->countryGetSpecific($country_id)->city;
        } catch (\Exception $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Requested Country could not be found"
            ], 404);
        }
        $request['limit'] = Input::get('limit', 10);
        $request['lang'] = Input::get('lang', 'en');

        /**
         * Checking the given laguage exists in language table if not exist default en is assigned
         *
         */
        $language = $this->language->languageGetSpecificByCode($request['lang']);
        if ($language->count() == 0) {
            $request['lang'] = "en";
        }

        try {
            /**
             * Get all district records from district table .. the records are
             * returned which is equals to status if status is given in parameter
             *
             *
             */
            $districts = [];
            $count = 0;
            foreach ($cities as $city) {
                $district = $this->district->districtGetAllByCityId($city['id'], 1);
                foreach ($district as $data) {
                    $districts[$count] = $data;
                    $count++;
                }
            }

            try {
                if (count($districts) == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }

            foreach ($districts as $district) {
                $city = $this->city->cityGetSpecific($district['city_id']);
                $countryLanguage = $this->countryLanguage->getSpecificLanguageOfCountryByLanguage($city['country_id'], $request['lang']);
                if ($countryLanguage->count() == 0) {
                    $request['lang'] = "en";
                }
                $districtTranslation = $this->districtTranslation->districtTranslationGetSpecificByCode($request['lang'], $district['id']);
                //return response()->json($districtTranslation);
                $district['lang_code'] = $districtTranslation[0]['lang_code'];
                $district['name'] = $districtTranslation[0]['name'];
            }
            return response()->json([
                'status' => '200',
                'data' => $districts
            ], 200);
        } catch (QueryException $e) {

            return response()->json([
                'status' => '404',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
            ], 404);
        }

    }

    /**
     * Display a listing of the resource of district By CityId.
     *
     * @return \Illuminate\Http\Response
     */

    public function getByCity($city_id)
    {
        /**
         * Display a listing of the district  from district
         *
         */
        $request['city_id'] = $city_id;
        /**
         * Validation of city_id
         *
         */

        if ($request['city_id'] != null) {
            $validators = Validator::make($request, [
                'city_id' => 'required|integer'
            ]);
            if ($validators->fails()) {
                return response()->json([
                    'status' => '422',
                    'message' => $validators->errors()
                ], 422);
            }
        }
        try {
            /**
             * Get all district records from district table .. the records are
             * returned which is equals to status if status is given in parameter
             * else all the records are returned
             *
             */
            $districts = $this->district->getDistrictFromCityId($request['city_id'], $request["status"]);
            return response()->json([
                'status' => '200',
                'data' => $districts
            ], 200);
        } catch (QueryException $e) {

            return response()->json([
                'status' => '404',
                'message' => 'district not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'district not found'
            ], 404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        //validation of district field
        try {
            $this->validate($request, [
                'city_id' => 'required|integer',
                'status' => 'integer|min:0|max:1',
                'translation' => 'required|array'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }
        //validation of translation field
        foreach ($request['translation'] as $key => $req) {

        }
        DB::beginTransaction();
        try {
            $request = $request->all();
            $request['status'] = Input::get('status', 1);//1 means active
            try {
                $city = $this->city->cityGetSpecific($request['city_id']);
                if (count($city) == 0) {
                    throw new \Exception();
                }
            } catch (Exception $exception) {
                DB::rollback();
                return response()->json(['status' => '404',
                    'message' => 'city_id not found'], 404);
            }
            $district['city_id'] = $request['city_id'];
            $district['status'] = $request['status'];
            $district = $this->district->districtCreate($district);
            $city = $this->city->cityGetSpecific($district['city_id']);
            /******************* district translation post starts ************/
            foreach ($request['translation'] as $key => $req) {
                $req['district_id'] = $district['id'];
                $req['lang_code'] = $key;
                $validators = Validator::make($req, [
                    'lang_code' => 'required|alpha',
                    'name' => 'required'
                ]);
                if ($validators->fails()) {
                    return response()->json([
                        'status' => '422',
                        'message' => ["translation" => ["$key" => $validators->errors()]]
                    ], 422);
                }
                try {
                    $language = $this->countryLanguage->getAllLanguageOfCountry($city['country_id']);
                    if (count($language) == 0) {
                        throw new \Exception();
                    }
                    $check = $this->checkLanguageCode($language, $request['translation']);
                    if ($check == false) {
                        return response()->json([
                            "status" => "422",
                            "message" => ['translation' => ["Input Translation does not match with specified country language code"]]
                        ], 422);
                    }
                } catch (\Exception $ex) {
                    DB::rollback();
                    return response()->json([
                        'status' => '404',
                        'message' => "Language Not found in country"
                    ], 404);
                }
                $districtTranslation = $this->districtTranslation->districtTranslationCreate($req);

            }
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'District created successfully'
            ], 200);

            /***************** district translation ends ***************/

        } catch (ModelNotFoundException  $ex) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => "city_id not found"
            ], 404);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => '422',
                'message' => "validation error"
            ], 422);
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
//    public function show($id)
//    {
//        $request['lang'] = Input::get('lang', 'en');
//        /**
//         * Checking the given laguage exists in language table if not exist default en is assigned
//         *
//         */
//        $language = $this->language->languageGetSpecificByCode($request['lang']);
//        if ($language->count() == 0) {
//            $request['lang'] = "en";
//        }
//        try {
//            $district = $this->district->districtGetSpecific($id);
//            $city = $this->city->cityGetSpecific($district->city_id);
//            $countryLanguage = $this->countryLanguage->getSpecificLanguageOfCountryByLanguage($city['country_id'], $request['lang']);
//            if ($countryLanguage->count() == 0) {
//                $request['lang'] = "en";
//            }
//            $districtTranslation = $this->districtTranslation->
//            districtTranslationGetSpecificByCode($request['lang'], $district['id']);
//            $district['lang_code'] = $districtTranslation[0]['lang_code'];
//            $district['name'] = $districtTranslation[0]['name'];
//
//            return response()->json([
//                'status' => '200',
//                'data' => $district
//            ], 200);
//        } catch (ModelNotFoundException $e) {
//            return response()->json([
//                'status' => '404',
//                "message" => "Empty Record"
//            ], 404);
//        } catch (\Exception $e) {
//            return response()->json([
//                'status' => '404',
//                'message' => 'error while getting records'
//            ], 404);
//        }
//    }

    public function adminShow($id)
    {
        try {
            $district = $this->district->districtGetSpecific($id);
            $translation = $this->districtTranslation->districtTranslationGetByDistrictId($id);
            $city = $district->city()->first();
            $district['city_translation'] = $city->cityTranslation()->get();
            $country = $city->country()->first();
            $district['country_translation'] = $country->countryTranslation()->get();
            $district['translation'] = $translation;
            return response()->json([
                'status' => '200',
                'data' => $district
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => '404',
                "message" => "Empty Record"
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'error while getting records'
            ], 404);
        }
    }

    public function publicShow($id)
    {
        $request['lang'] = Input::get('lang', 'en');
        /**
         * Checking the given laguage exists in language table if not exist default en is assigned
         *
         */
        $language = $this->language->languageGetSpecificByCode($request['lang']);
        if ($language->count() == 0) {
            $request['lang'] = "en";
        }
        try {
            $district = $this->district->districtGetSpecificByStatus($id, 1);
            $city = $this->city->cityGetSpecificByStatus($district->city_id, 1);
            $countryLanguage = $this->countryLanguage->getSpecificLanguageOfCountryByLanguage($city['country_id'], $request['lang']);
            if ($countryLanguage->count() == 0) {
                $request['lang'] = "en";
            }
            $districtTranslation = $this->districtTranslation->
            districtTranslationGetSpecificByCode($request['lang'], $district['id']);
            $district['lang_code'] = $districtTranslation[0]['lang_code'];
            $district['name'] = $districtTranslation[0]['name'];

            return response()->json([
                'status' => '200',
                'data' => $district
            ], 200);
        } catch (ModelNotFoundException $e) {

            return response()->json([
                'status' => '404',
                "message" => "Empty Record"
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'error while getting records'
            ], 404);
        }
    }

    public function update(Request $request, $id)
    {
        //validation of district field
        try {
            $this->validate($request, [
                'city_id' => 'required|integer',
                'status' => 'integer|min:0|max:1',
                "translation" => "required|array"
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }

        //validation of translation field
        foreach ($request['translation'] as $key => $req) {
            $req['lang'] = $key;

        }
        DB::beginTransaction();
        try {

            $request = $request->all();

            $request['status'] = Input::get('status', 1);//1 means active
            try {
                $city = $this->city->cityGetSpecific($request['city_id']);
                if (count($city) == 0) {
                    throw new \Exception();
                }
            } catch (Exception $exception) {
                return response()->json(['status' => '404',
                    'message' => 'city_id not found'], 404);
                DB::rollback();
            }
            $district['city_id'] = $request['city_id'];
            $district['status'] = $request['status'];
            $district = $this->district->districtUpdate($id, $district);
            $city = $this->city->cityGetSpecific($district['city_id']);
            /******************* district translation update starts ************/
            /******************* deleting all translation *********************/
            $districtTranslations = $this->districtTranslation->districtTranslationGetByDistrictId($id);
            foreach ($districtTranslations as $districtTranslation) {
                $districtTranslation = $this->districtTranslation->districtTranslationDelete($districtTranslation['id']);
            }
            /******************* creating all translation *********************/
            foreach ($request['translation'] as $key => $req) {
                try {
                    $req['district_id'] = $district['id'];
                    $req['lang_code'] = $key;
                    $validators = Validator::make($req, [
                        'lang_code' => 'required|alpha',
                        'name' => 'required'
                    ]);
                    if ($validators->fails()) {
                        return response()->json([
                            'status' => '422',
                            'message' => ["translation" => ["$key" => $validators->errors()]]
                        ], 422);
                    }
                    try {
                        $language = $this->countryLanguage->getAllLanguageOfCountry($city['country_id']);
                        if (count($language) == 0) {
                            throw new \Exception();
                        }
                        $check = $this->checkLanguageCode($language, $request['translation']);
                        if ($check == false) {
                            return response()->json([
                                "status" => "422",
                                "message" => ['translation' => ["Input Translation does not match with specified country language code"]]
                            ], 422);
                        }
                    } catch (\Exception $ex) {
                        DB::rollback();
                        return response()->json([
                            'status' => '404',
                            'message' => "Language Not found in country"
                        ], 404);
                    }
                    $districtTranslation = $this->districtTranslation->districtTranslationUpdateOrCreate($req);
                } catch (\Exception $e) {
                    return response()->json([
                        'status' => '422',
                        'message' => $e->response->original
                    ], 422);
                    DB::rollback();
                }

            }
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'District updated successfully'
            ], 200);

            /***************** district translation ends ***************/
        } catch (ModelNotFoundException  $ex) {
            return response()->json([
                'status' => '404',
                'message' => "city_id not found"
            ], 404);
            DB::rollback();
        } catch (\Exception $e) {
            return response()->json([
                'status' => '422',
                'message' => "validation error"
            ], 422);
            DB::rollback();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $district = $this->district->districtGetSpecificSoftOrHardDeleted($id);
        } catch (\Exception $ex) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => "Requested District could not be found"
            ], 404);
        }

        try {
            if (is_null($district->deleted_at)) {
                try {
                    if ($this->shippingAddress->checkDistrictExist($id))
                        throw new \Exception("Error! Could not delete the district as it is being used in Shipping Address.");
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "403",
                        "message" => $ex->getMessage()
                    ], 403);
                }

                $baseUrl = Config::get("config.restaurant_base_url");
                $fullUrl = $baseUrl . "/restaurant/branch/check";
                $request["district_id"] = $id;
                $districtCheck = RemoteCall::check($fullUrl, $request);
                if ($districtCheck["status"] != 200) {
                    return response()->json([
                        $districtCheck["message"]
                    ], $districtCheck["status"]);
                }
                try {
                    if ($districtCheck["message"]["data"]) {
                        throw new \Exception("Error! Could not delete the district as it is being used in Restaurant Branch.");
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "403",
                        "message" => $ex->getMessage()
                    ], 403);
                }
                $district = $this->district->districtDelete($id);
            } else {
                $district->forceDelete();
            }
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => "District deleted successfully"
            ]);
        } catch (QueryException $ex) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => "Requested District could not be found"
            ], 404);
        } catch (ModelNotFoundException  $ex) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => "Requested District could not be found"
            ], 404);
        } catch (\Exception $ex) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => "Requested District could not be found"
            ], 404);
        }
    }

    public function getNames(Request $request)
    {
        $response = [];
        if (isset($request['country_id'])) {
            try {
                $country = $this->country->countryGetSpecific($request["country_id"]);
                $response['country']['currency_code'] = $country['currency_code'];
                $response['country']['currency_symbol'] = $country['currency_symbol'];
            } catch (ModelNotFoundException $e) {
                return response()->json([
                    'status' => '404',
                    "message" => "Country Not found"
                ], 404);
            }
        }
        if ($request['districts'] == null) {
            return response()->json([
                'status' => '200',
                'data' => $response
            ], 200);
        }
        try {
            $districts = $this->district->districtIdToNames($request['districts']);
        } catch (ModelNotFoundException $e) {

            return response()->json([
                'status' => '404',
                "message" => "District could not be found"
            ], 404);
        }


        try {

            foreach ($districts as $district) {
                $districtTranslation = $this->districtTranslation->
                districtTranslationGetSpecificByCode($request['lang'], $district['district_id']);
                $district['lang_code'] = $districtTranslation[0]['lang_code'];
                $district['name'] = $districtTranslation[0]['name'];
            }
            $response['districts'] = $districts;


            return response()->json([
                'status' => '200',
                'data' => $response
            ], 200);
        } catch (ModelNotFoundException $e) {

            return response()->json([
                'status' => '404',
                "message" => "Empty Record"
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'error while getting records'
            ], 404);
        }
    }

    public function getCurrencyCodeByCountryId(Request $request)
    {
        if (!isset($request['country_id'])) {
            return response()->json([
                'status' => '200',
                "data" => []
            ], 200);
        }

        if (isset($request['country_id'])) {
            try {
                $country = $this->country->countryGetSpecific($request['country_id']);
                $data['currency_code'] = $country['currency_code'];
                $data['currency_symbol'] = $country['currency_symbol'];
            } catch (ModelNotFoundException $e) {

            }
            $response['country'] = $data;
        }
        return response()->json([
            'status' => '200',
            'data' => $response
        ], 200);

    }

    public function getLocationNames(Request $request)
    {
        $response = [];
        if (!isset($request['country_id'])) {
            $response['country'] = '';
        }
        if (!isset($request['city_id'])) {
            $response['city'] = '';
        }
        if (!isset($request['district_id'])) {
            $response['district'] = '';
        }

        if (isset($request['country_id'])) {
            $country = $this->country->countryGetSpecific($request['country_id']);
            $response["currency"] = $country["currency_symbol"];
              $country = $country->countryTranslation()->get();

            foreach ($country as $data) {
                if ($data['lang'] == "en") {
                    $response['country'] = $data['name'];
                    break;
                }
            }
        }
        if (isset($request['city_id'])) {
            $city = $this->city->cityGetSpecific($request['city_id'])->cityTranslation()->get();
            foreach ($city as $data) {
                if ($data['lang_code'] == "en") {
                    $response['city'] = $data['name'];
                    break;
                }
            }
        }
        if (isset($request['district_id'])) {
            $country = $this->district->districtGetSpecific($request['district_id'])->districtTranslation()->get();
            foreach ($country as $data) {
                if ($data['lang_code'] == "en") {
                    $response['district'] = $data['name'];
                    break;
                }
            }
        }
        return response()->json([
            'status' => '200',
            'data' => $response
        ], 200);

    }

    public function checkLanguageCode($language, $languageTranslations)
    {
        foreach ($language as $lang) {
            $languages[] = $lang['code'];
        }
        $count = count($languages);
        foreach ($languageTranslations as $key => $translation) {
            $langlist[] = $key;
        }
        $countLanguage = count($langlist);
        if ($count != $countLanguage) {
            return false;
        }
        $countIntersect = count(array_intersect($languages, $langlist));
        if ($countLanguage !== $countIntersect) {
            return false;
        }
        return true;
    }

    /**
     * Fetch, validate the location using coordinates
     * @return array
     */
    public function getLocationDetailFromGeoLocation(Request $request)
    {
        try {
            $this->validate($request, [
                'lng' => 'required|numeric',
                'lat' => 'required|numeric',
                'lang'=> 'sometimes'
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        $lat = $request['lat'];
        $lng = $request['lng'];
        $request['lang'] = Input::get('lang','en');
         $response = \GeoLocationChecker::fetch($lng, $lat, $request['lang']);
//         return $response;
            if($response['status'] == 200){
                $countryId = $response["payload"]["country"]["country_id"];
                 $countryDetails = $this->country->getCountrySpecificByStatus($countryId);
                $response["payload"]["country"]["calling_code"] = $countryDetails["calling_code"];
            }

        return response()->json($response,$response['status']);
    }

    public function getGeoLocationFromLocationDetail(Request $request)
    {

        try {
            $this->validate($request, [
                'location' => 'required'
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        $locationName = $request['location'];
        $response = \GeoLocationChecker::fetchCoordinate($locationName);
        return response()->json($response,$response['status']);
    }
}