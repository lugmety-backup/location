<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 3:19 PM
 */

namespace App\Http\Controllers;

use App\Repo\DistrictInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use App\Repo\DistrictTranslationInterface;
use Illuminate\Http\Request;

class DistrictTranslationController extends Controller
{
    protected $districtTranslation;
    protected $district;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(DistrictTranslationInterface $districtTranslation,DistrictInterface $district)
    {
        $this->districtTranslation=$districtTranslation;
        $this->district=$district;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $district= $this->districtTranslation->districtTranslationGetAll();
            try{
                if(!$district->first()){
                    throw new \Exception();
                }
            }
            catch (\Exception $ex){
                return response()->json([
                    'status'=>'200',
                    "message"=>'Empty Record'
                ],200);
            }
            return response()->json([
                'status'=>'200',
                'district'=>$district
            ],200);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'404',
                'message'=>'error while getting records'
            ],404);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'name' => 'required|alpha',
                'district_id' => 'required|integer',
                'lang_code' => 'required|alpha-dash',
            ]);
            $request=$request->all();
            try {
                $this->district->districtGetSpecific($request['district_id']) ;

            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'404',
                    'message'=>"District  could not be found"
                ],404);
            }
            $district=$this->districtTranslation->districtTranslationGetSpecificByName($request['name']);
            try {
                if ($district->count() != 0) {

                    throw new \Exception();
                }
            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'409',
                    'message'=>"District Translation could not be created due to duplicate entry"
                ],409);
            }
            $district=$this->districtTranslation->districtTranslationCreate($request);
            return response()->json([
                'status'=>'200',
                'message'=>'District created successfully',
                'Country Translation'=>$district
            ],200);

        }

        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $district= $this->districtTranslation->districtTranslationGetSpecific($id);
            return response()->json([
                'status'=>'200',
                'countryTranslation'=>$district
            ],200);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested District Translation could not be found"
            ],404);
        }
        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested District Translation could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested District Translation could not be found"
            ],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'name' => 'required|alpha',
                'district_id' => 'required|integer',
                'lang_code' => 'required|alpha-dash',
            ]);
            $request = $request->all();
            $district=$this->districtTranslation->districtTranslationGetSpecificByName($request['name']);
            try {
                if ($district->count() != 0) {

                    throw new \Exception();
                }
            }catch (\Exception $ex){
                return response()->json([
                    'status'=>'409',
                    'message'=>"District Translation could not be created due to duplicate entry"
                ],409);
            }

            $district = $this->districtTranslation->districtTranslationUpdate($id, $request);
            return response()->json([
                'status' => '200',
                'message' => 'District Translation Updated successfully',
                'districtTranslation' => $district
            ]);

        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'409',
                'message'=>"District could not be created due to duplicate entry"
            ],409);
        }

        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested District could not be found"
            ],404);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $district= $this->districtTranslation->districtTranslationDelete($id);
            return response()->json([
                'status'=>'200',
                'districtTranslation'=>"District Translation deleted successfully"
            ]);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested District Translation could not be found"
            ],404);
        }
        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested District Translation could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested District Translation could not be found"
            ],404);
        }
    }

}