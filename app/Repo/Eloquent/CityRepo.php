<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/13/2017
 * Time: 10:51 AM
 */

namespace App\Repo\Eloquent;


use App\City;
use App\Repo\CityInterface;

class CityRepo implements CityInterface
{
    protected $city;

    /**
     * CityRepo constructor.
     * @param $city
     */
    public function __construct(City $city)
    {
        $this->city = $city;
    }

    public function cityGetAll($status)
    {
        if(is_null($status)){
            return $this->city->all();
        }
        return $this->city->where('status',$status)->get();
    }
    public function cityGetAllPagination($status,$country_id,$limit)
    {
        if(is_null($status) && is_null($country_id)){
            $city = $this->city->paginate($limit);
        }
        else if(is_null($country_id)){
            $city = $this->city->where('status',$status)->paginate($limit);
        }
        else if(is_null($status)){
            $city = $this->city->where('country_id',$country_id)->paginate($limit);
        }else
        {
            $city  = $this->city->where([['status',$status],['country_id',$country_id]])->paginate($limit);
        }
        $city->appends(["limit" => $limit])->withPath("/city");
        return $city;
    }
    public function cityGetAllByCountry($status, $country_id,$limit)
    {
        if(is_null($status)){
            $city = $this->city->where('country_id',$country_id)->paginate($limit);
        }else
        {
            $city  = $this->city->where([['status',$status],['country_id',$country_id]])->paginate($limit);
        }
        $city->appends(["limit" => $limit])->withPath("/country/$country_id/city");
        return $city;
    }

    public function cityGetAllByCountryForDataTable($country_id)
    {
        if($country_id == null){
            $city = $this->city->all();
        }else{
            $city = $this->city->where("country_id",$country_id)->get();
        }

        return $city;
    }

    public function cityGetAllForDatatablesOnlyDeleted($country_id){
        if($country_id == null){
            $city = $this->city->onlyTrashed()->get();
        }else{
            $city = $this->city->onlyTrashed()->where("country_id",$country_id)->get();
        }

        return $city;
    }


    public function cityGetSpecific($id)
    {
        return $this->city->findOrfail($id);
    }

    public function cityGetSpecificByStatus($id,$status)
    {
        return $this->city->where([

            ["status" , $status]
        ])->findOrFail($id);
    }

    public function cityGetSpecificTax($id){ //added to get tax_percentage by ashish
        return $this->city->select("id","country_id","tax_percentage")->where([["status",1],["id",$id]])->firstOrFail();
    }

    public function cityGetSpecificByCode($code)
    {
        // TODO: Implement cityGetSpecificByCode() method.
    }

    public function cityCreate(array $attributes)
    {
        return $this->city->create($attributes);
    }

    public function cityUpdate($id, array $attributes)
    {
        $con=$this->city->findOrFail($id)->update($attributes);
        $con=$this->city->findOrFail($id)->fill($attributes);
        return $con;
    }

    public function cityDelete($id)
    {
        return $this->city->findOrFail($id)->delete();
    }

    public function cityGetSpecificSoftOrHardDeleted($id){
        return $this->city->withTrashed()->findOrFail($id);
    }

}