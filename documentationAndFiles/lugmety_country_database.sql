-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2017 at 07:16 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lugmety_country_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `billing_address`
--

CREATE TABLE `billing_address` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `address_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postal_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address_line_one` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_line_two` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `district` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `primary` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `billing_address`
--

INSERT INTO `billing_address` (`id`, `user_id`, `address_name`, `first_name`, `middle_name`, `email`, `postal_code`, `last_name`, `address_line_one`, `address_line_two`, `district`, `city`, `country`, `primary`, `created_at`, `updated_at`) VALUES
(1, 1, 'abc', 'abc', NULL, 'test@gmail.com', '44800', 'abc', 'test', NULL, 'Kathmandu', 'Bagmati', 'Nepal', 1, '2017-04-27 10:59:40', '2017-04-27 10:59:40');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `timezone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tax_percentage` double(5,3) NOT NULL,
  `status` smallint(6) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `country_id`, `timezone`, `tax_percentage`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 'timezone', 13.330, 1, NULL, NULL, NULL),
(15, 2, 'GMT-5:45', 13.330, 1, '2017-04-21 10:08:32', '2017-04-21 09:41:31', '2017-04-21 10:08:32'),
(16, 2, 'GMT-5:45', 13.330, 1, NULL, '2017-04-21 09:57:00', '2017-04-21 09:57:00'),
(17, 2, 'GMT-5:45', 13.330, 1, NULL, '2017-04-21 09:57:33', '2017-04-21 09:57:33'),
(61, 2, 'GMT-5:45', 13.330, 1, NULL, '2017-04-23 07:08:10', '2017-04-23 07:08:10'),
(62, 2, 'GMT-5:45', 13.330, 1, NULL, '2017-04-23 07:22:23', '2017-04-23 07:22:23'),
(103, 2, 'GMT-5:45', 13.330, 1, NULL, '2017-04-23 07:49:23', '2017-04-23 07:49:23'),
(104, 2, 'GMT-5:45', 13.330, 1, NULL, '2017-04-23 07:49:32', '2017-04-23 07:49:32'),
(105, 2, 'GMT-5:45', 13.330, 1, NULL, '2017-04-23 07:50:42', '2017-04-23 07:50:42'),
(115, 2, 'GMT-5:45', 13.330, 1, NULL, '2017-04-23 07:54:43', '2017-04-23 07:54:43'),
(116, 2, 'GMT-5:45', 13.330, 1, NULL, '2017-04-23 07:55:32', '2017-04-23 07:55:32'),
(745, 2, 'GMT-5:45', 13.330, 1, NULL, '2017-04-24 09:41:06', '2017-04-24 09:41:06'),
(746, 2, 'GMT-5:45', 13.330, 1, NULL, '2017-04-24 09:43:16', '2017-04-24 09:43:16'),
(747, 2, 'GMT-5:45', 13.330, 1, NULL, '2017-04-24 09:48:32', '2017-04-24 09:48:32'),
(883, 2, 'GMT-5:45', 13.330, 1, NULL, '2017-04-25 07:38:19', '2017-04-25 07:38:19');

-- --------------------------------------------------------

--
-- Table structure for table `city_translation`
--

CREATE TABLE `city_translation` (
  `id` int(10) UNSIGNED NOT NULL,
  `city_id` int(10) UNSIGNED NOT NULL,
  `lang_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `city_translation`
--

INSERT INTO `city_translation` (`id`, `city_id`, `lang_code`, `name`) VALUES
(1, 1, 'en', 'Bagmati'),
(2, 1, 'ar', 'باغماتي'),
(4, 16, 'en', 'NarayaniRevised'),
(5, 17, 'en', 'NarayaniRevised'),
(30, 61, 'en', 'Nepal'),
(31, 62, 'en', 'Nepal'),
(67, 103, 'en', 'Nepal'),
(68, 104, 'en', 'Nepall'),
(69, 105, 'en', 'Nepall'),
(77, 115, 'en', 'Nepal'),
(78, 115, 'jp', 'ネパール'),
(79, 116, 'en', 'Nepal'),
(80, 116, 'jp', 'ネパール'),
(428, 745, 'en', 'Nepal'),
(429, 745, 'jp', 'Nepal'),
(430, 746, 'en', 'Nepal'),
(431, 746, 'jp', 'Nepal'),
(438, 747, 'en', 'Nepal'),
(516, 883, 'en', 'Nepal');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `currency_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `currency_symbol` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `calling_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tax_percentage` double(5,3) NOT NULL,
  `status` smallint(6) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `week_day_start_at` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `code`, `currency_code`, `currency_symbol`, `calling_code`, `timezone`, `tax_percentage`, `status`, `deleted_at`, `week_day_start_at`, `created_at`, `updated_at`) VALUES
(2, 'USs', 'dollar', '$', '+1', 'UTC -5:30', 13.330, 1, NULL, 'sunday', '2017-04-11 10:16:46', '2017-04-11 10:16:46'),
(3, 'hshshshsssss', 'dollar', '$', '+1', 'UTC -5:30', 0.000, 1, NULL, 'sunday', '2017-04-11 10:30:26', '2017-04-11 10:30:26'),
(904, 'us', 'us', '$', '14', 'GMT-5:45', 13.333, 0, '2017-04-24 06:30:45', 'sunday', '2017-04-24 05:22:32', '2017-04-24 06:30:45'),
(906, 'us', 'us', '$', '14', 'GMT-5:45', 13.333, 1, NULL, 'sunday', '2017-04-24 06:31:04', '2017-04-24 06:31:04'),
(939, 'np', 'np', 'Rs', '977', 'GMT-5:45', 13.333, 1, NULL, 'sunday', '2017-04-24 07:50:24', '2017-04-24 07:50:24'),
(940, 'in', 'in', 'Rs', '91', 'GMT-6:45', 13.333, 0, NULL, 'monday', '2017-04-24 08:15:20', '2017-04-24 10:20:29'),
(972, 'ipn', 'in', 'Rs', '91', 'GMT-6:45', 13.333, 1, NULL, 'monday', '2017-04-24 11:43:16', '2017-04-24 11:43:16'),
(973, 'ipdn', 'in', 'Rs', '91', 'GMT-6:45', 13.333, 1, NULL, 'monday', '2017-04-24 11:44:56', '2017-04-24 11:44:56'),
(1007, 'ipdsfsddsaaan', 'in', 'Rs', '91', 'GMT-6:45', 13.333, 1, NULL, 'monday', '2017-04-25 06:02:17', '2017-04-25 06:02:17'),
(1008, 'uasdfghjks', 'Dollar', '$', '+12', 'UTC -5:30', 13.330, 1, NULL, 'sunday', '2017-04-25 06:20:00', '2017-04-25 06:20:00'),
(2251, 'llus', 'Dollar', '$', '+12', 'UTC -5:30', 13.330, 1, NULL, 'sunday', '2017-04-25 10:58:04', '2017-04-25 10:58:04');

-- --------------------------------------------------------

--
-- Table structure for table `country_language`
--

CREATE TABLE `country_language` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `country_language`
--

INSERT INTO `country_language` (`id`, `country_id`, `language_id`) VALUES
(1, 2, 1),
(20, 2, 2),
(22, 3, 1),
(23, 3, 1),
(24, 3, 2),
(25, 3, 3),
(26, 3, 4),
(33, 939, 1),
(51, 940, 1),
(52, 940, 2),
(55, 972, 1),
(56, 972, 2),
(57, 973, 1),
(58, 973, 2),
(125, 1007, 1),
(126, 1007, 2),
(127, 1008, 1),
(128, 1008, 2),
(1965, 2251, 1),
(1966, 2251, 2);

-- --------------------------------------------------------

--
-- Table structure for table `country_meta`
--

CREATE TABLE `country_meta` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `country_meta`
--

INSERT INTO `country_meta` (`id`, `country_id`, `meta_key`, `meta_value`) VALUES
(1, 2, 'test', 'test'),
(2, 2, 'test', 'test'),
(3, 2, 'test', 'test'),
(4, 2, 'test', 'test'),
(5, 2, 'test', 'test'),
(6, 2, 'test', 'test'),
(7, 2, 'testtest', 'test'),
(9, 906, 'countrymeta', 'countrymeta'),
(11, 939, 'countrymeta', 'countrymeta'),
(17, 940, 'countrymeta', 'countrymeta'),
(18, 1007, 'test', 'countrymeta'),
(19, 1008, 'house no', '222'),
(107, 2251, 'house no', '222');

-- --------------------------------------------------------

--
-- Table structure for table `country_translation`
--

CREATE TABLE `country_translation` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `country_translation`
--

INSERT INTO `country_translation` (`id`, `country_id`, `lang`, `name`) VALUES
(4, 906, 'en', 'Nepal'),
(5, 906, 'jp', 'jlajlkfdas'),
(6, 939, 'en', 'Nepal'),
(14, 940, 'en', 'India'),
(15, 972, 'en', 'India'),
(16, 973, 'en', 'India'),
(17, 1007, 'en', 'India'),
(18, 1008, 'en', 'Nepal'),
(93, 2251, 'en', 'eng');

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `id` int(10) UNSIGNED NOT NULL,
  `city_id` int(10) UNSIGNED NOT NULL,
  `status` smallint(6) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `city_id`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(169, 1, 1, '2017-04-22 18:15:00', '2017-04-18 12:17:08', '2017-04-19 07:10:03'),
(170, 1, 1, NULL, '2017-04-19 05:20:42', '2017-04-19 05:20:42'),
(171, 1, 1, NULL, '2017-04-19 05:20:49', '2017-04-19 05:20:49'),
(172, 1, 1, NULL, '2017-04-19 06:23:13', '2017-04-19 06:23:13'),
(173, 1, 0, NULL, '2017-04-19 06:23:16', '2017-04-19 06:23:16');

-- --------------------------------------------------------

--
-- Table structure for table `district_translation`
--

CREATE TABLE `district_translation` (
  `id` int(10) UNSIGNED NOT NULL,
  `district_id` int(10) UNSIGNED NOT NULL,
  `lang_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `district_translation`
--

INSERT INTO `district_translation` (`id`, `district_id`, `lang_code`, `name`) VALUES
(26, 170, 'en', 'Nepal'),
(27, 170, 'ars', 'نيبال'),
(28, 171, 'en', 'Nepal'),
(29, 171, 'ar', 'نيبال'),
(37, 173, 'en', 'Nepal'),
(38, 173, 'ar', 'نيبال'),
(39, 173, 'jp', 'ネパール'),
(116, 172, 'en', 'Nepal');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`, `code`, `status`, `created_at`, `updated_at`) VALUES
(1, 'english', 'en', 1, '2017-04-11 18:15:00', '2017-04-11 18:15:00'),
(2, 'japanese', 'ar', 1, NULL, NULL),
(3, 'chinese', 'pppp', 1, NULL, NULL),
(4, 'arabic', 'ar', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(13, '2017_04_09_052052_create_language_table', 1),
(14, '2017_04_09_120038_create_country_table', 1),
(15, '2017_04_09_120326_create_country_translation_table', 1),
(16, '2017_04_09_121740_create_country_language_table', 1),
(17, '2017_04_10_053238_create_country_meta_table', 1),
(18, '2017_04_10_060205_create_city_table', 1),
(19, '2017_04_10_064404_create_city_translation_table', 1),
(20, '2017_04_10_064854_create_district_table', 1),
(21, '2017_04_10_064959_create_district_translation_table', 1),
(22, '2017_04_10_073634_create_shipping_address_table', 1),
(23, '2017_04_10_080416_create_shipping_address_meta_table', 1),
(24, '2017_04_10_082214_create_billing_address_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_address`
--

CREATE TABLE `shipping_address` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `address_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google_coordinate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `district_id` int(10) UNSIGNED NOT NULL,
  `extra_direction` text COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('house','building') COLLATE utf8_unicode_ci NOT NULL,
  `primary` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipping_address`
--

INSERT INTO `shipping_address` (`id`, `user_id`, `address_name`, `street`, `google_coordinate`, `district_id`, `extra_direction`, `type`, `primary`, `created_at`, `updated_at`) VALUES
(18, 1, 'Kathmandu', 'DurbarMarg', '(88.888,99.999)', 171, 'east west north south', 'house', 1, '2017-04-20 11:07:02', '2017-04-20 11:07:02'),
(32, 1, 'Kathmandu', 'DurbarMarg', '(88.888,99.999)', 171, 'east west north south', 'house', 1, '2017-04-20 11:58:35', '2017-04-20 11:58:35'),
(33, 1, 'Thimida', 'Thimi', '(73.123,87,334)', 169, 'Go right then left then where ever you want.', 'house', 1, '2017-04-21 07:00:29', '2017-04-21 07:00:29');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_address_meta`
--

CREATE TABLE `shipping_address_meta` (
  `id` int(10) UNSIGNED NOT NULL,
  `shipping_address_id` int(10) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipping_address_meta`
--

INSERT INTO `shipping_address_meta` (`id`, `shipping_address_id`, `meta_key`, `meta_value`, `created_at`, `updated_at`) VALUES
(4, 32, 'ghar number', 'thaha xaina malai', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `billing_address`
--
ALTER TABLE `billing_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_country_id_foreign` (`country_id`);

--
-- Indexes for table `city_translation`
--
ALTER TABLE `city_translation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_translation_city_id_foreign` (`city_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country_language`
--
ALTER TABLE `country_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_language_country_id_foreign` (`country_id`),
  ADD KEY `country_language_language_id_foreign` (`language_id`);

--
-- Indexes for table `country_meta`
--
ALTER TABLE `country_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_meta_country_id_foreign` (`country_id`);

--
-- Indexes for table `country_translation`
--
ALTER TABLE `country_translation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_translation_country_id_foreign` (`country_id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`),
  ADD KEY `district_city_id_foreign` (`city_id`);

--
-- Indexes for table `district_translation`
--
ALTER TABLE `district_translation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `district_translation_district_id_foreign` (`district_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_address`
--
ALTER TABLE `shipping_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shipping_address_district_id_foreign` (`district_id`);

--
-- Indexes for table `shipping_address_meta`
--
ALTER TABLE `shipping_address_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shipping_address_meta_shipping_address_id_foreign` (`shipping_address_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `billing_address`
--
ALTER TABLE `billing_address`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=884;
--
-- AUTO_INCREMENT for table `city_translation`
--
ALTER TABLE `city_translation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=517;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2252;
--
-- AUTO_INCREMENT for table `country_language`
--
ALTER TABLE `country_language`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1967;
--
-- AUTO_INCREMENT for table `country_meta`
--
ALTER TABLE `country_meta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT for table `country_translation`
--
ALTER TABLE `country_translation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;
--
-- AUTO_INCREMENT for table `district_translation`
--
ALTER TABLE `district_translation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `shipping_address`
--
ALTER TABLE `shipping_address`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `shipping_address_meta`
--
ALTER TABLE `shipping_address_meta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`);

--
-- Constraints for table `city_translation`
--
ALTER TABLE `city_translation`
  ADD CONSTRAINT `city_translation_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`);

--
-- Constraints for table `country_language`
--
ALTER TABLE `country_language`
  ADD CONSTRAINT `country_language_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`),
  ADD CONSTRAINT `country_language_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`);

--
-- Constraints for table `country_meta`
--
ALTER TABLE `country_meta`
  ADD CONSTRAINT `country_meta_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`);

--
-- Constraints for table `country_translation`
--
ALTER TABLE `country_translation`
  ADD CONSTRAINT `country_translation_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`);

--
-- Constraints for table `district`
--
ALTER TABLE `district`
  ADD CONSTRAINT `district_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`);

--
-- Constraints for table `district_translation`
--
ALTER TABLE `district_translation`
  ADD CONSTRAINT `district_translation_district_id_foreign` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`);

--
-- Constraints for table `shipping_address`
--
ALTER TABLE `shipping_address`
  ADD CONSTRAINT `shipping_address_district_id_foreign` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`);

--
-- Constraints for table `shipping_address_meta`
--
ALTER TABLE `shipping_address_meta`
  ADD CONSTRAINT `shipping_address_meta_shipping_address_id_foreign` FOREIGN KEY (`shipping_address_id`) REFERENCES `shipping_address` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
