<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 5:18 PM
 */

namespace App\Repo\Eloquent;


use App\Repo\ShippingAddressMetaInterface;
use App\ShippingAddressMeta;

class ShippingAddressMetaRepo implements ShippingAddressMetaInterface
{
    private $shippingAddressMeta;

    /**
     * CountryRepo constructor.
     * @param $countryMeta
     */
    public function __construct(ShippingAddressMeta $shippingAddressMeta)
    {
        $this->shippingAddressMeta = $shippingAddressMeta;
    }

    public function shippingAddressMetaGetAll()//get all the shippingAddressMeta information from database
    {
        // TODO: Implement countryMetaGetAll() method.

        return $this->shippingAddressMeta->all();
    }


    public function shippingAddressMetaGetAllByShippingAddressId($id)
    {
        return $this->shippingAddressMeta->where('shipping_address_id',$id)->get();
    }

    public function shippingAddressMetaGetSpecific($id)
    {
        return $this->shippingAddressMeta->findOrFail($id);
    }

    public function shippingAddressMetaUpdateOrCreate(array $attributes)
    {
        $shippingMeta=$this->shippingAddressMeta->where([
            ['shipping_address_id',$attributes['shipping_address_id']]
            , ['meta_key',$attributes['meta_key']]
        ])->get();
        if($shippingMeta->count()!=0){
            $con=$this->shippingAddressMeta->where([
                ['shipping_address_id',$attributes['shipping_address_id']]
                , ['meta_key',$attributes['meta_key']]
            ])->update($attributes);
        }
        else{
            $con=$this->shippingAddressMeta->create($attributes);
        }
        return $con;
    }

    public function shippingAddressMetaGetSpecificByKey($key, $id)
    {
        return $this->shippingAddressMeta->where([['shipping_address_id',$id],['meta_key',$key]])->get();
    }

    public function shippingAddressMetaCreate(array $attributes)
    {
        return $this->shippingAddressMeta->create($attributes) ;
    }

    public function shippingAddressMetaUpdate($id, array $attributes)
    {
        $con=$this->shippingAddressMeta->findOrFail($id)->update($attributes);
        $con=$this->shippingAddressMeta->findOrFail($id)->fill($attributes);
        return $con;
    }

    public function shippingAddressMetaDelete($id)
    {
        $data=$this->shippingAddressMeta->findOrFail($id)->delete();
        return true ;

    }
}