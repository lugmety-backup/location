<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/20/2017
 * Time: 12:38 PM
 */
namespace App\Http\Controllers;
use App\Repo\CityInterface;
use App\Repo\CountryInterface;
use App\Repo\ShippingAddressInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use RoleChecker;
use Illuminate\Support\Facades\Config;
/**
 * Class TaxAndDeliveryChargeController
 * @package App\Http\Controllers
 */
class TaxAndDeliveryChargeController extends Controller
{
    /**
     * @var CityInterface
     */
    protected $city;
    /**
     * @var CountryInterface
     */
    protected $country;
    /**
     * @var ShippingAddressInterface
     */
    protected $shipping_address;
    /**
     * TaxAndDeliveryChargeController constructor.
     * @param CityInterface $city
     * @param CountryInterface $country
     * @param ShippingAddressInterface $shipping_address
     */
    public function __construct(CityInterface $city, CountryInterface $country, ShippingAddressInterface $shipping_address)
    {
        $this->city = $city;
        $this->country = $country;
        $this->shipping_address = $shipping_address;
    }
    /**
     * returns tax_percenatage and currency_symbol and delivery charge
     * @param $country_id
     * @param $city_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTaxRelatedInfo($country_id, $city_id, Request $request)
    {
        try{
            $userId = RoleChecker::getUser();
            $this->validate($request,[
                "address_id" => "sometimes|integer|min:1",
                "delivery_zone" => "required_with:address_id|array"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        $flag = false;
        $taxAndOtherInfo["address_name"] ="";
        if($request->has("address_id")) {
            try {
                $shippingId = $request["address_id"];
                $shippingAddress = $this->shipping_address->getShippingAddressFromUserIdAndId($shippingId, $userId);
                /*
                 * checking delivery zone belongs to shipping address or not for user
                 */
                foreach ($request["delivery_zone"] as $deliveryZone) {
                    if ($shippingAddress["district_id"] == $deliveryZone["district_id"]) {
                        $flag = true;
                        break;
                    }
                }
                if ($flag == false) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Your delivery address is outside restaurant delivery zone."
                    ], 404);
                }

                $taxAndOtherInfo["address_name"] = $shippingAddress->street;
                $taxAndOtherInfo["street"] = $shippingAddress->street;
                $taxAndOtherInfo["extra_direction"] = $shippingAddress->extra_direction;
                $taxAndOtherInfo["shipping_district_id"] = $shippingAddress["district_id"];
                $taxAndOtherInfo["shipping_details"] = $shippingAddress;
                $taxAndOtherInfo["latitude"] = $shippingAddress["latitude"];
                $taxAndOtherInfo["longitude"] = $shippingAddress["longitude"];

            } catch (ModelNotFoundException $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Shipping address not found"
                ], 404);
            }
        }
        try {
            $city = $this->city->cityGetSpecificTax($city_id);
            $cityName = $city->cityTranslation()->where("lang_code",'en')->first()->name;
            $country = $city->country()->where("status",1)->first();
            $countryName = $country->countryTranslation()->where("lang","en")->first()->name;
            $taxAndOtherInfo["address_name"] .=",".ucfirst($cityName).",".ucfirst($countryName);
            if(!$country){
                throw new \Exception();
            }
            /**
             * calling order srvice to get rules
             */
            $countryId = $country["id"];
            $timezone = $country["timezone"];
            $earningRuleUrl = Config::get("config.order_base_url") . "/earning/$countryId/rule?timezone=$timezone";

            $earningRuleDetail = \RemoteCall::getSpecificWithoutToken($earningRuleUrl);
            if ($earningRuleDetail['status'] != 200) {
                if ($earningRuleDetail['status'] == 503) {
                    return response()->json(
                        $earningRuleDetail, $earningRuleDetail['status']);
                }
                return response()->json(
                    $earningRuleDetail["message"], $earningRuleDetail['status']);
            }
            $earningDetail = $earningRuleDetail["message"]["data"];
            $taxAndOtherInfo["min_cost"] = $earningDetail["min_earning"];
            $taxAndOtherInfo["per_cost"] = $earningDetail["earning_per_km"];
            $taxAndOtherInfo["min_distance"] = $earningDetail["min_distance"];

//            $taxAndOtherInfo["min_distance"] = $country->min_distance;
//            $taxAndOtherInfo["min_cost"] = $country->min_cost;
//            $taxAndOtherInfo["per_cost"] = $country->per_cost;
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Country Could not be found"
            ], 404);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Country Could not be found"
            ], 404);
        }
        if ($city->count() == 0) {
            $taxAndOtherInfo["tax_percentage"] = $country["tax_percentage"];
        } else {
            $taxAndOtherInfo["tax_percentage"] = $city["tax_percentage"];
        }
        $taxAndOtherInfo["currency"] = $country["currency_symbol"];
        $taxAndOtherInfo["currency_code"] = $country["currency_code"];
        $taxAndOtherInfo["country_code"] = $country["code"];
        $taxAndOtherInfo["calling_code"] = $country["calling_code"];
        $taxAndOtherInfo["code"] = strtoupper($country["code"]);
        return response()->json([
            "status" => "200",
            "data" => $taxAndOtherInfo
        ], 200);
    }

    public function getShippingInfo($userId,$id, Request $request){
        try{
            if($request->has("country_id")){
                $countryInfo = $this->country->countryGetSpecific($request->country_id);
                $countryInfo["translation"] = $countryInfo->countryTranslation()->where("lang","en")->first();
                return response()->json([
                    "status" => "200",
                    "data" => $countryInfo
                ]);
            }
            $shippingAddress = $this->shipping_address->getShippingAddressFromUserIdAndId($id, $userId);
            $shippingAddress["district_name"] = $shippingAddress->district->districtTranslation()->where("lang_code","en")->first();
            $shippingAddress["city_name"] = $shippingAddress->district->city->cityTranslation()->where("lang_code","en")->first();
            $countryInfo = $shippingAddress->district->city->country()->first();
            $translation = $countryInfo->countryTranslation()->where("lang","en")->first();
            $shippingAddress = $shippingAddress->toArray();
            $shippingAddress["country"] = $countryInfo;
            $shippingAddress["translation"] = $translation;
            return response()->json([
                "status" => "200",
                "data" => $shippingAddress
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "404",
                "message" => "Shipping address not found"
            ],404);
        }
    }

    public function calculateShippingCost(Request $request){
        try{
            $this->validate($request,[
                "shipping_id" => "required|integer|min:1",
                "restaurant_id" => "required|integer|min:1"
            ]);
        }

        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ]);
        }
        try {
            $shippingAddress = $this->shipping_address->shippingAddressGetSpecific($request->shipping_id);
            $countryInfo = $shippingAddress->district->city->country;
            $city = $shippingAddress->district->city;
            if ($city->count() == 0) {
                $tax = $countryInfo->tax_percentage;
            } else {
                $tax = $city->tax_percentage;
            }
            /**
             * calling restaurant service
             */
            $restaurantFullUrl = Config::get("config.restaurant_base_url") . "/public/restaurant/branch/$request->restaurant_id/detail";

            $restaurantdata = \RemoteCall::getSpecificWithoutToken($restaurantFullUrl);
            if ($restaurantdata['status'] != 200) {
                if ($restaurantdata['status'] == 503) {
                    return response()->json(
                        $restaurantdata, $restaurantdata['status']);
                }
                return response()->json(
                    $restaurantdata["message"], $restaurantdata['status']);
            }
            $restaurantDetails = $restaurantdata["message"]["data"];
            $deliveryZoneCheckFlag = false;
            /**
             * calling order srvice to get rules
             */
            $countryId = $countryInfo["id"];
            $timezone = $countryInfo["timezone"];
            $earningRuleUrl = Config::get("config.order_base_url") . "/earning/$countryId/rule?timezone=$timezone";

            $earningRuleDetail = \RemoteCall::getSpecificWithoutToken($earningRuleUrl);
            if ($earningRuleDetail['status'] != 200) {
                if ($earningRuleDetail['status'] == 503) {
                    return response()->json(
                        $earningRuleDetail, $earningRuleDetail['status']);
                }
                return response()->json(
                    $earningRuleDetail["message"], $earningRuleDetail['status']);
            }
            $earningDetail = $earningRuleDetail["message"]["data"];
            $taxAndOtherInfo["min_cost"] = $earningDetail["min_earning"];
            $taxAndOtherInfo["per_cost"] = $earningDetail["earning_per_km"];
            $taxAndOtherInfo["min_distance"] = $earningDetail["min_distance"];
            foreach ($restaurantDetails["delivery_zone"] as $zone){
                if($zone["district_id"] == $shippingAddress->district_id) {
                    $deliveryZoneCheckFlag = true;
                    break;
                }
            }
            if(!$deliveryZoneCheckFlag){
                return response()->json([
                    "status" => "403",
                    "message" => "Your delivery address is outside of restaurant delivery zone."
                ],403);
            }
            $calculateDistanceInKm =\CalculateDistance::getDistance($restaurantDetails["latitude"], $restaurantDetails["longitude"], $shippingAddress->latitude, $shippingAddress->longitude);
            $cost = 0;
            // dd($calculateDistanceInKm);
            if ($calculateDistanceInKm <= $taxAndOtherInfo["min_distance"]) {
                $cost = $taxAndOtherInfo["min_cost"];
            } else {
                $cost = ($taxAndOtherInfo["min_cost"] + (($calculateDistanceInKm - $taxAndOtherInfo["min_distance"]) * $taxAndOtherInfo["per_cost"]));
            }
            $delivery_fee_limit = \Settings::getSettings('delivery-fee-limit');
            if ($delivery_fee_limit["status"] != 200) {
                return response()->json(
                    $delivery_fee_limit["message"]
                    , $delivery_fee_limit["status"]);
            }
            $delivery_limit = (float)$delivery_fee_limit['message']['data']['value'];
            if($delivery_limit < $cost){
                $cost = $delivery_limit;
            }

	        // check delivery fee is free or not @TODO: Implement other method
	        if( $restaurantDetails['free_delivery'] == 1 ) {
		        $cost = 0;
	        }

            $shippingInfoResponse["distance"] = $calculateDistanceInKm;
            $shippingInfoResponse["min_cost"] = $earningDetail["min_earning"];
            $shippingInfoResponse["per_cost"] = $earningDetail["earning_per_km"];
            $shippingInfoResponse["min_distance"] = $earningDetail["min_distance"];
            $shippingInfoResponse["amount_without_tax"] = round($cost,2);
            $shippingInfoResponse["tax"] = 0;
            $shippingInfoResponse["amount"] = round($cost ,2);
            $shippingInfoResponse["amount_in_currency"] = $countryInfo->currency_symbol .  $shippingInfoResponse["amount"];
            $shippingInfoResponse["currency"] = $countryInfo->currency_symbol;
            return response()->json([
                "status" => "200",
                "data" => $shippingInfoResponse
            ]);
        }
        catch(ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "data" => "Shipping address not found"
            ],404);

        }
        catch(\Exception $ex){
            return response()->json([
                "status" => "500",
                "data" => $ex->getMessage()
            ],500);

        }

    }

    public function calculateTaxForCart(Request $request)
    {
        try {
            $this->validate($request, [
                "city_id" => "required|integer|min:1",
                "amount" => "required|numeric|min:0"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }


        try {
            $city = $this->city->cityGetSpecificTax($request->city_id);
            $country = $city->country()->where("status", 1)->first();
            if (!$country) {
                throw new ModelNotFoundException();
            }

            if ($city->count() == 0  ) {
                $taxInfo["tax_percentage"] = $country["tax_percentage"];
            } else {
                $taxInfo["tax_percentage"] = $city["tax_percentage"];
            }

            $taxInfo["amount"] = $taxInfo["tax_percentage"] * 0.01 * $request->amount;

            return response()->json([
                "status" => "200",
                "data" => $taxInfo
            ]);
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Country Could not be found"
            ], 404);
        } catch (\Exception $ex) {
            Log::error('error calculating tax',[
                'request' => $request->all(),
                'error_message' => $ex->getMessage()
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Country Could not be found"
            ], 500);
        }

    }

}