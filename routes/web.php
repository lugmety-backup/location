<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(['Get tax_percentage and other information'], function () use ($app) {

    $app->post('country/{country_id:[0-9]+}/city/{city_id}',[
        'middleware' => 'checkTokenPermission',
        'uses' => 'TaxAndDeliveryChargeController@getTaxRelatedInfo'
    ]);

     $app->get('user/{userId:[0-9]+}/shipping/{id:[0-9]+}',[
         'uses' => 'TaxAndDeliveryChargeController@getShippingInfo'
     ]);

    $app->post('calculate/tax',[
        'uses' => 'TaxAndDeliveryChargeController@calculateTaxForCart'
    ]);

    $app->post('shipping/cost',[
        'uses' => 'TaxAndDeliveryChargeController@calculateShippingCost'
    ]);

    $app->post('country-name',[
        'uses' => 'ReportController@getCountry'
    ]);

    $app->get('notification/country/{country_id:[0-9]+}',[
        'uses' => 'CountryController@getCountryForNotification'
    ]);

});

//$app->group(['shipping cost calculation'], function () use ($app) {
//    $app->post('user/cost',['middleware' => 'checkTokenPermission',
//        'uses' => 'ShippingAddressController@getDistanceCost'
//    ]);
//});


$app->group(['Country route group'], function () use ($app) {

    $app->post('country/list',[
        'uses' => 'CountryController@getCountryList'
    ]);
    $app->post('language/validation', [
        'uses' => 'CountryController@languageValidation']);

    $app->get('public/country',[
        'uses' => 'CountryController@publicIndex'
    ]);

    $app->get('public/country/{id}', [
        'uses' => 'CountryController@publicShow']);

    $app->get('datatable/country',['middleware' => 'checkTokenPermission:country-view',//added permission
        'uses' => 'CountryController@adminIndex'
    ]);

    $app->get('country',['middleware' => 'checkTokenPermission:country-view',
        'uses' => 'CountryController@index'
    ]);

    $app->get('country/{id}', ['middleware' => 'checkTokenPermission:country-view',//added permission
        'uses' => 'CountryController@adminShow']);

//    $app->get('country/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:country-view',
//        'uses' => 'CountryController@show']);

    $app->post('country', ['middleware' => 'checkTokenPermission:country-create',
        'uses' => 'CountryController@store']);

    $app->put('country/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:country-edit',
        'uses' => 'CountryController@update']);

    $app->delete('country/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:country-delete',
        'uses' => 'CountryController@destroy']);

    $app->get('public/country/code/{code}', [
        'uses' => 'CountryController@getCountryByCode']);

    $app->get('fetchLocationDetail', [
        'uses' => 'DistrictController@getLocationDetailFromGeoLocation']);

    $app->get('fetchGoogleCoordinate', [
        'uses' => 'DistrictController@getGeoLocationFromLocationDetail']);
});

//$app->group(['Country translation route group'], function () use ($app) {
//
//    $app->get('country/translation', [
//        'uses' => 'CountryTranslationController@index']);
//    $app->get('country/translation/{id:[0-9]+}', [
//        'uses' => 'CountryTranslationController@show']);
//    $app->post('country/translation', ['middleware' => 'checkTokenPermission:country-create',
//        'uses' => 'CountryTranslationController@store']);
//    $app->put('country/translation/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:country-edit',
//        'uses' => 'CountryTranslationController@update']);
//    $app->delete('country/translation/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:country-delete',
//        'uses' => 'CountryTranslationController@destroy']);
//});
//
//$app->group(['Country Meta route group'], function () use ($app) {
//
//    $app->get('country/meta', [
//        'uses' => 'CountryMetaController@index']);
//    $app->get('country/meta/{id:[0-9]+}', [
//        'uses' => 'CountryMetaController@show']);
//    $app->post('country/meta/', ['middleware' => 'checkTokenPermission:country-create',
//        'uses' => 'CountryMetaController@store']);
//    $app->put('country/meta/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:country-edit',
//        'uses' => 'CountryMetaController@update']);
//    $app->delete('country/meta/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:country-delete',
//        'uses' => 'CountryMetaController@destroy']);
//});

$app->group(['Language route group'], function () use ($app) {

    $app->get('public/language', [
        'uses' => 'LanguageController@publicIndex']);

    $app->get('public/language/{id:[0-9]+}', [
        'uses' => 'LanguageController@publicShow']);

    $app->get('datatable/language', ['middleware' => 'checkTokenPermission:view-language',//added permission
        'uses' => 'LanguageController@adminIndex']);

    $app->get('datatable/language/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:view-language',//added permission
        'uses' => 'LanguageController@adminShow']);

    $app->post('language', ['middleware' => 'checkTokenPermission:create-language',
        'uses' => 'LanguageController@store']);

    $app->put('language/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:edit-language',
        'uses' => 'LanguageController@update']);

    $app->delete('language/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:delete-language',
        'uses' => 'LanguageController@destroy']);
});

//$app->group(['Country Language route group'], function () use ($app) {
//
//    $app->get('country/{id:[0-9]+}/language', [
//        'uses' => 'CountryLanguageController@index']);
//    $app->post('country/{id:[0-9]+}/language',['middleware'=>'checkTokenPermission:country-language-create',
//        'uses'=>'CountryLanguageController@store']);
//    $app->delete('country/{id:[0-9]+}/language',[
//        'middleware'=>'checkTokenPermission:country-language-delete',
//        'uses'=>'CountryLanguageController@store']);
//});


$app->group(['City route group'], function () use ($app) {


    $app->get('public/country/{country_id}/city', [
        'uses' => 'CityController@publicIndex']);

    $app->get('public/city/{id:[0-9]+}', [
        'uses' => 'CityController@publicShow']);


    $app->get('datatable/city', ['middleware' => 'checkTokenPermission:city-view',
        'uses' => 'CityController@adminIndex']);

    $app->get('city', ['middleware' => 'checkTokenPermission:city-view',
        'uses' => 'CityController@index']);

    $app->get('check/country/{id:[0-9]+}/city/{city_id:[0-9]+}', [
        'uses' => 'CityController@checkCountryAndCity']);

//    $app->get('city/{id:[0-9]+}', ['middleware' => 'checkTokenPermission',
//        'uses' => 'CityController@show']);

    $app->get('city/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:city-view',
        'uses' => 'CityController@adminShow']);

    $app->post('city', ['middleware' => 'checkTokenPermission:city-create',
        'uses' => 'CityController@store']);

    $app->put('city/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:city-edit',
        'uses' => 'CityController@update']);

    $app->delete('city/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:city-delete',
        'uses' => 'CityController@destroy']);
});

//$app->group(['City translation route group'], function () use ($app) {
//
//    $app->get('city/translation', [
//        'uses' => 'CityTranslationController@index']);
//    $app->get('city/translation/{id:[0-9]+}', [
//        'uses' => 'CityTranslationController@show']);
//    $app->post('city/translation', ['middleware' => 'checkTokenPermission:city-create',
//        'uses' => 'CityTranslationController@store']);
//    $app->put('city/translation/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:city-edit',
//        'uses' => 'CityTranslationController@update']);
//    $app->delete('city/translation/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:city-delete',
//        'uses' => 'CityTranslationController@destroy']);
//});
$app->group(['District route group'], function () use ($app) {

    $app->get('public/city/{city_id:[0-9]+}/district', [
        'uses' => 'DistrictController@publicIndex']);

    $app->get('public/country/{country_id:[0-9]+}/district', [
        'uses' => 'DistrictController@cityOfCountry']);

    $app->get('public/district/{id:[0-9]+}', [
        'uses' => 'DistrictController@publicShow']);

    $app->get('datatable/district', ['middleware' => 'checkTokenPermission:district-view',
        'uses' => 'DistrictController@adminIndex']);

//    $app->get('district/{id:[0-9]+}', ['middleware' => 'checkTokenPermission',
//        'uses' => 'DistrictController@show']);

    $app->get('district/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:district-view',
        'uses' => 'DistrictController@adminShow']);

    $app->get('district', ['middleware' => 'checkTokenPermission:district-view',
        'uses' => 'DistrictController@index']);

    $app->post('district', [
        'middleware' => 'checkTokenPermission:district-create',
        'uses' => 'DistrictController@store']);

    $app->put('district/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:district-edit',
        'uses' => 'DistrictController@update']);

    $app->delete('district/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:district-delete',
        'uses' => 'DistrictController@destroy']);

    $app->post('public/district/getnames',['uses'=>"DistrictController@getNames"]);

    $app->post('public/district/getCode',['uses'=>"DistrictController@getCurrencyCodeByCountryId"]);

    $app->get('location/names',['uses'=>"DistrictController@getLocationNames"]);
});

//$app->group(['District translation route group'], function () use ($app) {
//
//    $app->get('district/translation', [
//        'uses' => 'DistrictTranslationController@index']);
//    $app->get('district/translation/{id:[0-9]+}', [
//        'uses' => 'DistrictTranslationController@show']);
//    $app->post('district/translation', ['middleware' => 'checkTokenPermission:district-create',
//        'uses' => 'DistrictTranslationController@store']);
//    $app->put('district/translation/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:district-edit',
//        'uses' => 'DistrictTranslationController@update']);
//    $app->delete('district/translation/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:district-delete',
//        'uses' => 'DistrictTranslationController@destroy']);
//});

$app->group(['Billing Address  route group'], function () use ($app) {

    $app->get('billing/address', [
        'middleware' => 'checkTokenPermission:billing-address-view',
        'uses' => 'BillingAddressController@index']);

    $app->get('datatable/billing/address', [
        'middleware' => 'checkTokenPermission:billing-address-view',
        'uses' => 'BillingAddressController@adminIndex']);

    $app->get('datatable/billing/address/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:billing-address-view',
        'uses' => 'BillingAddressController@adminShow']);

    $app->get('billing/address/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:billing-address-view',
        'uses' => 'BillingAddressController@show']);

    $app->post('billing/address', ['middleware' => 'checkTokenPermission:billing-address-create',
        'uses' => 'BillingAddressController@store']);

    $app->put('billing/address/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:billing-address-edit',
        'uses' => 'BillingAddressController@update']);

    $app->delete('billing/address/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:billing-address-delete',
        'uses' => 'BillingAddressController@destroy']);
});

$app->group(['Shipping Address route group'], function () use ($app) {

    $app->post('shipping/address/list', [
        'uses' => 'ShippingAddressController@getListOfShippingAddressBased']);

    $app->get('datatable/shipping/address', ['middleware' => 'checkTokenPermission:shipping-address-view',
        'uses' => 'ShippingAddressController@adminIndex']);

    $app->get('pagination/shipping/address', ['middleware' => 'checkTokenPermission:shipping-address-view',
        'uses' => 'ShippingAddressController@indexPagination']);

    $app->get('datatable/shipping/address/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:shipping-address-view',
        'uses' => 'ShippingAddressController@adminShow']);

    $app->get('shipping/address', ['middleware' => 'checkTokenPermission:shipping-address-view',
        'uses' => 'ShippingAddressController@index']);

    $app->get('shipping/address/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:shipping-address-view',
        'uses' => 'ShippingAddressController@show']);
    /**
     * call for machine integration id purpose
     */
    $app->get('machine/integration/shipping/address/{id:[0-9]+}', [
        'uses' => 'ShippingAddressController@showShippingForMachineIntegration']);

    $app->get('public/shipping/address/{id:[0-9]+}', [
        'uses' => 'ShippingAddressController@showForInternalCall']);

    $app->post('shipping/address', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'ShippingAddressController@store']);

    $app->put('shipping/address/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:shipping-address-edit',
        'uses' => 'ShippingAddressController@update']);

    $app->delete('shipping/address/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:shipping-address-delete',
        'uses' => 'ShippingAddressController@destroy']);

    $app->put('shipping/address/setprimary/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:shipping-address-set-primary',
        'uses' => 'ShippingAddressController@makePrimary']);
});
//
//$app->get('connection/checker', 'ConnectionController@index');
//$app->get('redis/slug/{slug}', function ($slug) use ($app) {
//    return \Redis::get($slug);
//});

//$app->group(['Shipping Address Meta route group'], function () use ($app) {
//
//    $app->get('shipping/address/meta', [
//        'uses' => 'ShippingAddressMetaController@index']);
//    $app->get('shipping/address/meta/{id:[0-9]+}', [
//        'uses' => 'ShippingAddressMetaController@show']);
//    $app->post('shipping/address/meta', ['middleware' => 'checkTokenPermission:shipping-address-create',
//        'uses' => 'ShippingAddressMetaController@store']);
//    $app->put('shipping/address/meta/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:shipping-address-edit',
//        'uses' => 'ShippingAddressMetaController@update']);
//    $app->delete('shipping/address/meta/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:shipping-address-delete',
//        'uses' => 'ShippingAddressMetaController@destroy']);
//});

$app->get('export-country', 'ExportController@exportCountry'
);
$app->get('export-city', 'ExportController@exportCity'
);

$app->get('export-district', 'ExportController@exportDistrict'
);

$app->get('export-shipping-address', 'ExportController@exportShippingAddress'
);

$app->get('export-billing-address', 'ExportController@exportBillingAddress'
);

$app->get('connection/checker', 'ConnectionController@index');

$app->get('user/delivery-district/{id:[0-9]+}','ReportController@userDemoReportDeliveryDistrict');

$app->get('test/error/message','ExampleController@index');