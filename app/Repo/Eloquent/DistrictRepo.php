<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:02 AM
 */

namespace App\Repo\Eloquent;

use App\District;
use App\Repo\DistrictInterface;

class DistrictRepo implements DistrictInterface
{


    private $district;

    /**
     * CountryRepo constructor.
     * @param $country
     */
    public function __construct(District $district)
    {
        $this->district = $district;
    }

    public function districtGetAll($status)//get all the district information from database
    {
        // TODO: Implement countryGetAll() method.
        if(is_null($status)){
            return $this->district->all();
        }
        return $this->district->where('status',$status)->get();
    }

    public function districtGetAllPagination($status,$city_id,$limit)
    {
        if(is_null($status) && is_null($city_id)){
            $district = $this->district->paginate($limit);
        }
        else if(is_null($city_id)){
            $district = $this->district->where('status',$status)->paginate($limit);
        }
        else if(is_null($status)){
            $district = $this->district->where('city_id',$city_id)->paginate($limit);
        }else
        {
            $district  = $this->district->where([['status',$status],['city_id',$city_id]])->paginate($limit);
        }
        $district->appends(["limit" => $limit])->withPath("/district");
        return $district;
    }

    public function districtGetAllByCityId($city_id,$status)
    {
        // TODO: Implement countryGetAll() method.
        if(is_null($status)){
            return $this->district->where('city_id',$city_id)->get();
        }
        return $this->district->where([['status',1],['city_id',$city_id]])->get();
    }

    public function districtGetSpecific($id)
    {
        return $this->district->findOrFail($id);

    }

    public function districtGetAllForDataTable($city_id)
    {
        if($city_id == null){
            return $this->district->all();
        }
        return $this->district->where("city_id",$city_id)->get();
    }

    public function districtGetAllForDatatablesOnlyDeleted($city_id)
    {
        if($city_id == null){
            return $this->district->onlyTrashed()->get();
        }
        return $this->district->onlyTrashed()->where("city_id",$city_id)->get();
    }

    public function districtGetSpecificByStatus($id,$status)
    {
        return $this->district->where([

            ["status" , $status]
        ])->findOrFail($id);
    }

    public function getDistrictFromCityId($status,$city_id,$limit)
    {
        if(is_null($status)){
            $districts= $this->district->where('city_id',$city_id)->paginate($limit);
        }
        else{
            $districts= $this->district->where([['status',$status],['city_id',$city_id]])->paginate($limit);
        }
        $districts->appends(["limit" =>$limit,])->withPath("/city/$city_id/district");
        return $districts;
    }

    public function districtCreate(array $attributes)
    {
        return $this->district->create($attributes);

    }

    public function districtUpdate($id, array $attributes)
    {
        $con=$this->district->findOrFail($id)->update($attributes);
        $con=$this->district->findOrFail($id)->fill($attributes);
        return $con;
    }

    public function districtDelete($id)
    {
        $data=$this->district->findOrFail($id)->delete();
        return true ;
    }

    public function districtGetSpecificSoftOrHardDeleted($id){
        return $this->district->withTrashed()->findOrFail($id);
    }

    public function districtIdToNames($districts){
        $count=0;
        $returnData=[];
        foreach ($districts as $district){
            $data = $this->district->select('id as district_id' )->find($district);
            if($data != null){
                $returnData[$count]=$data;
                $count++;
            }
        }
        return $returnData;
    }

}