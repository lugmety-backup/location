<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:03 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class CityTranslation extends Model
{
    public $timestamps = false;
    protected $guarded=['id'];
    protected $table='city_translation';

    public function city(){
        return $this->belongsTo('App\City','city_id');
    }
    public function setLangCodeAttribute($value){
        $this->attributes['lang_code'] = strtolower($value);
    }
}