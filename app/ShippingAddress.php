<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:12 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShippingAddress extends Model
{
    use SoftDeletes;
    protected $guarded=['id'];
    protected $table='shipping_address';
    protected $hidden = ["district"];
    protected $fillable = [
        "user_id","address_name","street","longitude","latitude",
        "country_id","city_id","zip_code","house_number","floor_number",
        "building_name","office_name","district_id","extra_direction","type","primary"
    ];
    public function district(){
        return $this->belongsTo('App\District','district_id');
    }

    public function shippingAddressMeta(){
        return $this->hasMany('App\ShippingAddressMeta','shipping_address_id');
    }

    public function shippingAddressAdditionalField(){
        return $this->hasMany('App\ShippingAddressAdditionalField','shipping_address_id');
    }
}