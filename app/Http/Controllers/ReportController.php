<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 12/18/17
 * Time: 2:29 PM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function __construct()
    {
    }

    public function userDemoReportDeliveryDistrict($id,Request $request)
    {
        $limit = $request['limit'];
        try{
            $district = DB::table('shipping_address')->join("district", "district.id","=","shipping_address.district_id")->join("district_translation","district.id","=","district_translation.district_id")->join("city","district.city_id","=","city.id")
                ->select('shipping_address.district_id', 'district_translation.name', DB::raw('count(DISTINCT shipping_address.user_id) as user_count'))
                ->where([
                    ["district_translation.lang_code","=","en"],
                    ['city.country_id',"=",$id]
                ])->whereBetween('shipping_address.created_at', [$request['start_date'], $request['end_date']])
                ->groupBy('shipping_address.district_id')
                ->orderBy('user_count',"desc")

                ->limit($limit)->get()
            ;
            return response()->json([
                "status" => "200",
                "data" =>$district
            ], 200);
        }catch (\Exception $ex)
        {
            return response()->json([
                "status" => "400",
                "message" => $ex->getMessage()
            ], 400);
        }
    }

    public function getCountry(Request $request)
    {
        try{
            $country = DB::table('country_translation')->where('lang','en')->whereIn('country_id', $request['country_id'])->pluck('name','country_id');
            return response()->json([
                "status" => "200",
                "message" => $country
            ], 200);

        }catch (\Exception $ex)
        {
            return response()->json([
                "status" => "400",
                "message" => $ex->getMessage()
            ], 400);
        }
    }



}