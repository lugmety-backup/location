<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 2:28 PM
 */

namespace App\Repo;


interface CountryTranslationInterface
{

    public function countryTranslationGetAll();

    public function countryTranslationGetSpecific($id);

    public function countryTranslationGetSpecificByName($name);

    public function countryTranslationGetSpecificByNameCountryId($name,$id);

    public function countryTranslationGetByCountryId($id);

    public function countryTranslationCreate(array $attributes);

    public function countryTranslationUpdate($id, array $attributes);

    public function countryTranslationUpdateOrCreate(array $attributes);

    public function countryTranslationDelete($id);
}