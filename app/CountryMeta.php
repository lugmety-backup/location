<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 12:44 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class CountryMeta extends Model
{
    protected $guarded=['id'];
    protected $table='country_meta';
    protected $hidden=array('country_id');
    public $timestamps = false;
    public function country(){
        return $this->belongsTo('App\Country','country_id');
    }
    public function setMetakeyAttribute($value){
        $this->attributes['meta_key'] = strtolower($value);
    }
}