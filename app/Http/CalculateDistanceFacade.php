<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/25/2017
 * Time: 10:51 PM
 */

namespace App\Http;

use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Config;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Class RemoteCallFacade
 * @package App\Http
 */
class CalculateDistanceFacade
{

    /**
     * RemoteCallFacade constructor.
     * @param Client $guzzle
     */
    public function __construct(Client $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    /**
     * @param $url
     * @return array
     */
    public function getDistance($source_latitude,$source_longitude,$destination_latitude,$destination_longitude){
        $data = \Settings::getSettings('google-apikey');
        if ($data["status"] != 200) {
            return response()->json(
                $data["message"]
                , $data["status"]);
        }
        $apiKey = $data['message']['data']['value'];
//         $apiKey = Config::get("config.google_apiKey");
        $long1 = $source_longitude;
        $lat1 = $source_latitude;
        $long2 = $destination_longitude;
        $lat2 = $destination_latitude;
        try {
            $data = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=$lat1,$long1&destinations=$lat2,$long2&key=$apiKey");
            $data = json_decode($data);
            $status = $data->rows[0]->elements[0]->status;
            $distance = $data->rows[0]->elements[0]->distance->text;
            $distanceInKm = explode(' ', $distance);;

            if($status != "OK" && !isset($distance)){
                throw new \Exception();
            }
            $distance=$distanceInKm[0];

        }catch (\Exception $ex){
            $distance = $this->haversineGreatCircleDistance($lat1,$long1,$lat2,$long2);
        }
        return $distance;
    }

    /**
     * Calculates the great-circle distance between two points, with
     * the Haversine formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [km]
     */
    private  function haversineGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return ($angle * $earthRadius)/1000;
    }
}