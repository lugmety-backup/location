<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/13/2017
 * Time: 12:06 PM
 */

namespace App\Repo;


interface CityTranslationInterface
{
    public function cityTranslationGetAll();

    public function cityTranslationGetByCityId($id);

    public function cityTranslationGetSpecific($id);

    public function cityTranslationGetSpecificByCode($code, $city_id);

    public function cityTranslationCreate(array $attributes);

    public function cityTranslationUpdate($id, array $attributes);

    public function cityTranslationUpdateOrCreate(array $attributes);

    public function cityTranslationDelete($id);
}