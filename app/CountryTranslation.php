<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/9/2017
 * Time: 5:49 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class CountryTranslation extends Model
{
    protected $guarded=['id'];
    protected $table='country_translation';
    protected $hidden=array('id','country_id');
    public $timestamps = false;
    public function country(){
       return $this->belongsTo('App\Country','country_id');
    }
    public function setLangAttribute($value){
        $this->attributes['lang'] = strtolower($value);
    }
}