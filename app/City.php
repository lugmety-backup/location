<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:00 PM
 */

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
class City extends Model
{
    use SoftDeletes;
    protected $guarded=['id'];
    protected $table='city';
    protected $dates = ['deleted_at'];
    protected $hidden = ["country"];
    public function country(){
        return $this->belongsTo('App\Country');
    }

    public function cityTranslation(){
        return $this->hasMany('App\CityTranslation','city_id');
    }



    public function district(){
        return $this->hasMany('App\District','city_id');
    }

//    public function getTableColumns() {
//        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
//    }
}