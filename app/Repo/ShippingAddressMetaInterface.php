<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 5:17 PM
 */

namespace App\Repo;


interface ShippingAddressMetaInterface
{
    public function shippingAddressMetaGetAll();

    public function shippingAddressMetaGetAllByShippingAddressId($id);

    public function shippingAddressMetaGetSpecificByKey($key, $id);

    public function shippingAddressMetaGetSpecific($id);

    public function shippingAddressMetaCreate(array $attributes);

    public function shippingAddressMetaUpdate($id, array $attributes);

    public function shippingAddressMetaUpdateOrCreate(array $attributes);

    public function shippingAddressMetaDelete($id);
}