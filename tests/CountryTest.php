<?php

/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/9/2017
 * Time: 10:56 AM
 */
use App\City;
use Laravel\Lumen\Testing\DatabaseTransactions;
class CountryTest extends TestCase
{
    use DatabaseTransactions;
    public function __construct(){
        $access_token=$this->getAccessToken();
        $this->access_token1=$access_token[0];//having user access
        $this->access_token2=$access_token[1];//not having access

    }



    public function testGetCorrectSpecificCountry(){
        $country=factory(\App\Country::class)->create();
        $t=\Illuminate\Support\Facades\DB::table('language')->where('code','en')->first();
        $test=\Illuminate\Support\Facades\DB::table('country_language')->insert(
            [
                'country_id' => $country->id,
                'language_id' => $t->id
            ]
        );
        $translation=factory(\App\CountryTranslation::class)->create(['country_id'=>$country->id]);
        $meta=factory(\App\CountryMeta::class)->create(['country_id'=>$country->id]);
        $res=$this->get("country/$country->id");
        $this->seeJson([
            'data'=>[
                'id'=>$country->id,
                'code'=>$country->code,
                'currency_code'=>$country->currency_code,
                'currency_symbol'=>$country->currency_symbol,
                'calling_code'=>$country->calling_code,
                'timezone'=>$country->timezone,
                'tax_percentage'=>$country->tax_percentage,
                'status'=>$country->status,
                'week_day_start_at'=>$country->week_day_start_at,
                'lang'=>$translation->lang,
                'name'=>$translation->name,
                'meta'=>[
                    [
                    "id"=>$meta->id,
                    'meta_key'=>$meta->meta_key,
                    'meta_value'=>$meta->meta_value
                    ]
                ],
                'language'=>[
                    [
                        'id'=>$t->id,
                        'name'=>$t->name,
                        'code'=>$t->code,
                        'status'=>$t->status
                    ]

                ]

                ],'status'=>'200'
        ])->assertResponseStatus(200);
    }

    public function testGetCorrectSpecificCountryNotExistedInDB(){
        $country=factory(\App\Country::class)->create();
        \App\Country::whereId($country->id)->delete();
        $this->get('/country/country->id')->assertResponseStatus(404);
    }

    public function testGetCorrectSpecificCountryWithGivenLanguage(){ //Get Specific record with given language

        $country=factory(\App\Country::class)->create();
        $t=\Illuminate\Support\Facades\DB::table('language')->where('code','ar')->first();
        $test=\Illuminate\Support\Facades\DB::table('country_language')->insert(
            [
                'country_id' => $country->id,
                'language_id' => $t->id
            ]
        );
        $translation=factory(\App\CountryTranslation::class)->create(['country_id'=>$country->id,'lang'=>'ar']);
        $meta=factory(\App\CountryMeta::class)->create(['country_id'=>$country->id]);

        $res=$this->get("/country/$country->id?lang=ar",[
            'Authorization'=>'Bearer '.$this->access_token1
        ]);


        $this->seeJson([
            'data'=>[
                'id'=>$country->id,
                'code'=>$country->code,
                'currency_code'=>$country->currency_code,
                'currency_symbol'=>$country->currency_symbol,
                'calling_code'=>$country->calling_code,
                'timezone'=>$country->timezone,
                'tax_percentage'=>$country->tax_percentage,
                'status'=>$country->status,
                'week_day_start_at'=>$country->week_day_start_at,
                'lang'=>$translation->lang,
                'name'=>$translation->name,
                'meta'=>[
                    [
                        "id"=>$meta->id,
                        'meta_key'=>$meta->meta_key,
                        'meta_value'=>$meta->meta_value
                    ]
                ],
                'language'=>[
                    [
                        'id'=>$t->id,
                        'name'=>$t->name,
                        'code'=>$t->code,
                        'status'=>$t->status
                    ]

                ]

            ],'status'=>'200'
        ])->assertResponseStatus(200);
    }

    public function testGetCorrectSpecificCityWithEnglishInsteadOfGivenLanguage(){//Get Specific record with english as given language as given language name donot exist
        $country=factory(\App\Country::class)->create();
        $t=\Illuminate\Support\Facades\DB::table('language')->where('code','en')->first();
        $test=\Illuminate\Support\Facades\DB::table('country_language')->insert(
            [
                'country_id' => $country->id,
                'language_id' => $t->id
            ]
        );
        $translation=factory(\App\CountryTranslation::class)->create(['country_id'=>$country->id,'lang'=>'en']);
        $meta=factory(\App\CountryMeta::class)->create(['country_id'=>$country->id]);

        $res=$this->get("/country/$country->id?lang=xx",[
            'Authorization'=>'Bearer '.$this->access_token1
        ]);


        $this->seeJson([
            'data'=>[
                'id'=>$country->id,
                'code'=>$country->code,
                'currency_code'=>$country->currency_code,
                'currency_symbol'=>$country->currency_symbol,
                'calling_code'=>$country->calling_code,
                'timezone'=>$country->timezone,
                'tax_percentage'=>$country->tax_percentage,
                'status'=>$country->status,
                'week_day_start_at'=>$country->week_day_start_at,
                'lang'=>'en',
                'name'=>$translation->name,
                'meta'=>[
                    [
                        "id"=>$meta->id,
                        'meta_key'=>$meta->meta_key,
                        'meta_value'=>$meta->meta_value
                    ]
                ],
                'language'=>[
                    [
                        'id'=>$t->id,
                        'name'=>$t->name,
                        'code'=>$t->code,
                        'status'=>$t->status
                    ]

                ]

            ],'status'=>'200'
        ])->assertResponseStatus(200);
    }

    public function testCreateCountry(){//testing for creating country for valid input
        $language1=factory(\App\Language::class)->create();
        $language2=factory(\App\Language::class)->create();
        $request['code']='unique';
        $request['currency_code']='dollar';
        $request['currency_symbol']='$';
        $request['calling_code']='977';
        $request['timezone']="GMT-5:45";
        $request['tax_percentage']=13.33;
        $request['status']=1;
        $request['week_day_start_at']='sunday';
        $request['language']=[$language1->code,$language2->code];
        $request['translation']=[$language1->code=>['name'=>'Nepal']];
        $request['meta']=[['meta_key'=>"temp",'meta_value'=>'temp']];
        $this->post('country',$request)->seeJson([
            'status'=>'200'
        ])->assertResponseStatus(200);
    }
    public function testCreateCountryWithInvalidRequest(){//testing for creating country for invalid input
        //$country=factory(\App\Country::class)->create();
        $this->post('/country',[" "],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(422);
    }
    public function testCreateCountryWithInvalidRequestInTranslation(){//testing for creating country for invalid input in translation
        $language1=factory(\App\Language::class)->create();
        $language2=factory(\App\Language::class)->create();
        $request['code']='unique';
        $request['currency_code']='dollar';
        $request['currency_symbol']='$';
        $request['calling_code']='977';
        $request['timezone']="GMT-5:45";
        $request['tax_percentage']=13.33;
        $request['status']=1;
        $request['week_day_start_at']='sunday';
        $request['language']=[$language1->code,$language2->code];
        $request['translation']=['en'=>['']];
        $request['meta']=[['meta_key'=>"temp",'meta_value'=>'temp']];
        $this->post('country',$request)->assertResponseStatus(422);
    }
    public function testCreateCityWithLanguageNotExisted(){//testing for creating country wit invalid language
        $language1=factory(\App\Language::class)->create();
        \Illuminate\Support\Facades\DB::table('language')->delete($language1->id);
        $request['code']='unique';
        $request['currency_code']='dollar';
        $request['currency_symbol']='$';
        $request['calling_code']='977';
        $request['timezone']="GMT-5:45";
        $request['tax_percentage']=13.33;
        $request['status']=1;
        $request['week_day_start_at']='sunday';
        $request['language']=[$language1->code];
        $request['translation']=[$language1->code=>['name'=>'Nepal']];
        $request['meta']=[['meta_key'=>"temp",'meta_value'=>'temp']];
        $this->post('country',$request)->assertResponseStatus(404);
    }

    public function testUpdate(){
        $country=factory(\App\Country::class)->create();
        $t=\Illuminate\Support\Facades\DB::table('language')->where('code','en')->first();
        $test=\Illuminate\Support\Facades\DB::table('country_language')->insert(
            [
                'country_id' => $country->id,
                'language_id' => $t->id
            ]
        );
        $translation=factory(\App\CountryTranslation::class)->create(['country_id'=>$country->id]);
        $meta=factory(\App\CountryMeta::class)->create(['country_id'=>$country->id]);
        $language1=factory(\App\Language::class)->create();
        $language2=factory(\App\Language::class)->create();
        $request['code']='unique';
        $request['currency_code']='dollar';
        $request['currency_symbol']='$';
        $request['calling_code']='977';
        $request['timezone']="GMT-5:45";
        $request['tax_percentage']=13.33;
        $request['status']=1;
        $request['week_day_start_at']='sunday';
        $request['language']=[$language1->code,$language2->code];
        $request['translation']=[$language1->code=>['name'=>'Nepal']];
        $request['meta']=[['meta_key'=>"temp",'meta_value'=>'temp']];
        $this->put("/country/$country->id",$request)->seeJson([
            'status'=>'200'
        ])->assertResponseStatus(200);
    }
    public function testUpdateWithInvalidId(){
        $country=factory(\App\Country::class)->create();
        \Illuminate\Support\Facades\DB::table('country')->delete($country->id);
        $language1=factory(\App\Language::class)->create();
        $language2=factory(\App\Language::class)->create();
        $request['code']='unique';
        $request['currency_code']='dollar';
        $request['currency_symbol']='$';
        $request['calling_code']='977';
        $request['timezone']="GMT-5:45";
        $request['tax_percentage']=13.33;
        $request['status']=1;
        $request['week_day_start_at']='sunday';
        $request['language']=[$language1->code,$language2->code];
        $request['translation']=[$language1->code=>['name'=>'Nepal']];
        $this->put("/country/$country->id",$request)->assertResponseStatus(409);

    }


    public function testDelete(){
        $country=factory(\App\Country::class)->create();
        $t=\Illuminate\Support\Facades\DB::table('language')->where('code','en')->first();
        $test=\Illuminate\Support\Facades\DB::table('country_language')->insert(
            [
                'country_id' => $country->id,
                'language_id' => $t->id
            ]
        );
        $translation=factory(\App\CountryTranslation::class)->create(['country_id'=>$country->id]);
        $meta=factory(\App\CountryMeta::class)->create(['country_id'=>$country->id]);
        $this->delete("country/$country->id")->assertResponseStatus(200);
    }

}
