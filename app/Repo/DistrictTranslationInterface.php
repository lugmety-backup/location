<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 2:28 PM
 */

namespace App\Repo;


interface DistrictTranslationInterface
{

    public function districtTranslationGetAll();

    public function districtTranslationGetSpecificByCode($code,$district_id);

    public function districtTranslationGetByDistrictId($id);

    public function districtTranslationGetSpecific($id);

    public function districtTranslationCreate(array $attributes);

    public function districtTranslationUpdate($id, array $attributes);

    public function districtTranslationUpdateOrCreate(array $attributes);

    public function districtTranslationDelete($id);
}