<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/25/2017
 * Time: 10:51 PM
 */

namespace App\Http;

/**
 * Class RoleCheckerFacade
 * @package App\Http
 */
class RoleCheckerFacade
{

    /**
     * @var
     */
    protected $accessTokenRepository;

    /**
     * @param $value
     */
    public  function set($value)
    {
        $this->accessTokenRepository = $value;

    }

    /**
     * @param $key
     * @return bool
     */

    public function get( $key ){

        if( isset( $this->accessTokenRepository[ $key ] ) ){

            return $this->accessTokenRepository[ $key ];
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getUser(){
        return $this->accessTokenRepository['user_id'];
    }

    public function getRole(){
        return $this->accessTokenRepository["roles"];
    }


    /**
     * Get restaurant and restaurant branch info of currently logged in user
     * @return array|null
     */
    public function getRestaurantInfo(){
        $restaurant['is_restaurant_admin']=$this->accessTokenRepository["is_restaurant_admin"];
        $restaurant['restaurant_id']=$this->accessTokenRepository["restaurant_id"];
        $restaurant['branch_id']=$this->accessTokenRepository['branch_id'];
        if(is_null( $restaurant['is_restaurant_admin']) && is_null($restaurant['restaurant_id'])){
            return null;
        }
        return $restaurant;

    }



    /**
     * @param $role
     * @return array|bool
     */
    public function hasRole($role)
    {

        $roles=$this->accessTokenRepository["roles"];
        if(is_array($role)){
//            foreach ($roles as $r){
//                foreach ($role as $rol){
//                   if($rol==$r){
//                        $allowed[]=$rol;
//                    }
//                }
//           }
//            return $allowed;
            /*
             * returns an array containing all the values in first
             * argument that are present in all the second argument
             * */
            return array_intersect($roles,$role);
        }

        foreach ($roles as $r){
            if($r==$role){
                return true;
            }
        }
        return false;
    }

}