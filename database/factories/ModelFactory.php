<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'code' => $faker->name,
        'currency_code' => $faker->name,
        'currency_symbol'=> $faker->name,
        'calling_code'=> $faker->name,
        'timezone'=>$faker->name,
        'tax_percentage'=>$faker->name,
    ];
});
$factory->define(App\Country::class, function (Faker\Generator $faker) {
    return [
        'code'=> $faker->firstName,
        'currency_code'=>$faker->firstName,
        'currency_symbol'=>$faker->firstName,
        'calling_code'=>(string)$faker->numberBetween(10,999),
        'timezone'=>$faker->firstName,
        'tax_percentage'=>$faker->numberBetween(10,20),
        'status'=>1,
        'week_day_start_at'=>'sunday'
    ];
});
$factory->define(App\Language::class, function (Faker\Generator $faker) {
    return [
        'code'=> $faker->firstName,
        'status'=>1,
        'name'=> $faker->firstName
    ];
});
$factory->define(App\City::class, function (Faker\Generator $faker) {
    $f=factory(\App\Country::class)->create();
    $t=\Illuminate\Support\Facades\DB::table('language')->where('code','en')->first();
    $test=\Illuminate\Support\Facades\DB::table('country_language')->insert(
        [
            'country_id' => $f->id,
            'language_id' => $t->id
        ]
    );
    $t=\Illuminate\Support\Facades\DB::table('language')->where('code','ar')->first();
    $test=\Illuminate\Support\Facades\DB::table('country_language')->insert(
        [
            'country_id' => $f->id,
            'language_id' => $t->id
        ]
    );

    return [
        'country_id' =>$f->id,
        'timezone' => $faker->firstName,
        'tax_percentage' =>$f->tax_percentage,
        'status'=>1,
    ];
});
$factory->define(App\CountryTranslation::class, function (Faker\Generator $faker) {
    return [
        'country_id' => 1,
        'lang' => 'en',
        'name' => $faker->name,
    ];
});
$factory->define(App\CountryMeta::class, function (Faker\Generator $faker) {
    return [
        'country_id' => 1,
        'meta_key' => $faker->firstName,
        'meta_value' => $faker->firstName,
    ];
});
$factory->define(App\CityTranslation::class, function (Faker\Generator $faker) {
    return [
        'city_id' => 1,
        'lang_code' => 'en',
        'name' => $faker->name,
    ];
});
$factory->define(App\District::class, function (Faker\Generator $faker) {
    $f=factory(\App\City::class)->create();
    return [
        'city_id' =>$f->id,
        'status'=>1,
    ];
});
$factory->define(App\DistrictTranslation::class, function (Faker\Generator $faker) {
    return [
        'district_id' => 1,
        'lang_code' => 'en',
        'name' => $faker->firstName,
    ];
});
$factory->define(App\District::class, function (Faker\Generator $faker) {
    $f=factory(\App\City::class)->create();
    return [
        'city_id' =>$f->id,
        'status'=>1,
    ];
});
