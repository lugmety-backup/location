<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 5:02 PM
 */

namespace App\Http\Controllers;

use App\Country;
use Geocoder\Query\ReverseQuery;
use ImageUploader;
use App\CountryTranslation;
use App\Repo\CountryInterface;
use App\Repo\CountryMetaInterface;
use App\Repo\CountryTranslationInterface;
use App\Repo\LanguageInterface;
use App\Repo\CountryLanguageInterface;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use LogStoreHelper;
use RoleChecker;
use Illuminate\Support\Facades\Config;
use RemoteCall;

class CountryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $countryMeta;
    protected $country;
    protected $countryTranslation;
    protected $language;
    protected $countryLanguage;
    protected $logStoreHelper;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CountryMetaInterface $countryMeta,
                                CountryInterface $country,
                                CountryTranslationInterface $countryTranslation,
                                LanguageInterface $language,
                                CountryLanguageInterface $countryLanguage,
                                LogStoreHelper $logStoreHelper
    )
    {
        $this->countryMeta = $countryMeta;
        $this->country = $country;
        $this->countryTranslation = $countryTranslation;
        $this->language = $language;
        $this->countryLanguage = $countryLanguage;
        $this->logStoreHelper = $logStoreHelper;
    }


    /**
     *
     * Display a listing of the all avialable country ,country translation ,language and country meta.
     *user with role customer does not have access
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminIndex(Request $request)
    {
        if (!RoleChecker::hasRole('customer')) {
            /**
             * get all the data from country  based on status
             * if status is not given all the data is provided
             */
            if ($request['type'] == "deleted") {
                $countries = $this->country->countryGetAllForDatatablesOnlyDeleted();
            } else {
                $countries = $this->country->countryGetAllForDatatables();
            }


            try {
                if (!$countries->first()) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }

            $request['lang'] = "en";

            foreach ($countries as $country) {
                /**
                 * using and eloquent relationship to get translation based on the parameter lang
                 * if the lang parameter is not given or invalid default english language is provided
                 */
                $translation = $country->countryTranslation()->where('lang', $request['lang'])->get();

                /*
                 * $country->countryTranslation will return pivot like
                 * relationship so to remove it above approach is used
                 * */
                if ($translation->count() != 0) {
                    $country['name'] = $translation[0]['name'];
                    $country['lang'] = $translation[0]['lang'];
                }

//                $country['meta'] = $country->countryMeta()->get();
//                $country['language'] = $country->language;
//                $country["translation"] = $country->countryTranslation()->get();
            }
            return \Datatables::of($countries)->make(true);
        } else {
            return response()->json([
                "status" => "403",
                "message" => "You have no access"
            ], 403);
        }
    }

    public function adminIndexSoftDeletedRecords()
    {
        /**
         * get all the data from country  based on status
         * if status is not given all the data is provided
         */
        $countries = $this->country->countryGetAllForDatatablesOnlyDeleted();

        $request['lang'] = "en";

        foreach ($countries as $country) {
            /**
             * using and eloquent relationship to get translation based on the parameter lang
             * if the lang parameter is not given or invalid default english language is provided
             */
            $translation = $country->countryTranslation()->where('lang', $request['lang'])->get();

            /*
             * $country->countryTranslation will return pivot like
             * relationship so to remove it above approach is used
             * */
            if ($translation->count() != 0) {
                $country['name'] = $translation[0]['name'];
                $country['lang'] = $translation[0]['lang'];
            }

//                $country['meta'] = $country->countryMeta()->get();
//                $country['language'] = $country->language;
//                $country["translation"] = $country->countryTranslation()->get();
        }
        return \Datatables::of($countries)->make(true);

    }

    /**
     * Display a listing of the resource based on status,lang parameter and limit.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $request['limit'] = Input::get("limit", 10);

        /*
        * validation of limit param if limit param is not null
        * */
        if (!is_null($request['limit'])) {
            $rules = [
                "limit" => "sometimes|numeric|min:1",
            ];
            $validator = Validator::make($request, $rules);
            if ($validator->fails()) {
                $request['limit'] = 10;
            }
        }

        try {
            /**
             * get all the data from country  based on status
             * if status is not given all the data is provided
             */
            $countries = $this->country->countryGetAll(null, $request['limit']);
            try {
                if (!$countries->first()) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }
            foreach ($countries as $country) {
                /**
                 * using and eloquent relationship to get translation based on the parameter lang
                 * if the lang parameter is not given or invalid default english language is provided
                 */
                $translation = $country->countryTranslation()->where('lang', 'en')->get();
                /*
                 * $country->countryTranslation will return pivot like
                 * relationship so to remove it above approach is used
                 * */
                if ($translation->count() != 0) {
                    $country['name'] = $translation[0]['name'];
                    $country['lang'] = $translation[0]['lang'];
                }

                $country['meta'] = $country->countryMeta()->get();
                $country['language'] = $country->language;
            }

            return response()->json([
                'status' => '200',
                'data' => $countries
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'Requested Country could not be found'
            ], 404);
        }

    }

    /**
     * Return all country which status is 1
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function publicIndex()
    {


        /**
         * Display a listing of the all avialable country ,country translation ,language and country meta to public which status is 1.
         *
         *
         */
        $request['limit'] = Input::get("limit", 10);
        $request['lang'] = Input::get('lang', 'en');

        try {

            /*
       * validation of limit param if limit param is not null
       * */
            if (!is_null($request['limit'])) {
                $rules = [
                    "limit" => "sometimes|numeric|min:1",
                ];
                $validator = Validator::make($request, $rules);
                if ($validator->fails()) {
                    $request['limit'] = 10;
                }
            }
            /**
             * get all the data from country  based on status
             * if status is not given all the data is provided
             */
            $countries = $this->country->countryGetAll(1, $request['limit']);
            $countries->appends(['limit' => $request['limit']])->withPath('/public/country');
            try {
                if (!$countries->first()) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }
            if (!$this->language->languageGetSpecificByCode($request['lang'])->first()) {
                $request['lang'] = "en";
            }
            foreach ($countries as $country) {
                /**
                 * using and eloquent relationship to get translation based on the parameter lang
                 * if the lang parameter is not given or invalid default english language is provided
                 */
                $translation = $country->countryTranslation()->where('lang', $request['lang'])->get();
                /*
                 * $country->countryTranslation will return pivot like
                 * relationship so to remove it above approach is used
                 * */
                if ($translation->count() != 0) {
                    $country['name'] = $translation[0]['name'];
                    $country['lang'] = $translation[0]['lang'];
                }

                $country['meta'] = $country->countryMeta()->get();
                $country['language'] = $country->language;
            }

            return response()->json([
                'status' => '200',
                'data' => $countries->appends(['lang' => $request['lang']]),
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'Requested Country could not be found'
            ], 404);
        }
    }

    public function languageValidation(Request $request)
    {
        try {
            $this->validate($request, [
                'language' => 'required|array',

            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        if (!in_array("en", $request["language"])) {
            return response()->json([
                "status" => "422",
                "message" => ["language" => ["The English value is required"]]
            ], 422);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $request['status'] = Input::get('status', 1);//1 means active
            $this->validate($request, [
                'code' => 'required',
                'currency_code' => 'required|alpha',
                'currency_symbol' => 'required',
                'calling_code' => 'required|numeric',
                'tax_percentage' => 'required|numeric|between:0,99.99',
                'timezone' => 'required',
                'week_day_start_at' => 'required|alpha',
                'min_cost' => 'required|numeric|min:0',
                'per_cost' => 'required|numeric|min:0',
                'status' => 'integer|min:0|max:1',
                "min_distance" => 'required|numeric|min:0',
                'language' => 'required|array',
                'translation' => 'required|array',
                "meta" => "array",
                "flag" => "required"
            ]);

            $countryCheck = $this->country->countryGetSpecificByCode(strtolower($request['code']));

            try {
                if ($countryCheck->count() != 0) {

                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                DB::rollBack();
//storing the log
                $this->logStoreHelper->storeLogError(array('Country', [
                    'status' => '409',
                    'message' => "Country could not be created due to duplicate entry"
                ]));

                return response()->json([
                    'status' => '409',
                    'message' => "Country could not be created due to duplicate entry"
                ], 409);


            }
            $country['code'] = $request['code'];
            $country['currency_code'] = $request['currency_code'];
            $country["currency_symbol"] = $request["currency_symbol"];
            $country["calling_code"] = $request["calling_code"];
            $country["tax_percentage"] = $request["tax_percentage"];
            $country["timezone"] = $request["timezone"];
            $country["week_day_start_at"] = $request["week_day_start_at"];
            $country["min_cost"] = $request["min_cost"];
            $country["per_cost"] = $request["per_cost"];
            $country['status'] = $request['status'];
            $country["min_distance"] = $request["min_distance"];
            if ($request->has('flag')) {
                $image['avatar'] = $request["flag"];
                $image["type"] = "flag";
                $imageUpload = ImageUploader::upload($image);
                if ($imageUpload["status"] != 200) {
                    return response()->json(
                        $imageUpload["message"]
                        , $imageUpload["status"]);
                }
                $country['flag'] = $imageUpload["message"]["data"];
            }
            $country = $this->country->countryCreate($country);//Creating country in country table...

            $this->logStoreHelper->storeLogInfo(array('Country ', [
                "status" => "200",
                'message' => 'Country Created Successfully',
                "data" => $country]));//object to array conversion

//Attaching Language to Country............................
            if (!in_array("en", $request['language'])) {
                return response()->json([
                    "status" => "422",
                    "message" => "The en language code must be present."
                ], 422);
            }
            foreach ($request['language'] as $req) {
                try {
                    $language = $this->language->languageGetSpecificByCodeAndEnabled($req);

                    if (count($language) == 0) {
                        throw new \Exception();
                    }
                } catch (\Exception $ex) {
                    DB::rollBack();
                    $this->logStoreHelper->storeLogError(array('Country Language', [
                        'status' => '404',
                        'message' => "Language could not be found"
                    ]));
                    return response()->json([
                        'status' => '404',
                        'message' => "Language could not be found"
                    ], 404);
                }
                //return response()->json($language[0]->id);
                $requestLanguage = (string)$language[0]->id;
                $countrylanguage = $this->countryLanguage->attachCountryToLanguage($country->id, $requestLanguage);

                $this->logStoreHelper->storeLogInfo(array('Country Language', [
                    'status' => '200',
                    "message" => 'Country language assigned successfully',
                    'data' => $countrylanguage
                ]));
            }

//Creating Country Meta...........................
            if ($request->has('meta')) {
                $rules = [
                    'meta_key' => 'required',
                    'meta_value' => 'required',
                ];
                foreach ($request['meta'] as $key => $req) {
                    try {

                        $validator = Validator::make($req, $rules);

                        if ($validator->fails()) {
                            $error = $validator->errors();
                            DB::rollBack();
                            $this->logStoreHelper->storeLogError(array('Country Meta', [
                                'status' => '422',
                                "message" => ["meta" => ["$key" => $error]]
                            ]));
                            return response()->json([
                                'status' => '422',
                                "message" => ["meta" => ["$key" => $error]]
                            ]);
                        }
                        $req['country_id'] = $country->id;

                        $countryMeta = $this->countryMeta->countryMetaGetSpecificByCode(strtolower($req['meta_key']), $country->id);

                        try {
                            if ($countryMeta->count() != 0) {

                                throw new \Exception();
                            }
                        } catch (\Exception $ex) {
                            DB::rollBack();
                            $this->logStoreHelper->storeLogError(array('Country Meta', [
                                'status' => '409',
                                'message' => "Country Meta could not be created due to duplicate entry "
                            ]));
                            return response()->json([
                                'status' => '409',
                                'message' => "Country Meta could not be created due to duplicate entry "
                            ], 409);
                        }
                        $countryMeta = $this->countryMeta->countryMetaCreate($req);
                        $this->logStoreHelper->storeLogInfo(array('Country Meta ', [
                            "status" => "200",
                            "message" => "Country Meta Created Successfully",
                            "data" => $countryMeta]));

                    } catch (\Exception $e) {
                        DB::rollBack();
                        $this->logStoreHelper->storeLogError(array('Country Meta', [
                            'status' => '422',
                            'message' => $e->getMessage()
                        ]));
                        return response()->json([
                            'status' => '422',
                            'message' => $e->getMessage()
                        ], 422);
                    }
                }
            }
// Creating Country translation
            $rules = [
                'name' => 'required',
                'lang' => 'required|alpha',
            ];
            foreach ($request['translation'] as $key => $req) {
                try {
                    $req['lang'] = $key;
                    $req['country_id'] = $country->id;
                    $validator = Validator::make($req, $rules);

                    if ($validator->fails()) {
                        $error = $validator->errors();
                        DB::rollBack();
                        $this->logStoreHelper->storeLogError(array('Country Translation', [
                            'status' => '422',
                            "message" => ["translation" => ["$key" => $error]]
                        ]));
                        return response()->json([
                            'status' => '422',
                            "message" => ["translation" => ["$key" => $error]]
                        ], 422);
                    }
                    /**
                     * Checking if given language translation is similar to language attached to country
                     */
                    $check = $this->checkLanguageCode($request['language'], $request['translation']);
                    if ($check == false) {
                        return response()->json([
                            "status" => "422",
                            "message" => ['translation' => ["Input Translation does not match with specified country language code"]]
                        ], 422);
                    }

                    try {
                        $this->country->countryGetSpecific($country->id);
                    } catch (\Exception $ex) {
                        DB::rollBack();
                        $this->logStoreHelper->storeLogError(array('Country Translation', [
                            'status' => '404',
                            'message' => "Country  could not be found"
                        ]));
                        return response()->json([
                            'status' => '404',
                            'message' => "Country  could not be found"
                        ], 404);
                    }
                    $countryTranslation = $this->countryTranslation->countryTranslationGetSpecificByNameCountryId(strtolower($request['name']), $country->id);

                    try {
                        if ($countryTranslation->count() != 0) {

                            throw new \Exception();
                        }
                    } catch (\Exception $ex) {
                        DB::rollBack();
                        $this->logStoreHelper->storeLogError(array('Country Translation ', [
                            'status' => '409',
                            'message' => "Country Translation could not be created due to duplicate entry"
                        ]));
                        return response()->json([
                            'status' => '409',
                            'message' => "Country Translation could not be created due to duplicate entry"
                        ], 409);
                    }
                    $countryTranslation = $this->countryTranslation->countryTranslationCreate($req);
                    $this->logStoreHelper->storeLogInfo(array('Country Translation ', [
                        'status' => '200',
                        'message' => 'Country Translation created successfully',
                        'data' => $countryTranslation
                    ]));

                } catch (\Exception $e) {
                    return response()->json([
                        'status' => '422',
                        'message' => $e->response->original
                    ], 422);
                }
            }

            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'Country created successfully',
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->logStoreHelper->storeLogError(array('error in validation', [
                'status' => '422',
                'message' => $e->response->original
            ]));
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }


    }

    /**
     * Display the specified resource country for public based on $lang url param.whose status is 1
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function getCountryList(Request $request)
    {
        try {
            $this->validate($request, [
                "ids" => "required|array"
            ]);
            $countryList = $this->country->getCountryList($request->ids);
            foreach ($countryList as $list) {
                $list["name"] = $list->countryTranslation()->where("lang", "en")->first()->name;
            }
            return response()->json([
                "status" => "200",
                "data" => $countryList
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ], 500);
        }
    }

    public function publicShow($id)
    {

        $request['lang'] = Input::get('lang', 'en');


        try {
            /*
             * returns country based on id whose status is 1
             * */


            if (ctype_digit($id)) {
                $country = $this->country->getCountrySpecificByStatus($id);
            } else {
                $country = $this->country->countryGetSpecificByCodeByStatusOne($id);
                if (count($country) == 0) {
                    throw new \Exception();
                }
                $country = $country[0];
            }


            if (!$this->language->languageGetSpecificByCode($request['lang'])->first()) {
                $request['lang'] = "en";
            }
            $translation = $country->countryTranslation()->where('lang', $request['lang'])->get();
            if ($translation->count() != 0) {
                $country['name'] = $translation[0]['name'];
                $country['lang'] = $translation[0]['lang'];
            }

            $country['meta'] = $country->countryMeta()->get();
            $country['language'] = $country->language;//show only status 1 for public .remaining
            $country["translation"] = $country->countryTranslation()->get();


            return response()->json([
                'status' => '200',
                'data' => $country

            ], 200);
        } catch (QueryException $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Requested Country could not be found"
            ], 404);
        } catch (ModelNotFoundException  $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Requested Country could not be found"
            ], 404);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Requested Country could not be found"
            ], 404);
        }
    }

    /**
     * Display the specified resource country based on $lang url param.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
//    public function show($id)
//    {
//
//        $request['lang'] = Input::get('lang', 'en');
//
//
//        try {
//            $country = $this->country->countryGetSpecific($id);
//            if (!$this->language->languageGetSpecificByCode($request['lang'])->first()) {
//                $request['lang'] = "en";
//            }
//            $translation = $country->countryTranslation()->where('lang', $request['lang'])->get();
//            if ($translation->count() != 0) {
//                $country['name'] = $translation[0]['name'];
//                $country['lang'] = $translation[0]['lang'];
//            }
//
//            $country['meta'] = $country->countryMeta()->get();
//            $country['language'] = $country->language;
//            $country["translation"] =$country->countryTranslation()->get();
//
//            return response()->json([
//                'status' => '200',
//                'data' => $country
//
//            ], 200);
//        } catch (QueryException $ex) {
//            return response()->json([
//                'status' => '404',
//                'message' => "Requested Country could not be found"
//            ], 404);
//        } catch (ModelNotFoundException  $ex) {
//            return response()->json([
//                'status' => '404',
//                'message' => "Requested Country could not be found"
//            ], 404);
//        } catch (\Exception $ex) {
//            return response()->json([
//                'status' => '404',
//                'message' => "Requested Country could not be found"
//            ], 404);
//        }
//    }


    /**
     * Display the specified resource country based for admin .
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function adminShow($id)
    {
        try {
            if (!RoleChecker::hasRole('customer')) {
                if (ctype_digit($id)) {
                    $country = $this->country->countryGetSpecific($id);
                } else {
                    $country = $this->country->countryGetSpecificByCode($id);
                    if (count($country) == 0) {
                        throw new \Exception();
                    }
                    $country = $country[0];
                }

                $country['meta'] = $country->countryMeta()->get();
                $country['language'] = $country->language;
                $country["translation"] = $country->countryTranslation()->get();

                return response()->json([
                    'status' => '200',
                    'data' => $country

                ], 200);
            } else {
                return response()->json([
                    "status" => "403",
                    "message" => "You have no access"
                ], 403);
            }
        } catch (QueryException $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Requested Country could not be found"
            ], 404);
        } catch (ModelNotFoundException  $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Requested Country could not be found"
            ], 404);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Requested Country could not be found"
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $this->validate($request, [
                'code' => 'required',
                'currency_code' => 'required|alpha',
                'currency_symbol' => 'required',
                'calling_code' => 'required|numeric',
                'tax_percentage' => 'required|numeric|between:0,99.99',
                'timezone' => 'required',
                'week_day_start_at' => 'required|alpha',
                'min_cost' => 'required|numeric|min:0',
                'per_cost' => 'required|numeric|min:0',
                "min_distance" => 'required|numeric|min:0',
                'status' => 'integer|min:0|max:1',
                'translation' => 'required|array',
                'language' => 'required|array',
                "meta" => "array"
            ]);


            try {
                $thisCountry = $this->country->countryGetSpecific($id);
            } catch (\Exception $ex) {
                DB::rollBack();
                $this->logStoreHelper->storeLogError(array('Country', [
                    'status' => '409',
                    'message' => "Country could not be found"
                ]));
                return response()->json([
                    'status' => '409',
                    'message' => "Country could not be found"
                ], 409);
            }
            $countryCheck = $this->country->countryGetSpecificByCode(strtolower($request['code']));
            try {
                if ($countryCheck->count() > 1) {
                    if ($countryCheck->code != $request['code'])
                        throw new \Exception();
                }
            } catch (\Exception $ex) {
                DB::rollBack();
                $this->logStoreHelper->storeLogError(array('Country', [
                    'status' => '409',
                    'message' => "Country could not be created due to duplicate entry"
                ]));
                return response()->json([
                    'status' => '409',
                    'message' => "Country could not be created due to duplicate entry"
                ], 409);


            }

            //updating country......................................................................

            $country['code'] = $request['code'];
            $country['currency_code'] = $request['currency_code'];
            $country["currency_symbol"] = $request["currency_symbol"];
            $country["currency_symbol"] = $request["currency_symbol"];
            $country["calling_code"] = $request["calling_code"];
            $country["tax_percentage"] = $request["tax_percentage"];
            $country["min_cost"] = $request["min_cost"];
            $country["per_cost"] = $request["per_cost"];
            $country["timezone"] = $request["timezone"];
            $country["week_day_start_at"] = $request["week_day_start_at"];
            $country['status'] = $request['status'];
            $country["min_distance"] = $request["min_distance"];
            if ($request->has("flag")) {
                $image['avatar'] = $request["flag"];
                $image["type"] = "flag";
                $imageUpload = ImageUploader::upload($image);
                if ($imageUpload["status"] != 200) {
                    return response()->json(
                        $imageUpload["message"]
                        , $imageUpload["status"]);
                }
                $country['flag'] = $imageUpload["message"]["data"];
            }
            $country = $this->country->countryUpdate($id, $country);
            $this->logStoreHelper->storeLogInfo(array('Country ', [
                "status" => "200",
                "message" => 'Country Updated Sucessfully',
                "data" => $country]));

            //Attaching Language to Country............................

            $this->countryLanguage->dettachCountryWithAllLanguage($id);
            $this->logStoreHelper->storeLogInfo(array('Country Language', [
                'status' => '200',
                'message' => "All  required country  language deleted successfully"
            ]));
            foreach ($request['language'] as $req) {
                try {
                    $language = $this->language->languageGetSpecificByCodeAndEnabled($req);

                    if (count($language) == 0) {
                        throw new \Exception();
                    }
                } catch (\Exception $ex) {
                    DB::rollBack();
                    $this->logStoreHelper->storeLogError(array('Country Language', [
                        'status' => '404',
                        'message' => "Language could not be found"
                    ]));
                    return response()->json([
                        'status' => '404',
                        'message' => "Language could not be found"
                    ], 404);
                }
                //return response()->json($language[0]->id);
                $requestLanguage = (string)$language[0]->id;
                $countrylanguage = $this->countryLanguage->attachCountryToLanguage($country->id, $requestLanguage);
                $this->logStoreHelper->storeLogInfo(array('Country Language', [
                    'status' => '422',
                    'message' => ["Country Language Created successfully"
                    ]
                ]));
            }
// updating country meta .....................................................................................
            if ($request->has('meta')) {
                $rules = [
                    'meta_key' => 'required',
                    'meta_value' => 'required',
                ];
                $countryMetas = $this->countryMeta->countryMetaGetAllByCountryId($id);

                foreach ($countryMetas as $countryMeta) {
                    $countryMeta = $this->countryMeta->countryMetaDelete($countryMeta['id']);
                    /*
                     * firstly all the specific country meta is listed and the all meta are deleted .then after the passed json value in updated
                     * */
                    $this->logStoreHelper->storeLogInfo(array('Country Meta', [
                        'status' => '422',
                        'message' => "Country Meta deleted successfully",
                        "data" => $countryMeta
                    ]));

                }
                foreach ($request['meta'] as $key => $req) {
                    try {

                        $validator = Validator::make($req, $rules);

                        if ($validator->fails()) {
                            $error = $validator->errors();
                            DB::rollBack();
                            $this->logStoreHelper->storeLogError(array('Country Meta', [
                                'status' => '422',
                                "message" => ["meta" => ["$key" => $error]]
                            ]));

                            return response()->json([
                                'status' => '422',
                                "message" => ["meta" => ["$key" => $error]]
                            ]);
                        }


                        try {
                            $req['country_id'] = $country->id;
                            $countryMeta = $this->countryMeta->countryMetaUpdateOrCreate($req);
                            $this->logStoreHelper->storeLogInfo(array('Country Meta ', [
                                "status" => "200",
                                "message" => 'Country Meta Updated Successfully',
                                "data" => $countryMeta]));

                        } catch (\Exception $e) {
                            DB::rollBack();
                            $this->logStoreHelper->storeLogError(array('Country Meta', [
                                'status' => '422',
                                'message' => $e->getMessage()
                            ]));
                            return response()->json([
                                'status' => '422',
                                'message' => $e->getMessage()
                            ], 422);
                        }
                        $this->logStoreHelper->storeLogInfo(array('Country Meta ', [
                            "status" => "200",
                            "data" => $countryMeta]));

                    } catch (\Exception $e) {
                        DB::rollBack();
                        $this->logStoreHelper->storeLogError(array('Country Meta', [
                            'status' => '422',
                            'message' => $e->getMessage()
                        ]));
                        return response()->json([
                            'status' => '422',
                            'message' => $e->getMessage()
                        ], 422);
                    }
                }


            }
//updating translation ......................................................................................................
            $rules = [
                'name' => 'required',
                'lang' => 'required|alpha',
            ];
            $countryTranslations = $this->countryTranslation->countryTranslationGetByCountryId($id);
            foreach ($countryTranslations as $countryTranslation) {
                $countryTranslation = $this->countryTranslation->countryTranslationDelete($countryTranslation['id']);
                $this->logStoreHelper->storeLogInfo(array('Country Translation', [
                    'status' => '200',
                    'message' => "Country Translation Deleted Successfully",
                    "data" => $countryTranslation
                ]));
            }
            foreach ($request['translation'] as $key => $req) {
                try {
                    $req['lang'] = $key;
                    $req['country_id'] = $country->id;
                    $validator = Validator::make($req, $rules);

                    if ($validator->fails()) {
                        $error = $validator->errors();
                        DB::rollBack();
                        $this->logStoreHelper->storeLogError(array('Country Translation', [
                            'status' => '422',
                            "message" => ["translation" => ["$key" => $error]]
                        ]));
                        return response()->json([
                            'status' => '422',
                            "message" => ["translation" => ["$key" => $error]]
                        ], 422);
                    }

                    /**
                     * Checking if given language translation is similar to language attached to country
                     */
                    $check = $this->checkLanguageCode($request['language'], $request['translation']);
                    if ($check == false) {
                        return response()->json([
                            "status" => "422",
                            "message" => ['translation' => ["Input Translation does not match with specified country language code"]]
                        ], 422);
                    }

                    $countryTranslation = $this->countryTranslation->countryTranslationUpdateOrCreate($req);
                    $this->logStoreHelper->storeLogInfo(array('Country Translation', [
                        'status' => '200',
                        'message' => "Country Translation Deleted Successfully",
                        "data" => $countryTranslation
                    ]));


                } catch (\Exception $e) {
                    $this->logStoreHelper->storeLogError(array('error in validation', [
                        'status' => '200',
                        "data" => $e->response->original
                    ]));
                    return response()->json([
                        'status' => '422',
                        'message' => $e->response->original
                    ], 422);
                }
            }


            DB::commit();
            $this->logStoreHelper->storeLogInfo(array('Country ', ['status' => '200',
                'message' => 'Country updated successfully']));
            return response()->json([
                'status' => '200',
                'message' => 'Country updated successfully'
            ], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $this->logStoreHelper->storeLogError('error in validation', [
                'status' => '422',
                'message' => $e->response->original
            ]);
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $country = $this->country->countryGetSpecificSoftOrHardDeleted($id);
            $city = $country->city()->first();
        } catch (\Exception $ex) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => "Requested Country could not be found"
            ], 404);
        }
        try {
            if (is_null($country->deleted_at)) {
                try {
                    if ($city)
                        throw new \Exception("Error! Could not delete the country as it is used in city");
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "403",
                        "message" => $ex->getMessage()
                    ], 403);
                }

                $baseUrl = Config::get("config.restaurant_base_url");
                $fullUrl = $baseUrl . "/restaurant/check";
                $request["country_id"] = $id;
                $districtCheck = RemoteCall::check($fullUrl, $request);
                if ($districtCheck["status"] != 200) {
                    return response()->json([
                        $districtCheck["message"]
                    ], $districtCheck["status"]);
                }
                try {
                    if ($districtCheck["message"]["data"]) {
                        throw new \Exception("Error! Could not delete the country as it is being used in Restaurant.");
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "403",
                        "message" => $ex->getMessage()
                    ], 403);
                }
                $country = $this->country->countryDelete($id);
            } else {
                $country->forceDelete();
            }
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => "Country deleted successfully"
            ]);
        } catch (QueryException $ex) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => "Requested Country could not be found"
            ], 404);
        } catch (ModelNotFoundException  $ex) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => "Requested Country could not be found"
            ], 404);
        } catch (\Exception $ex) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => "Requested Country could not be found"
            ], 404);
        }
    }

    public function checkLanguageCode($languages, $languageTranslations)
    {
        $count = count($languages);
        foreach ($languageTranslations as $key => $translation) {
            $langlist[] = $key;
        }
        $countLanguage = count($langlist);
        if ($count != $countLanguage) {
            return false;
        }
        $countIntersect = count(array_intersect($languages, $langlist));
        if ($countLanguage !== $countIntersect) {
            return false;
        }
        return true;
    }

    public function getCountryByCode($code)
    {
        try {
            $country = $this->country->countryGetSpecificByCode($code);
            if (count($country) == 0) {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError(array("Country", ['status' => '404',
                'message' => "Requested Country could not be found"]));
            return response()->json([
                'status' => '404',
                'message' => "Requested Country could not be found"
            ], 404);
        }
        $country[0]->language;
        return response()->json([
            'status' => '200',
            'data' => $country[0]
        ], 200);
    }


    public function getCountryForNotification($id)
    {
        try {
            $country = $this->country->countryGetSpecific($id);
            if (count($country) == 0) {
                throw new \Exception();
            }

            return response()->json([
                'status' => '200',
                'data' => $country
            ], 200);
        } catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError(array("Country", ['status' => '404',
                'message' => "Requested Country could not be found"]));
            return response()->json([
                'status' => '404',
                'message' => $ex->getMessage()
            ], 404);
        }

    }


}