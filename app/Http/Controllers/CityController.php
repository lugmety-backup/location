<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/13/2017
 * Time: 11:00 AM
 */

namespace App\Http\Controllers;


use App\CityTranslation;
use App\Country;
use App\Repo\CityInterface;
use App\Repo\CityTranslationInterface;
use App\Repo\CountryInterface;
use App\Repo\LanguageInterface;
use App\Repo\CountryLanguageInterface;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use LogStoreHelper;
use RoleChecker;
use RemoteCall;
use Yajra\Datatables\Datatables;

/**
 * Class CityController
 * @package App\Http\Controllers
 */
class CityController extends Controller
{
    protected $city;
    protected $country;
    protected $language;
    protected $cityTranslation;
    protected $countryLanguage;
    protected $logStoreHelper;

    /**
     * CityController constructor.
     * @param $city
     */
    public function __construct(CityInterface $city, CountryInterface $country,
                                LanguageInterface $language,
                                CountryLanguageInterface $countryLanguage,
                                LogStoreHelper $logStoreHelper,
                                CityTranslationInterface $cityTranslation)
    {
        $this->city = $city;
        $this->country = $country;
        $this->language = $language;
        $this->countryLanguage = $countryLanguage;
        $this->cityTranslation = $cityTranslation;
        $this->logStoreHelper = $logStoreHelper;
    }

    /**
     * returns all country based on country id, status,limit,lang
     * @param $country_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {

        $request['limit'] = Input::get("limit", 10);
        $request['lang'] = "en";
        /*
         *
    * validation of limit param if limit param is not null
    * */
        if (!is_null($request['limit'])) {
            $rules = [
                "limit" => "sometimes|numeric|min:1",
                "country_id" => "sometimes|integer|min:1"
            ];

            try {
                $this->validate($request, $rules);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => '422',
                    'message' => $e->response->original
                ], 422);
            }
        }

        /**
         * Checking the given laguage exists in language table if not exist default en is assigned
         *
         */
        $request['lang'] = "en";
        try {
            /**
             * Get all city records from city table .. the records are
             * returned which is equals to status if status is given in parameter
             * else all the records are returned
             *
             */
            $cities = $this->city->cityGetAllPagination(null, $request['country_id'], $request["limit"]);
            try {
                if (count($cities) == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }
            foreach ($cities as $city) {
                $countryLanguage = $this->countryLanguage->getSpecificLanguageOfCountryByLanguage($city['country_id'], $request['lang']);
                if ($countryLanguage->count() == 0) {
                    $request['lang'] = "en";
                }
                $cityTranslation = $this->cityTranslation->cityTranslationGetSpecificByCode($request['lang'], $city['id']);
                $city['lang_code'] = $cityTranslation[0]['lang_code'];
                $city['name'] = $cityTranslation[0]['name'];
            }
            return response()->json([
                'status' => '200',
                'data' => $cities->appends(["country_id" => $request['country_id']])
            ], 200);
        } catch (ModelNotFoundException $e) {

            return response()->json([
                'status' => '404',
                "message" => "Empty Record"
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                "message" => "Empty Record"
            ], 404);
        }
    }

    public function adminIndex(Request $request)
    {
        /**
         * Display a listing of the city and city translation from city and city_translation table.
         *
         */
        /*
         * if logged in user is admin or super admin ,he/she can view city with either status 1 0r 0 or both at once when status is not passed
         * */
        if (RoleChecker::hasRole('customer')) {
            return response()->json([
                'status' => '403',
                'message' => "You have no access"
            ], 403);
        }

        $request['lang'] = Input::get('lang', 'en');

        /**
         * Checking the given laguage exists in language table if not exist default en is assigned
         *
         */
        $language = $this->language->languageGetSpecificByCode($request['lang']);
        if ($language->count() == 0) {
            $request['lang'] = "en";
        }

        /**
         * Get all city records from city table .. the records are
         * returned which is equals to status if status is given in parameter
         * else all the records are returned
         *
         */
        if($request['type'] == "deleted"){
            $cities = $this->city->cityGetAllForDatatablesOnlyDeleted($request['country_id']);
        }else{
            $cities = $this->city->cityGetAllByCountryForDataTable($request['country_id']);
        }

        foreach ($cities as $city) {
//            $countryLanguage = $this->countryLanguage->getSpecificLanguageOfCountryByLanguage($city['country_id'], $request['lang']);
//            if ($countryLanguage->count() == 0) {
//                $request['lang'] = "en";
//            }
            $cityTranslation = $this->cityTranslation->cityTranslationGetSpecificByCode($request['lang'], $city['id']);
            $city['lang_code'] = $cityTranslation[0]['lang_code'];
            $city['name'] = $cityTranslation[0]['name'];
        }
        return Datatables::of($cities)->make(true);
    }


    /**
     * returns all country based on country id, status=1,limit,lang
     * @param $country_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function publicIndex($country_id)
    {
        /*
        * Validation of Country id
        * */
        try {
            $country = $this->country->getCountrySpecificByStatus($country_id);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Requested Country could not be found"
            ], 404);
        }
        /**
         * Display a listing of the city and city translation from city and city_translation table.
         *
         */
        $request['limit'] = Input::get("limit", 10);
        $request['status'] = 1;
        $request['lang'] = Input::get('lang', 'en');


        /*
         *
    * validation of limit param if limit param is not null
    * */
        if (!is_null($request['limit'])) {
            $rules = [
                "limit" => "sometimes|numeric|min:1",
            ];
            $validator = Validator::make($request, $rules);
            if ($validator->fails()) {
                $request['limit'] = 10;
            }
        }

        /**
         * Checking the given laguage exists in language table if not exist default en is assigned
         *
         */
        $language = $this->language->languageGetSpecificByCode($request['lang']);
        if ($language->count() == 0) {
            $request['lang'] = "en";
        }
        try {
            /**
             * Get all city records from city table .. the records are
             * returned which is equals to status if status is given in parameter
             * else all the records are returned
             *
             */
            $cities = $this->city->cityGetAllByCountry(1, $country->id, $request["limit"]);

            try {
                if (count($cities) == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }
            foreach ($cities as $city) {
                $countryLanguage = $this->countryLanguage->getSpecificLanguageOfCountryByLanguage($city['country_id'], $request['lang']);
                if ($countryLanguage->count() == 0) {
                    $request['lang'] = "en";
                }
                $cityTranslation = $this->cityTranslation->cityTranslationGetSpecificByCode($request['lang'], $city['id']);
                $city['lang_code'] = $cityTranslation[0]['lang_code'];
                $city['name'] = $cityTranslation[0]['name'];
            }
            return response()->json([
                'status' => '200',
                'data' => $cities
            ], 200);
        } catch (QueryException $e) {

            return response()->json([
                'status' => '404',
                "message" => "Empty Record"
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                "message" => "Empty Record"
            ], 404);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * check if the passed city belongs to passed country id and is active
     * @param $id
     * @param $city_id
     * @return \Illuminate\Http\JsonResponse
     */

    public function checkCountryAndCity($id,$city_id){
        try{
            $getCityInfo = $this->city->cityGetSpecific($city_id);
            $countryDetail = $getCityInfo->country()->first();
            if(count($countryDetail) == 0){
                return response()->json([
                    "status" => "404",
                    "message" => "Country cound not be found."
                ],404);
            }
            if ($countryDetail->id != $id ){
                return response()->json([
                    "status" => "403",
                    "message" => "The city doesnot belongs to the passed country."
            ],403);
            }
            return response()->json([
                "status" => "200",
                "message" => "Successfully verified city and country."
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "City cound not be found."
            ],404);
        }
        catch (\Exception $ex){

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation of city field
        try {
            $this->validate($request, [
                'timezone' => 'required',
                'country_id' => 'required|integer',
                'tax_percentage' => 'required|numeric|between:0,99.99',
                'status' => 'integer|min:0|max:1',
                'translation' => 'required|array',
            ]);
        } catch (\Exception $e) {
            $this->logStoreHelper->storeLogError(array("Error in Validation", [
                'status' => '422',
                'message' => $e->response->original
            ]));
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }
        DB::beginTransaction();
        try {
            $request = $request->all();
            $request['status'] = Input::get('status', 1);//1 means active

            try {
                //dd($request['country_id']);
                $country = $this->country->countryGetSpecific($request['country_id']);
                if (count($country) == 0) {
                    throw new \Exception();
                }
            } catch (Exception $exception) {
                DB::rollback();
                $this->logStoreHelper->storeLogError(array('Country Id', [
                    ['status' => '404',
                        'message' => 'country_id not found']
                ]));
                return response()->json(['status' => '404',
                    'message' => 'country_id not found'], 404);
            }
            $city['country_id'] = $request['country_id'];
            $city['status'] = $request['status'];
            $city['timezone'] = $request['timezone'];
            $city['tax_percentage'] = $request['tax_percentage'];
            $city = $this->city->cityCreate($city);
            $this->logStoreHelper->storeLogInfo(array("City", [
                "status" => '200',
                "message" => "City Created Successfully",
                "data" => $city
            ]));
            /******************* city translation post starts ************/
            foreach ($request['translation'] as $key => $req) {
                $req['city_id'] = $city['id'];
                $req['lang_code'] = strtolower($key);
                $validators = Validator::make($req, [
                    'name' => 'required',
                    "lang_code" =>"required|alpha"
                ]);
                if ($validators->fails()) {
                    $error = $validators->errors();
                    $this->logStoreHelper->storeLogError(array('Error', [
                        'status' => '422',
                        "message" => ["translation" => ["$key" => $error]]
                    ]));
                    return response()->json([
                        'status' => '422',
                        "message" => ["translation" => ["$key" => $error]]
                    ], 422);
                }
                try {
                    $language = $this->countryLanguage->getAllLanguageOfCountry($request['country_id']);
                    if (count($language) == 0) {
                        throw new \Exception();
                    }
                    $check = $this->checkLanguageCode($language, $request['translation']);
                    if ($check == false) {
                        return response()->json([
                            "status" => "422",
                            "message" => ['translation' => ["Input Translation does not match with specified country language code"]]
                        ], 422);
                    }
                } catch (\Exception $ex) {
                    DB::rollback();
                    return response()->json([
                        'status' => '404',
                        'message' => "Language Not found in country"
                    ], 404);
                }
                $cityTranslation = $this->cityTranslation->cityTranslationCreate($req);

            }
            DB::commit();
            $this->logStoreHelper->storeLogInfo(array("City", [
                'status' => '200',
                'message' => 'City created successfully'
            ]));
            return response()->json([
                'status' => '200',
                'message' => 'City created successfully'
            ], 200);

            /***************** city translation ends ***************/

        } catch (ModelNotFoundException  $ex) {
            DB::rollback();
            $this->logStoreHelper->storeLogError(array("Country ", [
                'status' => '404',
                'message' => "country_id not found"
            ]));
            return response()->json([
                'status' => '404',
                'message' => "country_id not found"
            ], 404);
        } catch (\Exception $e) {
            DB::rollback();
            $this->logStoreHelper->storeLogError(array("Country ", [
                'status' => '422',
                'message' => "validation error"
            ]));
            return response()->json([
                'status' => '422',
                'message' => "validation error"
            ], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
//    public function show($id)
//    {
//        $request['lang'] = Input::get('lang','en');
//        /**
//         * Checking the given laguage exists in language table if not exist default en is assigned
//         *
//         */
//
//        try {
//            $city = $this->city->cityGetSpecific($id);
//            try{
//                $country = $this->country->countryGetSpecific($city->country_id);
//            }
//            catch (\Exception $ex){
//                return response()->json([
//                    "status" => "404",
//                    "message" =>"Country Could Not be found for requested city"
//                ]);
//            }
//
//
//            $countryLanguage=$this->countryLanguage->getSpecificLanguageOfCountryByLanguage($city['country_id'],$request['lang']);
//            if ($countryLanguage->count() == 0) {
//                $request['lang']="en";
//            }
//            $cityTranslation=$this->cityTranslation->
//            cityTranslationGetSpecificByCode($request['lang'],$city['id']);
//            $city['lang_code']=$cityTranslation[0]['lang_code'];
//            $city['name']=$cityTranslation[0]['name'];
//
//            return response()->json([
//                'status' => '200',
//                'data' => $city
//            ], 200);
//        } catch (ModelNotFoundException $e) {
//
//            return response()->json([
//                'status' => '404',
//            ], 404);
//        } catch (\Exception $e) {
//            return response()->json([
//                'status' => '404',
//                'message' => 'error while getting records'
//            ], 404);
//        }
//    }

    public function adminShow($id)
    {
        try {
            $city = $this->city->cityGetSpecific($id);
            $translation = $this->cityTranslation->cityTranslationGetByCityId($id);
            $country = $city->country()->first();
            $city["country_translation"] = $country->countryTranslation()->get();
            $city['translation'] = $translation;
            return response()->json([
                'status' => '200',
                'data' => $city
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => '404',
                "message" => "Empty Record"
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'error while getting records'
            ], 404);
        }
    }

    /**
     * Display the specified city whose status is 1.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function publicShow($id)
    {
        $request['lang'] = Input::get('lang', 'en');
        /**
         * Checking the given laguage exists in language table if not exist default en is assigned
         *
         */
        try {

            $city = $this->city->cityGetSpecificByStatus($id, 1);
            try {
                $country = $this->country->getCountrySpecificByStatus($city->country_id);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Country Could Not be found for requested city"
                ]);
            }


            $countryLanguage = $this->countryLanguage->getSpecificLanguageOfCountryByLanguage($city['country_id'], $request['lang']);
            if ($countryLanguage->count() == 0) {
                $request['lang'] = "en";
            }
            $cityTranslation = $this->cityTranslation->
            cityTranslationGetSpecificByCode($request['lang'], $city['id']);
            $city['lang_code'] = $cityTranslation[0]['lang_code'];
            $city['name'] = $cityTranslation[0]['name'];

            return response()->json([
                'status' => '200',
                'data' => $city
            ], 200);
        } catch (ModelNotFoundException $e) {

            return response()->json([
                'status' => '404',
                "message" => "Empty record"
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'error while getting records'
            ], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation of city field
        try {
            $this->validate($request, [
                'timezone' => 'required',
                'country_id' => 'required|integer',
                'tax_percentage' => 'required|numeric|between:0,99.99',
                'status' => 'integer|min:0|max:1',
                'translation' => 'required|array',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }


        DB::beginTransaction();

        try {
            $request = $request->all();

            $request['status'] = Input::get('status', 1);//1 means active
            try {
                $country = $this->country->countryGetSpecific($request['country_id']);
                if (count($country) == 0) {
                    throw new \Exception();
                }
            } catch (Exception $exception) {
                return response()->json(['status' => '404',
                    'message' => 'country_id not found'], 404);
                DB::rollback();
            }
            $city['country_id'] = $request['country_id'];
            $city['status'] = $request['status'];
            $city['timezone'] = $request['timezone'];
            $city['tax_percentage'] = $request['tax_percentage'];
            $cityUpdate = $this->city->cityUpdate($id, $city);
            /******************* district translation update starts ************/
            $cityUpdate->cityTranslation()->delete();
            foreach ($request['translation'] as $key => $req) {
                $req['city_id'] = $cityUpdate['id'];
                $req['lang_code'] = strtolower($key);
                $validators = Validator::make($req, [
                    'name' => 'required',
                    "lang_code" =>"required|alpha"
                ]);
                if ($validators->fails()) {
                    $error = $validators->errors();
                    $this->logStoreHelper->storeLogError(array('Error', [
                        'status' => '422',
                        "message" => ["translation" => ["$key" => $error]]
                    ]));
                    return response()->json([
                        'status' => '422',
                        "message" => ["translation" => ["$key" => $error]]
                    ], 422);
                }
                try {
                    $language = $this->countryLanguage->getAllLanguageOfCountry($request['country_id']);
                    if (count($language) == 0) {
                        throw new \Exception();
                    }
                    $check = $this->checkLanguageCode($language, $request['translation']);
                    if ($check == false) {
                        return response()->json([
                            "status" => "422",
                            "message" => ['translation' => ["Input Translation does not match with specified country language code"]]
                        ], 422);
                    }
                } catch (\Exception $ex) {
                    DB::rollback();
                    return response()->json([
                        'status' => '404',
                        'message' => "Language Not found in country"
                    ], 404);
                }
                try {
                    $cityTranslation = $this->cityTranslation->cityTranslationUpdateOrCreate($req);
                } catch (\Exception $e) {
                    return response()->json([
                        'status' => '422',
                        'message' => $e->response->original
                    ], 422);
                    DB::rollback();
                }

            }
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'City updated successfully'
            ], 200);

            /***************** district translation ends ***************/
        } catch (ModelNotFoundException  $ex) {
            return response()->json([
                'status' => '404',
                'message' => "city_id not found"
            ], 404);
            DB::rollback();
        } catch (\Exception $e) {
            return response()->json([
                'status' => '422',
                'message' => "validation error"
            ], 422);
            DB::rollback();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $city = $this->city->cityGetSpecificSoftOrHardDeleted($id);
            $districts = $city->district()->first();
        } catch (\Exception $ex) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => "Requested City could not be found"
            ], 404);
        }

        try {
            if(is_null($city->deleted_at)) {
                try {

                    if (count($districts) != 0)
                        throw new \Exception("Error! Could not delete the city as it is used in district");
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "403",
                        "message" => $ex->getMessage()
                    ], 403);
                }

                $baseUrl = Config::get("config.restaurant_base_url");
                $fullUrl = $baseUrl . "/restaurant/branch/check";
                $request["city_id"] = $id;
                $districtCheck = RemoteCall::check($fullUrl, $request);
                if ($districtCheck["status"] != 200) {
                    return response()->json([
                        $districtCheck["message"]
                    ], $districtCheck["status"]);
                }
                try {
                    if ($districtCheck["message"]["data"]) {
                        throw new \Exception("Error! Could not delete the city as it is being used in Restaurant Branch.");
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "403",
                        "message" => $ex->getMessage()
                    ], 403);
                }
                $city = $this->city->cityDelete($id);
            }
            else{
                $city->forceDelete();
            }
            DB::commit();
            return response()->json([
                'status' => '200',
                'country' => "City deleted successfully"
            ]);
        } catch (QueryException $ex) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => "Requested City could not be found"
            ], 404);
        } catch (ModelNotFoundException  $ex) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => "Requested City could not be found"
            ], 404);
        } catch (\Exception $ex) {
            DB::rollback();
            return response()->json([
                'status' => '404',
                'message' => "Requested City could not be found"
            ], 404);
        }
    }

    public function checkLanguageCode($language, $languageTranslations)
    {
        foreach ($language as $lang) {
            $languages[] = $lang['code'];
        }
        $count = count($languages);
        foreach ($languageTranslations as $key => $translation) {
            $langlist[] = $key;
        }
        $countLanguage = count($langlist);
        if ($count != $countLanguage) {
            return false;
        }
        $countIntersect = count(array_intersect($languages, $langlist));
        if ($countLanguage !== $countIntersect) {
            return false;
        }
        return true;
    }


}