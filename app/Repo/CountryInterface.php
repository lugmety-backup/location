<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface CountryInterface
{
    public function countryGetAllForDatatables();

    public function countryGetAllForDatatablesOnlyDeleted();

    public function countryGetAll($status ,$limit);

    public function countryGetSpecific($id);

    public function getCountrySpecificByStatus($id);

    public function countryGetSpecificByCode($code);

    public function countryGetSpecificByCodeByStatusOne($code);

    public function countryCreate(array $attributes);

    public function countryUpdate($id, array $attributes);

    public function countryDelete($id);

    public function countryGetSpecificSoftOrHardDeleted($id);
}