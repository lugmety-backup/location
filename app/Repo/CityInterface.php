<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/13/2017
 * Time: 10:50 AM
 */

namespace App\Repo;


interface CityInterface
{

    public function cityGetAll($status);

    public function cityGetAllByCountry($status, $country_id,$limit);

    public function cityGetAllPagination($status,$country_id,$limit);

    public function cityGetAllByCountryForDataTable($country_id);

    public function cityGetAllForDatatablesOnlyDeleted($country_id);

    public function cityGetSpecific($id);

    public function cityGetSpecificByStatus($id,$status);

    public function cityGetSpecificTax($id);

    public function cityGetSpecificByCode($code);

    public function cityCreate(array $attributes);

    public function cityUpdate($id, array $attributes);

    public function cityDelete($id);

    public function cityGetSpecificSoftOrHardDeleted($id);
}