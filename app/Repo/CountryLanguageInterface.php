<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface CountryLanguageInterface
{
    public function getAllLanguageOfCountry($id);

    public function getSpecificLanguageOfCountryByLanguage($id,$code);

    public function dettachCountryWithAllLanguage($id);

    public function attachCountryToLanguage($id, $request);

    public function dettachCountryToLanguage($id, $request);

}