<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 10/13/17
 * Time: 11:05 AM
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class ExportController extends Controller
{
    private $db_ext;

    public function __construct()
    {
        $this->db_ext = DB::connection('mysql_external');

    }

    public function exportCountry()
    {
        try {
            DB::table('country')->insert([
                'id' => 1,
                'code' => 'ksa',
                'currency_code' => 'SAR',
                'calling_code' => '+966',
                'timezone' => 'Asia/Riyadh',
                'tax_percentage' => 10.00,
                'min_cost' => 5.00,
                'per_cost' => 1.5,
                'status' => 1,
                'deleted_at' => null,
                'week_day_start_at' => 'Sunday',
                'created_at' => '2016-07-17 17:34:42',
                'updated_at' => '2016-07-17 17:34:42',
                'flag' => ''

            ]);

            DB::table('country_translation')->insert([
                [
                    'country_id' => 1,
                    'lang' => 'en',
                    'name' => 'Saudi Arabia',
                ],
                [
                    'country_id' => 1,
                    'lang' => 'ar',
                    'name' => 'المملكة العربية السعودية',
                ]
            ]);
            return response()->json([

                'message' => 'data migrated successfully',
                'status' => '200'

            ], 200);
        } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }

    }

    public function exportCity()
    {
        try {
            DB::beginTransaction();
            $oldcites = $this->db_ext->table('cities')->get();

            //return $oldcites;
            foreach ($oldcites as $city) {
                $insertCity = [];
                $insertCity['id'] = $city->id;
                $insertCity['country_id'] = 1;
                $insertCity['timezone'] = $city->timezone;
                $insertCity['tax_percentage'] = $city->tax_percent;
                $insertCity['status'] = $city->status;
                $insertCity['deleted_at'] = $city->is_deleted == 'N' ? null : $city->updated_at;
                $insertCity['updated_at'] = $city->created_at;
                $insertCity['created_at'] = $city->updated_at;

                DB::table('city')->insert($insertCity);


                $insertTranslation1 = [];
                $insertTranslation1['lang_code'] = 'en';
                $insertTranslation1['city_id'] = $city->id;
                $insertTranslation1['name'] = $city->name;

                DB::table('city_translation')->insert($insertTranslation1);

                if (!empty($city->ar_name)) {
                    $insertTranslation2 = [];
                    $insertTranslation2['lang_code'] = 'ar';
                    $insertTranslation2['city_id'] = $city->id;
                    $insertTranslation2['name'] = $city->ar_name;

                    DB::table('city_translation')->insert($insertTranslation2);
                }

            }
            DB::commit();
            return response()->json([

                'message' => 'data migrated successfully',
                'status' => '200'

            ], 200);

        } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }
    }

    public function exportDistrict()
    {
        try {
            DB::beginTransaction();
            $oldDistricts = $this->db_ext->table('districts')->get();

            //return $oldDistricts;
            foreach ($oldDistricts as $district) {
                $insertDistrict = [];
                $insertDistrict['id'] = $district->id;
                $insertDistrict['city_id'] = $district->city_id;
                $insertDistrict['status'] = $district->status;
                $insertDistrict['deleted_at'] = $district->is_deleted == 'N' ? null : $district->updated_at;
                $insertDistrict['updated_at'] = $district->created_at;
                $insertDistrict['created_at'] = $district->updated_at;

                DB::table('district')->insert($insertDistrict);


                $insertTranslation1 = [];
                $insertTranslation1['lang_code'] = 'en';
                $insertTranslation1['district_id'] = $district->id;
                $insertTranslation1['name'] = $district->name;

                DB::table('district_translation')->insert($insertTranslation1);

                if (!empty($district->ar_name)) {
                    $insertTranslation2 = [];
                    $insertTranslation2['lang_code'] = 'ar';
                    $insertTranslation2['district_id'] = $district->id;
                    $insertTranslation2['name'] = $district->ar_name;

                    DB::table('district_translation')->insert($insertTranslation2);
                }

            }
            DB::commit();
            return response()->json([

                'message' => 'data migrated successfully',
                'status' => '200'

            ], 200);

        } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }
    }

    public function exportShippingAddress()
    {
        try {
            DB::beginTransaction();
            $oldAddresses = $this->db_ext->table('user_customer_addresses')
                ->join('user_customers', 'user_customers.id', '=', 'user_customer_addresses.customer_id')
                ->select('user_customer_addresses.*', 'user_customers.user_id')
                ->get();

            foreach ($oldAddresses as $address) {
                $city = $this->db_ext->table('districts')->join('cities', 'districts.city_id', '=', 'cities.id')->where('districts.id', $address->district_id)->pluck('cities.id');

                if (count($city) > 0) {

                    $insertAddress['city'] = $city[0];

                }
                if (!empty($address->google_coordinates)) {
                    $coordinates = explode(",", $address->google_coordinates);
                }
                //return $coordinates;
                $insertAddress = [];
                $insertAddress['id'] = $address->id;
                $insertAddress['user_id'] = $address->user_id;
                $insertAddress['address_name'] = $address->address_name;
                $insertAddress['street'] = $address->street;
                $insertAddress['latitude'] = isset($coordinates[0]) ? trim($coordinates[0]) : '24.774265';
                $insertAddress['longitude'] = isset($coordinates[1]) ? trim($coordinates[1]) : '46.738586';
                $insertAddress['district_id'] = $address->district_id;
                $insertAddress['extra_direction'] = empty($address->extra_direction) ? '' : $address->extra_direction;
                $insertAddress['type'] = empty($address->floor_number) ? 'house' : 'building';
                $insertAddress['primary'] = 0;
                $insertAddress['created_at'] = $address->created_at;
                $insertAddress['updated_at'] = $address->updated_at;
                $insertAddress['country_id'] = 1;
                $insertAddress['city_id'] = $city[0];
                $insertAddress['zip_code'] = empty($address->zip_code) ? '' : $address->zip_code;
                $insertAddress['house_number'] = $address->house_number;
                $insertAddress['floor_number'] = empty($address->floor_number) ? '' : $address->floor_number;
                $insertAddress['building_name'] = empty($address->building_name) ? '' : $address->building_name;
                $insertAddress['office_name'] = empty($address->office_name) ? '' : $address->office_name;


                DB::table('shipping_address')->insert($insertAddress);


            }
            DB::commit();
            return response()->json([

                'message' => 'data migrated successfully',
                'status' => '200'

            ], 200);

        } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }
    }

    public function exportBillingAddress()
    {

        try {
            DB::beginTransaction();
            $oldAddresses = $this->db_ext->table('user_customer_billing_addresses')
                ->join('user_customers', 'user_customers.id', '=', 'user_customer_billing_addresses.customer_id')
                ->join('users', 'users.id', '=', 'user_customers.user_id')
                ->join('districts', 'districts.id', '=', 'user_customer_billing_addresses.district_id')
                ->join('cities', 'districts.city_id', '=', 'cities.id')
                ->select('user_customer_billing_addresses.*', 'user_customer_billing_addresses.id as billing_id', 'users.*', 'user_customers.user_id', 'districts.name as district_name', 'cities.name as city_name', 'user_customer_billing_addresses.created_at as billing_created_at', 'user_customer_billing_addresses.updated_at as billing_updated_at')
                ->get();
            //return $oldAddresses;
            foreach ($oldAddresses as $address) {
                $addressLines = unserialize($address->address);
                $insertAddress['address_line_one'] = empty($addressLines['address_line1']) ? '' : $addressLines['address_line1'];
                $insertAddress['address_line_two'] = empty($addressLines['address_line2']) ? '' : $addressLines['address_line2'];

                $insertAddress['id'] = $address->billing_id;
                $insertAddress['user_id'] = $address->user_id;
                $insertAddress['address_name'] = '';
                $insertAddress['first_name'] = $address->first_name;
                $insertAddress['middle_name'] = '';
                $insertAddress['email'] = $address->email;
                $insertAddress['postal_code'] = $address->zip_code;
                $insertAddress['last_name'] = $address->last_name;

                $insertAddress['district'] = $address->district_name;
                $insertAddress['city'] = $address->city_name;
                $insertAddress['country'] = "Saudi Arabia";
                $insertAddress['primary'] = 1;
                $insertAddress['created_at'] = $address->billing_created_at;
                $insertAddress['updated_at'] = $address->billing_updated_at;

                DB::table('billing_address')->insert($insertAddress);

            }

            DB::commit();
            return response()->json([

                'message' => 'data migrated successfully',
                'status' => '200'

            ], 200);

        } catch (\Exception $ex) {
            return response()->json(['message' => $ex->getMessage(),
                'status' => '400'], 400);
        }
    }


}