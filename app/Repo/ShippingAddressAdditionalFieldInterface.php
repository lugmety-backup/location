<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 5:17 PM
 */

namespace App\Repo;


interface ShippingAddressAdditionalFieldInterface
{
    public function shippingAddressAdditionalFieldGetAll();

    public function shippingAddressAdditionalFieldGetAllByShippingAddressId($id);

    public function shippingAddressAdditionalFieldGetSpecificByKey($key, $id);

    public function shippingAddressAdditionalFieldGetSpecific($id);

    public function shippingAddressAdditionalFieldCreate(array $attributes);

    public function shippingAddressAdditionalFieldUpdate($id, array $attributes);

    public function shippingAddressAdditionalFieldUpdateOrCreate(array $attributes);

    public function shippingAddressAdditionalFieldDelete($id);

    public function shippingAddressAdditionalFieldDeleteByShippingId($shipping_address_id);
}